package adapters;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitplus.AppDelegate;
import com.fitplus.CreateTeamActivity;
import com.fitplus.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Constants.Tags;
import model.TeamListModel;
import staggeredView.DataSet;


public class TeamListAdapter extends RecyclerView.Adapter<TeamListAdapter.ViewHolder> {
    private final ArrayList<TeamListModel> trending_list;
    View v;
    DataSet ds;
    FragmentActivity context;
    private LayoutInflater mInflater;
    private Map map = new HashMap<>();
    private Map immap = new HashMap<>();
    ArrayList<String> images = new ArrayList<>();
    private Bundle bundle;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.team_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final TeamListModel userDataModel = trending_list.get(position);
        holder.last_msg.setText(userDataModel.lastMessage);
        holder.team_name.setText(userDataModel.team_name);
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt("key", 1);
                bundle.putParcelable(Tags.createName, userDataModel);
                Intent intent = new Intent(context, CreateTeamActivity.class);
                intent.putExtras(bundle);
                context.startActivity(intent);
                // AppDelegate.showFragment(context.getSupportFragmentManager(), new CreateTeamActivity(), R.id.container, bundle, null);
            }
        });
if(AppDelegate.isValidString(userDataModel.datetime)) {
    String time = userDataModel.datetime.substring(0, userDataModel.datetime.lastIndexOf("+"));
//        2016-06-27T06:32
    try {
//            AppDelegate.LogT("time before = " + time);
        time = new SimpleDateFormat("hh:mm aa").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time));
    } catch (ParseException e) {
        e.printStackTrace();
    }
    holder.time.setText(time);
}
        // holder.time.setText(userDataModel.created);
        AppDelegate.LogT("userDataModel.team_avtar => " + userDataModel.team_avtar);
        if (AppDelegate.isValidString(userDataModel.team_avtar)) {
            holder.img_loading.setVisibility(View.VISIBLE);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
                    frameAnimation.setCallback(holder.img_loading);
                    frameAnimation.setVisible(true, true);
                    frameAnimation.start();
                    ((Animatable) holder.img_loading.getDrawable()).start();
                }
            });
            Picasso.with(context).load(userDataModel.team_avtar).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    holder.image.setImageBitmap(bitmap);
                    holder.img_loading.setVisibility(View.GONE);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }
            });
        }
    }

    public TeamListAdapter(FragmentActivity context, ArrayList<TeamListModel> trending_list) {
        this.context = context;
        this.trending_list = trending_list;
        // this. ds=ds;
    }

    @Override
    public int getItemCount() {
        return trending_list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView last_msg, team_name, time;
        ImageView star, dots;
        ImageView image, img_loading;
        LinearLayout card;

        public ViewHolder(View itemView) {
            super(itemView);
            last_msg = (TextView) itemView.findViewById(R.id.lastmsg);
            image = (ImageView) itemView.findViewById(R.id.team_icon);
            img_loading = (ImageView) itemView.findViewById(R.id.img_loading);
            team_name = (TextView) itemView.findViewById(R.id.team_name);
            time = (TextView) itemView.findViewById(R.id.time);
            card = (LinearLayout) itemView.findViewById(R.id.card);
        }
    }
}