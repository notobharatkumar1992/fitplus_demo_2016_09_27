package adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.fitplus.AppDelegate;
import com.fitplus.R;

import java.util.ArrayList;

import model.UploadDocsModel;


/**
 * Created by admin on 04-07-2016.
 */
public class UploadDocsAdapter extends BaseAdapter {
    ArrayList<UploadDocsModel> productListModels;
    Context context;

    public UploadDocsAdapter(Context context, ArrayList<UploadDocsModel> productListModels) {
        this.productListModels = productListModels;
        this.context = context;
    }

    @Override
    public int getCount() {
        return productListModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.img_item, null, true);

        final ImageView showImage = (ImageView) rowView.findViewById(R.id.show_image);
        final ImageView deleteImage = (ImageView) rowView.findViewById(R.id.delete_image);
        showImage.setImageBitmap(productListModels.get(position).getBitmap());
        deleteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productListModels.get(position).setBitmap(null);
                productListModels.get(position).setImage(null);
                productListModels.remove(position);
                notifyDataSetChanged();
                AppDelegate.LogT("After deletion upload docs =>"+productListModels+"");
            }
        });
        return rowView;
    }
}
