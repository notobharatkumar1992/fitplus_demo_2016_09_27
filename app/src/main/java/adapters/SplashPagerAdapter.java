package adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitplus.R;

public class SplashPagerAdapter extends PagerAdapter {

    private int mSize;
    private Context mContext;

    public SplashPagerAdapter() {
        mSize = 3;
    }

    public SplashPagerAdapter(Context mContext, int count) {
        mSize = count;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return mSize;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup view, int position, Object object) {
        view.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View convertView = ((LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.get_started_text, null);
        view.addView(convertView);
        return convertView;
    }
}