package adapters;


import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitplus.AppDelegate;
import com.fitplus.R;
import com.fitplus.SetLocationActivity;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import Async.LocationAddress;
import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnReciveServerResponse;
import model.ChatModel;
import model.PostAysnc_Model;
import model.ScheduledListModel;
import utils.Prefs;


public class Recycler_Calendar_dates_Adapter extends RecyclerView.Adapter<Recycler_Calendar_dates_Adapter.ViewHolder> implements OnReciveServerResponse{
    View v;
    FragmentActivity context;
    ArrayList<ScheduledListModel> scheduled_List;
    private Handler mHandler;
    private String address;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.calendar_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        setHandler();
        return viewHolder;
    }

    private void setloc(double latitude, double longitude) {
        LocationAddress.getAddressFromLocation(latitude, longitude, context, mHandler);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 0:
                        break;
                    case 2:
                        setAddressFromGEOcoder(msg.getData());
                        break;
                }
            }
        };
    }

    private void setAddressFromGEOcoder(Bundle data) {
        address = data.getString(Tags.ADDRESS) + "";
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        AppDelegate.LogT("ViewHolder" + position + "");
        final ScheduledListModel scheduledListModel = scheduled_List.get(position);
        holder.txt_work_out.setText(scheduledListModel.teamName + "");
        try {
            Date simpleDateFormat = new SimpleDateFormat("HH:mm:ss").parse(scheduledListModel.training_date);
            String formattedDate = new SimpleDateFormat("HH:mm").format(simpleDateFormat);
            holder.txt_starttime.setText(formattedDate);
            holder.txt_workout_time.setText(formattedDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(simpleDateFormat);
            calendar.add(Calendar.HOUR, 1);
            Date previous_time = calendar.getTime();
            String toformattedDate = new SimpleDateFormat("HH:mm").format(previous_time);
            holder.txt_work_out.setText(scheduledListModel.teamName +"" );
            holder.txt_workout_time.setText(" - "+ formattedDate+"-"+toformattedDate+"");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (!scheduledListModel.teamMembers.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < scheduledListModel.teamMembers.size(); i++) {
                sb.append(scheduledListModel.teamMembers.get(i)).append(" ,");
            }
            String users = sb.toString();
            users = users.substring(0, users.length() - 1);
            holder.txt_username.setText(users);
        }
        holder.extra.setText(String.valueOf(scheduledListModel.teamMembers.size()) + "");
        setloc(scheduledListModel.latitude, scheduledListModel.longitude);

        if (scheduledListModel.latitude != 0.0 && scheduledListModel.longitude != 0.0) {
            new AddressAsync(scheduledListModel.latitude, scheduledListModel.longitude, holder).execute();
        }

        if(scheduledListModel.log_hours==0){
            holder.img_lock.setImageResource(R.drawable.openlock);
            holder.txt_starttime.setSelected(true);
            holder. img_lock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    execute_lock_hours(scheduledListModel.id,scheduledListModel.team_id);
                }
            });
        }else{
            holder.txt_starttime.setSelected(false);
            holder.img_lock.setImageResource(R.drawable.lock);
        }
        holder.location.setText(address);
        holder.img_follower_one.setVisibility(View.GONE);
        holder.img_follower_two.setVisibility(View.GONE);
        holder.img_follower_three.setVisibility(View.GONE);
        holder.img_follower_four.setVisibility(View.GONE);

        if (!scheduledListModel.teamMembersAvtar.isEmpty()) {
            for (int i = 0; i < scheduledListModel.teamMembersAvtar.size(); i++) {
                final int finalI = i;
                Picasso.with(context).load(scheduledListModel.teamMembersAvtar.get(i)).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        AppDelegate.LogT("onBitmapLoaded" + scheduledListModel.teamMembersAvtar.get(finalI));
                        if (finalI == 0) {
                            holder.img_follower_one.setVisibility(View.VISIBLE);
                            holder.img_follower_one.setImageBitmap(bitmap);
                        } else if (finalI == 1) {
                            holder.img_follower_two.setVisibility(View.VISIBLE);
                            holder.img_follower_two.setImageBitmap(bitmap);
                        } else if (finalI == 2) {
                            holder.img_follower_three.setVisibility(View.VISIBLE);
                            holder.img_follower_three.setImageBitmap(bitmap);
                        } else if (finalI == 3) {
                            holder.img_follower_four.setVisibility(View.VISIBLE);
                            holder.img_follower_four.setImageBitmap(bitmap);
                        }
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                        AppDelegate.LogT("onBitmapFailed" + scheduledListModel.teamMembersAvtar.size());
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                        AppDelegate.LogT("onPrepareLoad" + scheduledListModel.teamMembersAvtar.size());
                    }
                });
            }
        }
    }

    private void execute_lock_hours(int id,int team_id) {
        try {
            if (AppDelegate.haveNetworkConnection(context, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(context).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(context).setPostParamsSecond(mPostArrayList, Parameters.coach_id, new Prefs(context).getUserdata().userId);
                AppDelegate.getInstance(context).setPostParamsSecond(mPostArrayList, Parameters.session_id, id);
                AppDelegate.getInstance(context).setPostParamsSecond(mPostArrayList, Parameters.team_id,team_id);

                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(context, Recycler_Calendar_dates_Adapter.this, ServerRequestConstants.LOGHOURS,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(context);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(context, context.getResources().getString(R.string.try_again), "Alert!!!");
        }


    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(context);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(context, context.getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.LOGHOURS)) {
            parseLockHours(result);
        }
    }

    private void parseLockHours(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            AppDelegate.showToast(context, jsonObject.getString(Tags.message));
        } catch (Exception e) {
            AppDelegate.ShowDialog(context,context. getResources().getString(R.string.response_error), "");
            AppDelegate.LogE(e);
        }
    }

    class AddressAsync extends AsyncTask<Void, Void, Void> {

        public double latitude, longitude;
        ViewHolder holder;
        public String addressResult = "";

        public AddressAsync(double latitude, double longitude, ViewHolder holder) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.holder = holder;
        }

        @Override
        protected Void doInBackground(Void... params) {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            try {
                List<Address> addressList = geocoder.getFromLocation(latitude,
                        longitude, 1);
                AppDelegate.LogT("addressList latitude = " + latitude
                        + ", longitude = " + longitude + ", addressList size = "
                        + addressList.size());
                if (addressList != null && addressList.size() > 0) {
                    Address address = addressList.get(0);
                    AppDelegate.LogUR("addressList = " + address);
//                    StringBuilder sb = new StringBuilder();
//                    placeName = address.getAddressLine(0);
//                    if (address.getAddressLine(1) != null && !address.getAddressLine(1).equalsIgnoreCase("null") && address.getAddressLine(0).length() > 0) {
//                        placeName = placeName + ", " + address.getAddressLine(1);
//                        placeName = placeName.replace("null,", " ");
//                        placeName = placeName.replace("null", " ");
//                    }
//                    if (placeName != null && placeName.contains("null")) {
//                        placeName = placeName.replace("null,", " ");
//                        placeName = placeName.replace("null", " ");
//                    }
//                    sb.append(address.getAdminArea()).append(", ");
//                    sb.append(address.getLocality()).append(", ");
//                    sb.append(address.getCountryName());
//
//                    if (address.getLocality() != null
//                            && !address.getLocality().equalsIgnoreCase("null")) {
//                        placeAdd = address.getLocality() + ", "
//                                + address.getCountryName();
//                    } else {
//                        placeAdd = address.getCountryName();
//                    }
//                    if (placeAdd != null && placeAdd.contains("null")) {
//                        placeAdd = placeAdd.replace("null,", " ");
//                        placeAdd = placeAdd.replace("null", " ");
//                    }
//                    if (placeAdd.contains(placeName)) {
//                        placeAdd.replace(placeName, "");
//                    } else if (placeName.contains(placeAdd)) {
//                        placeName.replace(placeAdd, "");
//                    }
//
//                    StringBuilder place = new StringBuilder();
//                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
//                        place.append(address.getAddressLine(i)).append(", ");
//                        if (i == 1) {
//                            break;
//                        }
//                    }
//                    city = address.getLocality();
//                    country = address.getCountryName();
//                    postalCode = address.getPostalCode();
//                    state = address.getAdminArea();
//                    loc = place.toString();
//                    result = sb.toString();
                    addressResult = address.getCountryName();
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            } finally {
                AppDelegate.LogE("Finally called for LocationAddress");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                if (AppDelegate.isValidString(addressResult)) {
                    holder.location.setText(addressResult);
                    holder.location.setVisibility(View.VISIBLE);
                } else {
                    holder.location.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
    }

    public Recycler_Calendar_dates_Adapter(FragmentActivity context, ArrayList<ScheduledListModel> scheduled_List) {
        this.context = context;
        this.scheduled_List = scheduled_List;
        AppDelegate.LogT("scheduled_List  " + scheduled_List.size() + "");
    }

    @Override
    public int getItemCount() {
        return scheduled_List.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_starttime, txt_workout_time, txt_work_out, txt_username, extra, location;
        ImageView img_follower_one, img_follower_two, img_follower_three, img_follower_four,img_lock;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_starttime = (TextView) itemView.findViewById(R.id.txt_starttime);
            txt_workout_time = (TextView) itemView.findViewById(R.id.txt_workout_time);
            txt_work_out = (TextView) itemView.findViewById(R.id.txt_work_out);
            txt_username = (TextView) itemView.findViewById(R.id.txt_username);
            location = (TextView) itemView.findViewById(R.id.location);
            extra = (TextView) itemView.findViewById(R.id.extra);
            img_follower_one = (ImageView) itemView.findViewById(R.id.img_follower_one);
            img_follower_two = (ImageView) itemView.findViewById(R.id.img_follower_two);
            img_follower_three = (ImageView) itemView.findViewById(R.id.img_follower_three);
            img_follower_four = (ImageView) itemView.findViewById(R.id.img_follower_four);
            img_lock = (ImageView) itemView.findViewById(R.id.img_lock);

        }
    }
}