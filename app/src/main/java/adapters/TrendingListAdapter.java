package adapters;


import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitplus.AppDelegate;
import com.fitplus.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;

import interfaces.OnListItemClickListener;
import model.UserDataModel;
import staggeredView.STGVImageView;


public class TrendingListAdapter extends RecyclerView.Adapter<TrendingListAdapter.ViewHolder> {

    private ArrayList<UserDataModel> trending_list;
    View v;
    FragmentActivity context;
    OnListItemClickListener onListItemClickListener;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trendinglist_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final UserDataModel userDataModel = trending_list.get(position);
        AppDelegate.LogT("Radar Model in onBindViewHolder" + userDataModel.avtar);
        holder.desc.setText(userDataModel.first_name + "");

        holder.img_loading.setVisibility(View.VISIBLE);
        AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
        frameAnimation.setCallback(holder.img_loading);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();
        Picasso.with(context).load(userDataModel.avtar).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                AppDelegate.LogT("onBitmapLoaded" + userDataModel.avtar);
                holder.background_img.mWidth = bitmap.getWidth();
                holder.background_img.mHeight = bitmap.getHeight();
               // holder.background_img.setImageBitmap(AppDelegate.blurRenderScript(context, bitmap));
                holder.background_img.setImageBitmap( bitmap);
                holder.img_loading.setVisibility(View.GONE);
                try {
                    notifyDataSetChanged();
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                AppDelegate.LogT("onBitmapFailed" + userDataModel.avtar);
                try {
                    notifyDataSetChanged();
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                AppDelegate.LogT("onPrepareLoad" + userDataModel.avtar);
                try {
                    notifyDataSetChanged();
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }
        });

        holder.background_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onListItemClickListener != null) {
                    onListItemClickListener.setOnListItemClickListener("name", position);
                }
            }
        });
    }

    public TrendingListAdapter(FragmentActivity context, ArrayList<UserDataModel> trending_list, OnListItemClickListener onListItemClickListener) {
        this.context = context;
        this.trending_list = trending_list;
        this.onListItemClickListener = onListItemClickListener;
    }

    @Override
    public int getItemCount() {
        return trending_list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView desc, coach_name, followers, pkg;
        ImageView star, dots;
        ImageView image, img_loading;

        STGVImageView background_img;

        public ViewHolder(View itemView) {
            super(itemView);
            desc = (TextView) itemView.findViewById(R.id.desc);
            background_img = (STGVImageView) itemView.findViewById(R.id.img_content);
            img_loading = (ImageView) itemView.findViewById(R.id.img_loading);
        }
    }
}