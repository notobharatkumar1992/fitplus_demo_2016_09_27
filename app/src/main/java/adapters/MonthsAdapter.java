package adapters;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitplus.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import interfaces.OnListItemClickListener;
import model.StatMonthsModel;


public class MonthsAdapter extends RecyclerView.Adapter<MonthsAdapter.ViewHolder> {

    FragmentActivity context;
    OnListItemClickListener itemClickListener;
    ArrayList<StatMonthsModel> monthArray;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.months_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // bundle=new Bundle();
        //  final PointsHistoryModel dv = pointsHistory.get(position);
        holder.month.setText(monthArray.get(position).month);
        if (monthArray.get(position).isSelected) {
            holder.month.setTextColor(context.getResources().getColor(R.color.white));
            holder.bar.setVisibility(View.VISIBLE);
        } else {
            holder.month.setTextColor(context.getResources().getColor(R.color.stats_textcolor));
            holder.bar.setVisibility(View.INVISIBLE);
        }
        holder.month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemClickListener != null) {
                    itemClickListener.setOnListItemClickListener("state", position);
                }
            }
        });
    }

    public MonthsAdapter(FragmentActivity context, ArrayList<StatMonthsModel> monthArray, OnListItemClickListener itemClickListener) {
        this.context = context;
        this.monthArray = monthArray;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return monthArray.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView month;
        LinearLayout bar;

        public ViewHolder(View itemView) {
            super(itemView);
            month = (TextView) itemView.findViewById(R.id.month);
            bar = (LinearLayout) itemView.findViewById(R.id.bar);
        }
    }
}