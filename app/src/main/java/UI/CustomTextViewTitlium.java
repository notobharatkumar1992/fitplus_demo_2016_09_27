package UI;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by UTI on 21-01-2016.
 */
public class CustomTextViewTitlium extends TextView {

    public CustomTextViewTitlium(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomTextViewTitlium(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTextViewTitlium(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/TitilliumWeb-Regular.ttf");
        setTypeface(tf);
    }

}
