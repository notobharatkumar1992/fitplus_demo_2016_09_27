package UI;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.RadioButton;

/**
 * Created by UTI on 21-01-2016.
 */
public class CustomRadioButtion extends RadioButton {

    public CustomRadioButtion(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomRadioButtion(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomRadioButtion(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Poppins-SemiBold.ttf");
        setTypeface(tf);
    }

}