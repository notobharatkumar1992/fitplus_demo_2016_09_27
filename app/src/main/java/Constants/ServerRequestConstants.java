package Constants;

public class ServerRequestConstants {
    /**
     * (All Constants that were used in server connection are declared)
     */

	/*
     * Post Parameter Type Constants
	 */
    public static final String CODE_200 = "200";
    public static final String CODE_400 = "400";
    public static final String CODE_401 = "401";
    public static final String CODE_402 = "402";
    public static final String CODE_403 = "403";
    public static final String CODE_404 = "404";
    public static final String Key_PostStrValue = "String";
    public static final String Key_PostDoubleValue = "double";
    public static final String Key_PostintValue = "int";
    public static final String Key_PostbyteValue = "byte";
    public static final String Key_PostFileValue = "File";
    public static final String Key_PostStringArrayValue = "String[]";
    public static final String Key_PostStringArrayValue1 = "String[]";

    //    public static final String CHAT_SERVER_URL = "http://192.168.100.34:3000/";
    public static final String CHAT_SERVER_URL = "http://notosolutions.net:3002/";
    public static final String BASE_URL = "http://notosolutions.net/fitplus/webServices/";
    //   public static final String BASE_URL = "http://192.168.100.53/fitplus/webServices/";
    //  public static final String BASE_URL_DRIVER = "http://192.168.100.81/short/webServicesDriver/";
    public static final String REGISTER = BASE_URL + "signup";
    public static final String SOCIAL_SIGNIN = BASE_URL + "SocialLoginCheck";
    public static final String SOCIAL_SIGNIN2 = BASE_URL + "SocialLogin2";
    public static final String GET_COUNTRY_LIST = BASE_URL + "getCountryList";
    public static final String GET_STATELIST = BASE_URL + "getStateList";
    public static final String GET_CITYLIST = BASE_URL + "getCityList";
    public static final String VIEW_FITDAY = BASE_URL + "viewFitday";
    public static final String FORGOT_PASSWORD = BASE_URL + "forgot_password";
    public static final String RESET_PASSWORD = BASE_URL + "resetpassword";
    public static final String LOGIN = BASE_URL + "login";
    public static final String EDIT_PROFILE_FAN = BASE_URL + "editProfile";
    public static final String CHANGE_PASSWORD = BASE_URL + "changePassword";
    public static final String GET_NEWS = BASE_URL + "getNews";
    public static final String GET_DASHBOARD = BASE_URL + "getDashboard";
    public static final String CHANGE_TRACK = BASE_URL + "getDashboard";
    public static final String RACE_DETAILS = BASE_URL + "getRaceDetails";
    public static final String FAN_LIST = BASE_URL + "fanList";
    public static final String DRIVER_DETAILS = BASE_URL + "driverDetails";
    public static final String PRODUCT_LIST = BASE_URL + "productList";
    public static final String PRODUCT_DETAIL = BASE_URL + "productDetail";
    public static final String PURCHASE_PRODUCT = BASE_URL + "purchaseProduct";
    public static final String FAN_POINTS = BASE_URL + "fanPoints";
    public static final String SHOP_HISTORY = BASE_URL + "productHistory";
    public static final String PURCHASE_DRIVER = BASE_URL + "purchaseDriver";
    public static final String REFER_A_FRIEND = BASE_URL + "GetReferfriend";
    public static final String LIKE = "https://www.facebook.com/Short-1009566702472573/";
    public static final String HELP = "http://notosolutions.net/short_track/pages/help";
    public static final String TEAM_LIST = BASE_URL + "teamList";
    public static final String GET_NEARBY_FITDAYS = BASE_URL + "getNearByFitdays";
    public static final String USER_CATEGORY = BASE_URL + "UserCategory";
    public static final String FIT_DAY_UPLOAD = BASE_URL + "fitdayupload";
    public static final String BECOME_COACH = BASE_URL + "BecomeCoach";
    public static final String SAVE_ANSWERS = BASE_URL + "saveAnswers";
    public static final String CREATE_TEAM = BASE_URL + "createTeam";
    public static final String ADD_TEAM_MEMBER = BASE_URL + "addTeamMate";
    public static final String COACH_DOCUMENTS = BASE_URL + "coachDocuments";
    public static final String USER_RESPONSE = BASE_URL + "UserResponse";
    public static final String FIND_COACH = BASE_URL + "FindCoach";
    public static final String RADAR = BASE_URL + "Radar";
    public static final String COACH_ACTION = BASE_URL + "coachAction";
    public static final String USER_ACTION = BASE_URL + "userAction";
    public static final String GET_PROFILE = BASE_URL + "getProfile";
    public static final String EDIT_PROFILE = BASE_URL + "editProfile";
    public static final String FOLLOW_STATUS = BASE_URL + "followStatus";
    public static final String TRENDING_LIST = BASE_URL + "trendingList";
    public static final String NEWSCHEDULE = BASE_URL + "newSchedule";
    public static final String FETCH_MESSAGES = BASE_URL + "fetchMessages";
    public static final String FOLLOW = BASE_URL + "followStatus";
    public static final String NOTIFICATIONS =  BASE_URL + "Notifications";
    public static final String GET_COACH =  BASE_URL + "getCoach";
    public static final String SCHEDULED_LIST =  BASE_URL + "scheduledList";
    public static final String SCHEDULED_DATES =  BASE_URL + "scheduledDates";
    public static final String SCHEDULE_ACTION =  BASE_URL + "scheduleAction";
    public static final String COACHTRAININGVIDEO =  BASE_URL + "CoachTrainingVideo";
    public static final String ABUSE_REPORT =  BASE_URL + "AbuseReport";
    public static final String FITDAYCOUNT = BASE_URL + "FitdayCount";
    public static final String LOGHOURS = BASE_URL + "LogHours";
    public static final String SETTINGS = BASE_URL + "settings";
}

