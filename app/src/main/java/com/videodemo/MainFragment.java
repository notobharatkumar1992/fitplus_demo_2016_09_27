package com.videodemo;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Bharat on 09/27/2016.
 */
public class MainFragment extends Fragment {

    public static MyView mv;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mv = new MyView(getActivity());
        return mv;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                mv.mCanvas.drawBitmap(mv.mBitmap, 0, 0, mv.mBitmapPaint);
//                storeImage(getNewImageFilePath(), mv.mBitmap);
//            }
//        }, 3000);
    }

    public static void setPaintColor(int color) {
        if (mv != null)
            mv.setPaintColor(color);
    }

    public static Bitmap getPaintImage() {
        try {
            mv.mCanvas.drawBitmap(mv.mBitmap, 0, 0, mv.mBitmapPaint);
        } catch (Exception e) {
            com.fitplus.AppDelegate.LogE(e);
        }
        return mv.mBitmap;
    }

    public static String getNewImageFilePath() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "fitplus/SendFiles");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        Log.d("test", "new File created: getNewImageFilePath => " + uriSting);
        return uriSting;
    }

    public static void storeImage(String filePath, Bitmap image) {
        File pictureFile = new File(filePath);
        if (pictureFile == null) {
            Log.d("test", "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 0, fos);
            fos.close();
            Log.d("test", "storeImage => File saved successfully : " + filePath);
        } catch (FileNotFoundException e) {
            Log.d("test", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("test", "Error accessing file: " + e.getMessage());
        }
    }

    public static Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_4444);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        Log.d("test", "getBitmapFromView called and return result");
        return returnedBitmap;
    }
}
