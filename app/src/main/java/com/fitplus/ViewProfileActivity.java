package com.fitplus;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Async.LocationAddress;
import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.PagerAdapter;
import fragments.FitTeamFragments;
import fragments.VideoFragment;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.ChatModel;
import model.FitDayModel;
import model.PostAysnc_Model;
import model.UserDataModel;
import utils.Prefs;

/**
 * Created by admin on 26-07-2016.
 */
public class ViewProfileActivity extends FragmentActivity implements OnClickListener, AdapterView.OnItemClickListener, OnReciveServerResponse, OnDialogClickListener {


    public static final int FROM_TOGET = 1, FROM_VIEW = 2;

    ImageView display_pic, background_img, back;
    TextView name, address, description, tagline, followers, about_title, about_desc, get, follow;
    ViewPager video_list;
    private int login_id;
    private LinearLayout get_follow;
    private ViewPager sponcer;
    private PagerAdapter bannerPagerAdapter;
    private List<Fragment> bannerFragment = new ArrayList();

    LinearLayout pager_indicator;
    private int isfollowed;
    private ImageView img_loading;
    private Handler mHandler;
    private ScrollView scroll;
    public int getView = 0;
    int team_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_profile);
        login_id = getIntent().getExtras().getInt(Parameters.login_id);
        getView = getIntent().getExtras().getInt(Tags.FROM);
        team_id=getIntent().getIntExtra(Tags.team_id,0);
        initView();
        execute_getprofile();
    }

    private void initView() {
        scroll = (ScrollView) findViewById(R.id.scroll);
        background_img = (ImageView) findViewById(R.id.user_img);
        display_pic = (ImageView) findViewById(R.id.display_pic);
        back = (ImageView) findViewById(R.id.back);
        name = (TextView) findViewById(R.id.name);
        address = (TextView) findViewById(R.id.address);
        description = (TextView) findViewById(R.id.description);
        tagline = (TextView) findViewById(R.id.Tagline);
        followers = (TextView) findViewById(R.id.followers);
        about_title = (TextView) findViewById(R.id.about_title);
        about_desc = (TextView) findViewById(R.id.about_desc);
        video_list = (ViewPager) findViewById(R.id.video_list);
        get = (TextView) findViewById(R.id.get);
        get_follow = (LinearLayout) findViewById(R.id.get_follow);
        follow = (TextView) findViewById(R.id.follow);
        img_loading = (ImageView) findViewById(R.id.img_loading);
        if (login_id == new Prefs(ViewProfileActivity.this).getUserdata().userId) {
            get_follow.setVisibility(View.GONE);
        } else {
            get_follow.setVisibility(View.VISIBLE);
        }
        pager_indicator = (LinearLayout) findViewById(R.id.pager_indicator);
        follow.setOnClickListener(this);
        get.setOnClickListener(this);
        back.setOnClickListener(this);
        setUiPageViewController();
        if (getView == FROM_TOGET) {
            get.setVisibility(View.VISIBLE);

        } else {
            get.setVisibility(View.GONE);
        }

    }
    private ImageView[] dots;
    private int dotsCount, item_position = 0, selected_tab = 0, click_type = 0;
    private void setUiPageViewController() {
        pager_indicator.removeAllViews();
        dotsCount = bannerFragment.size();
        if (dotsCount > 0) {
            dots = new ImageView[dotsCount];
            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new ImageView(ViewProfileActivity.this);
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.tab));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(AppDelegate.getInstance(ViewProfileActivity.this).dp2px(15), AppDelegate.getInstance(ViewProfileActivity.this).dp2px(15));
                params.setMargins(AppDelegate.getInstance(ViewProfileActivity.this).dp2px(5), 0, AppDelegate.getInstance(ViewProfileActivity.this).dp2px(5), 0);
                pager_indicator.addView(dots[i], params);
            }
            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.tabactive));
        }
        video_list.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switchBannerPage(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        switchBannerPage(0);
    }

    private void switchBannerPage(int position) {
        if (dotsCount > 0) {
            for (int i = 0; i < dotsCount; i++) {
                dots[i].setImageResource(R.drawable.tab);
            }
            dots[position].setImageResource(R.drawable.tabactive);
        }
    }

    private void execute_getprofile() {
        try {
            if (AppDelegate.haveNetworkConnection(ViewProfileActivity.this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(ViewProfileActivity.this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(ViewProfileActivity.this).setPostParamsSecond(mPostArrayList, Parameters.user_id, login_id);
                AppDelegate.getInstance(ViewProfileActivity.this).setPostParamsSecond(mPostArrayList, Parameters.login_id, new Prefs(ViewProfileActivity.this).getUserdata().userId);

                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(ViewProfileActivity.this, ViewProfileActivity.this, ServerRequestConstants.GET_PROFILE,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(ViewProfileActivity.this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(ViewProfileActivity.this, "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.get:
                if (getView == FROM_TOGET) {
                    execute_GetCoach();
                }
                break;
            case R.id.follow:
                if (follow.isSelected()) {
                    follow.setSelected(false);
                    if (isfollowed == 0) {
                        execute_follow_status("follow");
                    } else {
                        execute_follow_status("unfollow");
                    }
                } else {
                    if (isfollowed == 0) {
                        execute_follow_status("follow");
                    } else {
                        execute_follow_status("unfollow");
                    }
                    follow.setSelected(true);
                }
                break;
            case R.id.back:
                finish();
                break;
        }
    }

    private void execute_GetCoach() {
        try {
            if (AppDelegate.haveNetworkConnection(ViewProfileActivity.this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(ViewProfileActivity.this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(ViewProfileActivity.this).setPostParamsSecond(mPostArrayList, Parameters.coach_id, login_id);
                AppDelegate.getInstance(ViewProfileActivity.this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(ViewProfileActivity.this).getUserdata().userId);
                AppDelegate.getInstance(ViewProfileActivity.this).setPostParamsSecond(mPostArrayList, Tags.team_id, team_id);

                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(ViewProfileActivity.this, ViewProfileActivity.this, ServerRequestConstants.GET_COACH,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(ViewProfileActivity.this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(ViewProfileActivity.this, "Please try again.", "Alert!!!");
        }
    }

    private void execute_follow_status(String status) {
        try {
            if (AppDelegate.haveNetworkConnection(ViewProfileActivity.this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(ViewProfileActivity.this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(ViewProfileActivity.this).setPostParamsSecond(mPostArrayList, Parameters.user_id, login_id);
                AppDelegate.getInstance(ViewProfileActivity.this).setPostParamsSecond(mPostArrayList, Parameters.follower_id, new Prefs(ViewProfileActivity.this).getUserdata().userId);
                AppDelegate.getInstance(ViewProfileActivity.this).setPostParamsSecond(mPostArrayList, Tags.status, status);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(ViewProfileActivity.this, ViewProfileActivity.this, ServerRequestConstants.FOLLOW,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(ViewProfileActivity.this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(ViewProfileActivity.this, "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.months:

                break;
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(ViewProfileActivity.this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(ViewProfileActivity.this, ViewProfileActivity.this.getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.GET_PROFILE)) {
            parsegetprofile(result);
        } else if (apiName.equals(ServerRequestConstants.FOLLOW)) {
            parseFOlLOW(result);
        } else if (apiName.equals(ServerRequestConstants.GET_COACH)) {
            parseGET_COACH(result);
        }
    }

    private void parseGET_COACH(String result) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(result);
            AppDelegate.showToast(ViewProfileActivity.this, jsonObject.getString(Tags.message));
            if (jsonObject.getInt(Tags.status) == 1 && jsonObject.getInt(Tags.dataFlow) == 1) {
                if(RadarActivity.mActivity!=null){
                    RadarActivity.mActivity.finish();
                }if(CoachListActivity.mActivity!=null){
                    CoachListActivity.mActivity.finish();
                }
                try {
                    ChatModel chatModel = new ChatModel((team_id));
                    if (CreateTeamActivity.onReciveSocketMessage != null) {
                        CreateTeamActivity.onReciveSocketMessage.setOnReciveSocketMessage("refresh", chatModel);
                    }
                    if(FitTeamFragments.mHandler!=null){
                        FitTeamFragments.mHandler.sendEmptyMessage(1);
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
               finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parseFOlLOW(String result) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(result);
            AppDelegate.showToast(ViewProfileActivity.this, jsonObject.getString(Tags.message));
            if (jsonObject.getInt(Tags.status) == 1 && jsonObject.getInt(Tags.dataFlow) == 1) {
                isfollowed = jsonObject.getInt("flag");
                if (isfollowed == 0) {
                    follow.setText(ViewProfileActivity.this.getResources().getString(R.string.follow));
                } else {
                    if (isfollowed == 1) {
                        follow.setText(ViewProfileActivity.this.getResources().getString(R.string.unfollow));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setloc(double latitude, double longitude) {
        LocationAddress.getAddressFromLocation(latitude, longitude, this, mHandler);
    }

    private void setHandlergeo() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 0:
                        break;
                    case 2:
                        setAddressFromGEOcoder(msg.getData());
                        break;
                }
            }
        };
    }

    private void setAddressFromGEOcoder(Bundle data) {
        address.setText(data.getString(Tags.country_param) + "");
    }

    private void parsegetprofile(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONObject obj = jsonObject.getJSONObject(Tags.response);
                JSONObject object = obj.getJSONObject("user");
                JSONArray fitday = obj.getJSONArray("fitday");
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.password = object.getString(Tags.password);
                userDataModel.created = object.getString(Tags.created);
                userDataModel.str_Gender = object.getString(Tags.gender);
                userDataModel.userId = object.getInt(Tags.user_id);
                userDataModel.dob = object.getString(Tags.birthdate);
                userDataModel.nickname = object.getString(Tags.nick_name);
                userDataModel.avtar = object.getString(Tags.avtar);
                userDataModel.views = object.getString(Tags.views);
                userDataModel.follower_count = object.getInt(Tags.follower_count);
                userDataModel.profile_visibility = object.getInt(Tags.profile_visibility);
                userDataModel.presentation_vedio = object.getString(Tags.presentation_vedio);
                userDataModel.bank_account = object.getString(Tags.bank_account);
                userDataModel.certificates = object.getString(Tags.certificates);
                userDataModel.country_id = object.getString(Tags.country_id);
                userDataModel.role_id = object.getInt(Tags.role_id);
                userDataModel.state_id = object.getString(Tags.state_id);
                userDataModel.city_id = object.getString(Tags.city_id);
                userDataModel.post_code = object.getString(Tags.post_code);
                userDataModel.sex_group = object.getString(Tags.sex_group);
                userDataModel.fat_status = object.getString(Tags.fat_status);
                userDataModel.type = object.getString(Tags.type);
                userDataModel.token = object.getString(Tags.token);
                userDataModel.is_login = object.getInt(Tags.is_login);
                userDataModel.is_social = object.getInt(Tags.is_social);
                userDataModel.is_verified = object.getInt(Tags.is_verified);
                userDataModel.latitude = object.getDouble(Tags.latitude);
                userDataModel.longitude = object.getDouble(Tags.longitude);
                userDataModel.about_me = object.getString(Tags.about_me);
                userDataModel.is_followed = object.getInt(Tags.is_followed);
                //AppDelegate.ShowDialogID(ViewProfileActivity.this, jsonObject.getString(Tags.message), "Alert", Tags.ok, ViewProfileFragment.this);
                isfollowed = userDataModel.is_followed;
                if (isfollowed == 0) {
                    follow.setText(ViewProfileActivity.this.getResources().getString(R.string.follow));
                } else {
                    if (isfollowed == 1) {
                        follow.setText(ViewProfileActivity.this.getResources().getString(R.string.unfollow));
                    }
                }
                setprofile(userDataModel);
                ArrayList<FitDayModel> fitDayModelArrayList = new ArrayList<>();
                //FitDayModel fitDayModel=new FitDayModel();
                for (int i = 0; i < fitday.length(); i++) {
                    JSONObject object1 = fitday.getJSONObject(i);
                    FitDayModel fitDayModel = new FitDayModel();
                    fitDayModel.id = object1.getInt(Tags.user_id);
                    fitDayModel.file_name = object1.getString(Tags.file_name);
                    fitDayModel.file_type = object1.getInt(Tags.file_type);
                    fitDayModel.user_id = object1.getInt(Parameters.user_id);
                    fitDayModel.view = object1.getInt(Tags.view);
                    fitDayModel.comment = object1.getString(Tags.comment);
                    fitDayModel.created = object1.getString(Tags.created);
                    fitDayModel.file_thumb = object1.getString(Tags.file_thumb);
                    fitDayModelArrayList.add(fitDayModel);
                }
                setfitday(fitDayModelArrayList);
            } else {
                if (jsonObject.getInt(Tags.status) == 0) {
                    AppDelegate.ShowDialog(ViewProfileActivity.this, jsonObject.getString(Tags.message), "alert");
                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
                    AppDelegate.showAlert(ViewProfileActivity.this, jsonObject.getString(Tags.message));
                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(ViewProfileActivity.this, "Response is not proper. Please try again later.");
            AppDelegate.LogE(e);
        }
    }

    private void setfitday(ArrayList<FitDayModel> fitDayModelArrayList) {
        for (int i = 0; i < fitDayModelArrayList.size(); i++) {
            Fragment fragment = new VideoFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.FitDayModel, fitDayModelArrayList.get(i));
            fragment.setArguments(bundle);
            bannerFragment.add(fragment);
        }
        AppDelegate.LogT("bannerFragment. size==>" + bannerFragment.size() + "");
        bannerPagerAdapter = new PagerAdapter(getSupportFragmentManager(), bannerFragment);
        video_list.setAdapter(bannerPagerAdapter);
        setUiPageViewController();
        scroll.fullScroll(View.FOCUS_UP);//if you move at the end of the scroll
        scroll.pageScroll(View.FOCUS_UP);//if you move at the middle of the scroll
        scroll.smoothScrollTo(0, 0);
    }

    private void setprofile(UserDataModel userDataModel) {
        if (userDataModel.avtar.isEmpty()) {

        } else {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
                    frameAnimation.setCallback(img_loading);
                    frameAnimation.setVisible(true, true);
                    frameAnimation.start();
                    ((Animatable) img_loading.getDrawable()).start();
                }
            });
            Picasso.with(ViewProfileActivity.this)
                    .load(userDataModel.avtar)
                    .placeholder(R.drawable.img) // optional
                    .error(R.drawable.img).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    display_pic.setImageBitmap(bitmap);
                    img_loading.setVisibility(View.GONE);
                    try {
                        background_img.setImageBitmap(AppDelegate.blurRenderScript(ViewProfileActivity.this, bitmap));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    AppDelegate.LogT("onBitmapFailed");
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    AppDelegate.LogT("onPrepareLoad");
                }
            });

        }
        name.setText((AppDelegate.isValidString(userDataModel.first_name) ? userDataModel.first_name : "") + " " + (AppDelegate.isValidString(userDataModel.last_name) ? userDataModel.last_name : ""));
        setHandlergeo();
        setloc(userDataModel.latitude, userDataModel.longitude);
        about_title.setText("About " + userDataModel.first_name + ":");
        about_desc.setText(userDataModel.about_me + "");
        followers.setText(String.valueOf(userDataModel.follower_count) + "  " + ViewProfileActivity.this.getResources().getString(R.string.followers));
        tagline.setText((AppDelegate.isValidString(userDataModel.fat_status) ? userDataModel.fat_status : "") + ", " + (AppDelegate.isValidString(userDataModel.sex_group) ? userDataModel.sex_group : "") + ", " + (AppDelegate.isValidString(userDataModel.type) ? userDataModel.type : " "));
    }

    @Override
    public void setOnDialogClickListener(String name) {
    }
}

