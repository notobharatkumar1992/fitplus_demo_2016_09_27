package com.fitplus;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import UI.CircleImageView;
import adapters.FitDayAdapter;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.FitDayModel;
import model.PostAysnc_Model;
import model.UserDataModel;
import utils.Prefs;
import utils.SpacesItemDecoration;
import utils.SpacesItemTransDecoration;

public class FitDayActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnDialogClickListener {
    Activity mActivity;
    private RecyclerView trending_list;
    private Prefs prefs;
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    private FitDayAdapter trending_Adapter;
    private ImageView back;
    Intent intent;
    private int id;
    private String name;
    private TextView title;
    private UserDataModel userDataModel;
    private Bundle bundle;
    private CircleImageView userimg;
    String img;
    public static Handler mHandler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fitday_list);
        mActivity = this;
        prefs = new Prefs(this);
        id = new Prefs(this).getIntValue("post_id", 0);
        name = new Prefs(this).getStringValue("snippet", "");
        img = new Prefs(this).getStringValue("avtar", "");
        AppDelegate.LogT("clickedsnippetvdfgdf " + new Prefs(this).getStringValue("avtar", "") + "checked");
        findIDs();
        execute_trendingList();
        setHandler();
    }

    private void execute_trendingList() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.user_id, id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.login_id, new Prefs(this).getUserdata().userId, ServerRequestConstants.Key_PostintValue);
                //  AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_token, AppDelegate.getValue(SignupActivity.this, Tags.REGISTRATION_ID), ServerRequestConstants.Key_PostintValue);
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.VIEW_FITDAY,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mActivity != null) {
            mActivity = null;
        }
    }

    private void findIDs() {
        trending_list = (RecyclerView) findViewById(R.id.trending);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(this);
        trending_list.setAdapter(null);
        title = (TextView)findViewById(R.id.title);
        title.setText(name + "");
        userimg = (CircleImageView) findViewById(R.id.userimg);
        userimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FitDayActivity.this, ViewProfileActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(Parameters.login_id, id);
                bundle.putInt(Tags.FROM, ViewProfileActivity.FROM_VIEW);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        if (!img.isEmpty()) {
            Picasso.with(FitDayActivity.this)
                    .load(img)
                    .placeholder(R.drawable.img) // optional
                    .error(R.drawable.img).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    userimg.setImageBitmap(bitmap);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    AppDelegate.LogT("onBitmapFailed");
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    AppDelegate.LogT("onPrepareLoad");
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.VIEW_FITDAY)) {
            parseTrendingList(result);
        }
    }

    private void parseTrendingList(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                ArrayList<FitDayModel> trending = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    FitDayModel userDataModel = new FitDayModel();
                    JSONObject object = jsonArray.getJSONObject(i);
                    userDataModel.id = object.getInt(Tags.user_id);
                    userDataModel.file_name = object.getString(Tags.file_name);
                    userDataModel.file_type = object.getInt(Tags.file_type);
                    userDataModel.user_id = object.getInt(Parameters.user_id);
                    userDataModel.view = object.getInt(Tags.view);
                    userDataModel.comment = object.getString(Tags.comment);
                    userDataModel.created = object.getString(Tags.created);
                    userDataModel.file_thumb = object.getString(Tags.file_thumb);
                    userDataModel.viewstatus = object.getInt(Tags.viewstatus);
                    trending.add(userDataModel);
                }
                setTrendingList(trending);
            } else {
                if (jsonObject.getInt(Tags.status) == 0) {
//                    AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.ok, FitDayActivity.this);
                    AppDelegate.showToast(FitDayActivity.this, jsonObject.getString(Tags.message));
                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
//                    AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.ok, FitDayActivity.this);
                    AppDelegate.showToast(FitDayActivity.this, jsonObject.getString(Tags.message));
                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(this, getResources().getString(R.string.response_error));
            AppDelegate.LogE(e);
        }
    }
    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 0:
                        break;
                    case 2:

                        break;
                    case 3:
                        execute_trendingList();
                        break;
                }
            }
        };
    }

    private void setTrendingList(ArrayList<FitDayModel> trending) {
        trending_list.setAdapter(null);
        trending_list.setPadding(AppDelegate.dpToPix(this, 5), AppDelegate.dpToPix(this, 5), AppDelegate.dpToPix(this, 5), AppDelegate.dpToPix(this, 5));
        trending_list.setHasFixedSize(true);
        AppDelegate.LogT("AppDelegate.dpToPix(this, 5)"+  AppDelegate.dpToPix(this, 5));
        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        trending_list.setLayoutManager(gaggeredGridLayoutManager);
        trending_list.addItemDecoration(new SpacesItemTransDecoration(AppDelegate.dpToPix(this, 5), AppDelegate.dpToPix(this, 5), AppDelegate.dpToPix(this, 60)));
        trending_Adapter = new FitDayAdapter(this, trending);
        trending_list.setAdapter(trending_Adapter);
    }

    @Override
    public void setOnDialogClickListener(String name) {

    }
}
