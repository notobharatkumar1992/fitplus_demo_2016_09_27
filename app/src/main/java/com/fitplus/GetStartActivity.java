package com.fitplus;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

import Constants.Tags;
import adapters.ImageAdapter;
import interfaces.OnListItemClickListener;

public class GetStartActivity extends AppCompatActivity implements OnListItemClickListener, View.OnClickListener {
    private LinearLayout pager_indicator;
    ViewPager viewpager;
    private ArrayList<Object> slider;
    private ImageAdapter imageAdapter;
    private ImageView[] dots;
    private int dotsCount, item_position = 0, selected_tab = 0, click_type = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_start);
        findIDs();
        AppDelegate.getHashKey(this);
        getToken();
    }

    private void getToken() {
        GCMClientManager pushClientManager = new GCMClientManager(this, getString(R.string.project_number));
        pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
            @Override
            public void onSuccess(String registrationId, boolean isNewRegistration) {
                Log.d("Registration id", registrationId);

                AppDelegate.save(GetStartActivity.this, registrationId, Tags.REGISTRATION_ID);
                //send this registrationId to your server
            }

            @Override
            public void onFailure(String ex) {
                super.onFailure(ex);
                Log.d("onFailure", ex);
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void findIDs() {
        viewpager = (ViewPager) findViewById(R.id.image);
        findViewById(R.id.get_start).setOnClickListener(this);
        pager_indicator = (LinearLayout) findViewById(R.id.pager_indicator);
        setImages();
    }

    private void setImages() {
        slider = new ArrayList<>();

        slider.add(R.drawable.getstart);
        slider.add(R.drawable.getstart);
        slider.add(R.drawable.getstart);
        imageAdapter = new ImageAdapter(slider, GetStartActivity.this);
        viewpager.setAdapter(imageAdapter);
        setUiPageViewController();
    }

    private void setUiPageViewController() {
        pager_indicator.removeAllViews();
        dotsCount = imageAdapter.getCount();
        if (dotsCount > 0) {
            dots = new ImageView[dotsCount];
            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new ImageView(this);
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.tab));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(AppDelegate.getInstance(this).dp2px(15), AppDelegate.getInstance(this).dp2px(15));
                params.setMargins(AppDelegate.getInstance(this).dp2px(5), 0, AppDelegate.getInstance(this).dp2px(5), 0);
                pager_indicator.addView(dots[i], params);
            }
            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.tabactive));
        }
        next();
    }

    private void next() {
        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switchBannerPage(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        switchBannerPage(0);
    }

    private void switchBannerPage(int position) {
        if (dotsCount > 0) {
            for (int i = 0; i < dotsCount; i++) {
                dots[i].setImageResource(R.drawable.tab);
            }
            dots[position].setImageResource(R.drawable.tabactive);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.get_start:
                Intent intent = new Intent(GetStartActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }
}
