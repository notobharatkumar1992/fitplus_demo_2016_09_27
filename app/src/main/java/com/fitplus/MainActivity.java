package com.fitplus;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.fitplus.service.ChatService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Async.LocationAddress;
import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import fragments.CalendarFragment;
import fragments.HomeFragment;
import fragments.NotificationFragments;
import fragments.RadarFragment;
import fragments.SettingsFragment;
import fragments.StatsFragment;
import fragments.ViewProfileFragment;
import interfaces.OnReciveServerResponse;
import model.Notification_Fragments_Model;
import model.PostAysnc_Model;
import utils.Prefs;
import utils.SlidingPaneLayout1;

public class MainActivity extends FragmentActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, OnReciveServerResponse {
    Bitmap bitmap;
    ImageView bar1, bar2, bar3, bar4, bar5, bar6, calendar_notification;
    TextView fit_days, inbox, stats, calendar, setting;
    carbon.widget.TextView inbox_notification;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude = 0, currentLongitude = 0;
    public LinearLayout side_panel;
    public SlidingPaneLayout1 mSlidingPaneLayout;
    public SlideMenuClickListener menuClickListener = new SlideMenuClickListener();
    public static final int PANEL_FITDAYS = 0, PANEL_INBOX = 1, PANEL_STATS = 2, PANEL_CALENDAR = 3, PANEL_SETTINGS = 4, PANEL_LOGOUT = 5, PANEL_VIEWPROFILE = 6;
    public boolean isSlideOpen = false;
    public int ratio, ride_cancel_time = 30;
    public float init = 0.0f;
    private TextView username;
    private ImageView background_img;
    UI.CircleImageView display_pic;
    public static MainActivity mainActivity;
    public static Handler mHandler;
    private TextView address;
    public static int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainActivity = this;
        initView();
        initGPS();
        setHandlergeo();
        setloc(new Prefs(this).getUserdata().latitude, new Prefs(this).getUserdata().longitude);
        new LoadImage().execute(new Prefs(this).getUserdata().avtar);
        // set_image();
//        AppDelegate.showGPSAlert(this);
        showGPSalert();
        AppDelegate.LogT("GCM => " + String.valueOf(AppDelegate.getValue(this, Tags.REGISTRATION_ID)).trim());
        startService(new Intent(this, ChatService.class));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ChatService.mSocket.emit("send message", "{\"sender_id\":20,\"team_id\":53,\"message\":\"hellp\",\"status\":\"1\"}");
            }
        }, 1000);
        mHandler.sendEmptyMessage(3);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler = null;
    }

    /**
     * Start invite user for application
     **/
    private Dialog dialog;

    public void showInviteList(final Activity mActivity) {
        final Bundle mBundle = new Bundle();
        mBundle.putString("title", "Invitation for app");
        mBundle.putString("description", "Download this app.");
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        ListView modeList = new ListView(mActivity);
        String[] stringArray = new String[]{"  Facebook", "  Phone Contact", "  Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(mActivity, android.R.layout.simple_list_item_1, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        builder.setTitle("Invite User from");
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
        dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        openFacebook(mActivity, mBundle);
                        break;
                    case 1:
                        dialog.dismiss();
                        showContactAlertDialog(mActivity);
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    private AlertDialog.Builder alertDialogBuilder;
    private String[] alertDialogItems = new String[]{};
    private boolean[] selectedTrueFalse = new boolean[]{};
    private List<String> ItemsIntoList;

    public void showContactAlertDialog(Activity mActivity) {
        alertDialogBuilder = new AlertDialog.Builder(mActivity);
        ItemsIntoList = fetchPhoneContacts(this);
        AppDelegate.LogT("ItemsIntoList.size => " + ItemsIntoList.size());
        alertDialogItems = new String[ItemsIntoList.size()];
        selectedTrueFalse = new boolean[ItemsIntoList.size()];
        for (int i = 0; i < ItemsIntoList.size(); i++) {
            alertDialogItems[i] = ItemsIntoList.get(i);
            selectedTrueFalse[i] = false;
        }
        alertDialogBuilder.setMultiChoiceItems(alertDialogItems, selectedTrueFalse, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                selectedTrueFalse[which] = isChecked;
            }
        });
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setTitle("Select contact");
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int a = 0;
                while (a < selectedTrueFalse.length) {
                    boolean value = selectedTrueFalse[a];
                    if (value) {

                    }
                    a++;
                }
            }
        });

        alertDialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog dialog = alertDialogBuilder.create();
        dialog.show();
    }

    public ArrayList<String> fetchPhoneContacts(Context mContext) {
        ArrayList<String> contactList = new ArrayList<>();
        ContentResolver cr = mContext.getContentResolver(); //Activity/Application android.content.Context
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String contactNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        contactList.add(contactNumber);
                        break;
                    }
                    pCur.close();
                }
            } while (cursor.moveToNext());
        }
        return contactList;
    }

    private ShareDialog shareDialog;
    public static CallbackManager callbackManager;
    private boolean isCalledOnce = false;

    public void openFacebook(final Activity mActivity, final Bundle mBundle) {
        FacebookSdk.sdkInitialize(mActivity);
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(mActivity, Arrays.asList("public_profile", "user_friends", "user_birthday", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        AppDelegate.LogFB("onSuccess = " + loginResult.getAccessToken());
                        shareFacebook(mActivity, mBundle);
                    }

                    @Override
                    public void onCancel() {
                        AppDelegate.LogFB("login cancel");
                        if (AccessToken.getCurrentAccessToken() != null)
                            LoginManager.getInstance().logOut();
                        if (!isCalledOnce) {
                            isCalledOnce = true;
                            openFacebook(mActivity, mBundle);
                        }
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        AppDelegate.LogFB("login error = " + exception.getMessage());
                        if (exception.getMessage().contains("CONNECTION_FAILURE")) {
                            AppDelegate.hideProgressDialog(mActivity);
                        } else if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                                if (!isCalledOnce) {
                                    isCalledOnce = true;
                                    openFacebook(mActivity, mBundle);
                                }
                            }
                        }
                    }
                });
    }

    private void shareFacebook(final Activity mActivity, final Bundle mBundle) {
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle(mBundle.getString("title"))
//                    .setImageUrl(imageURI)
//                    .setImageUrl(Uri.parse(productModel.image_1))
                    .setContentDescription(mBundle.getString("description"))
//                    .setContentUrl(Uri.parse(productModel.image_1))
                    .build();
            shareDialog.show(linkContent);
        }
    }

    /**
     * End of user invite
     **/


    private void execute_NotificationList() {
        try {
            if (AppDelegate.haveNetworkConnection(MainActivity.this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(MainActivity.this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(MainActivity.this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(MainActivity.this).getUserdata().userId);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(MainActivity.this, this, ServerRequestConstants.NOTIFICATIONS, mPostArrayList, null);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(MainActivity.this, "Please try again.", "Alert!!!");
        }
    }

    private void showGPSalert() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(MainActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            mGoogleApiClient.connect();
        }
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    MainActivity.this, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    public static MainActivity getInstance() {
        return mainActivity;
    }

    private void initGPS() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        AppDelegate.LogT("mGoogleApiClient Initialited");
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(500)        // 10 seconds, in milliseconds
                .setFastestInterval(500); // 10 second, in milliseconds
        AppDelegate.LogT("mLocationRequest Initialited");
    }

    private void initView() {
        mSlidingPaneLayout = (SlidingPaneLayout1) findViewById(R.id.mSlidingPaneLayout);
        mSlidingPaneLayout.setSliderFadeColor(getResources().getColor(android.R.color.transparent));
        mSlidingPaneLayout.setPanelSlideListener(new SliderListener());
        side_panel = (LinearLayout) findViewById(R.id.side_panel);
        bar1 = (ImageView) findViewById(R.id.bar1);
        bar2 = (ImageView) findViewById(R.id.bar2);
        bar3 = (ImageView) findViewById(R.id.bar3);
        bar4 = (ImageView) findViewById(R.id.bar4);
        bar5 = (ImageView) findViewById(R.id.bar5);
        bar6 = (ImageView) findViewById(R.id.bar6);
        username = (TextView) findViewById(R.id.username);
        address = (TextView) findViewById(R.id.address);
        findViewById(R.id.rl_inbox).setOnClickListener(this);
        findViewById(R.id.rl_fitdays).setOnClickListener(this);
        findViewById(R.id.rl_calendar).setOnClickListener(this);
        findViewById(R.id.rl_stats).setOnClickListener(this);
        findViewById(R.id.rl_setting).setOnClickListener(this);
        findViewById(R.id.rl_logout).setOnClickListener(this);
        background_img = (ImageView) findViewById(R.id.background_img);
        display_pic = (UI.CircleImageView) findViewById(R.id.user_img);
        display_pic.setOnClickListener(this);
        inbox_notification = (carbon.widget.TextView) findViewById(R.id.inbox_notification);
        background_img.setImageBitmap(AppDelegate.blurRenderScript(this, ((BitmapDrawable) getResources().getDrawable(R.drawable.userimg)).getBitmap()));
        AppDelegate.showFragmentAnimation(getSupportFragmentManager(), new HomeFragment(), R.id.main_content);
        setInitailSideBar(0);
        username.setText(new Prefs(this).getUserdata().first_name);
    }

    private void setloc(double latitude, double longitude) {
        LocationAddress.getAddressFromLocation(latitude, longitude, this, mHandler);
    }

    private void setHandlergeo() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 0:

                        break;
                    case 2:
                        setAddressFromGEOcoder(msg.getData());
                        break;
                    case 3:
                        execute_NotificationList();
                        break;
                    case 4:
                        if (counter > 0) {
                            inbox_notification.setVisibility(View.VISIBLE);
                            inbox_notification.setText(String.valueOf(counter));
                        } else {
                            inbox_notification.setVisibility(View.INVISIBLE);
                        }
                        break;
                }
            }
        };
    }

    private void setAddressFromGEOcoder(Bundle data) {
        address.setText(data.getString(Tags.country_param) + "");
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Disconnect from API onPause()
        try {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (com.google.android.gms.location.LocationListener) MainActivity.this);
                mGoogleApiClient.disconnect();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        AppDelegate.LogT("onConnected Initialited");
        if (location == null) {
            try {
                AppDelegate.LogT("onConnected Initialited== null");
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (com.google.android.gms.location.LocationListener) MainActivity.this);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            AppDelegate.LogT("latLng = " + currentLatitude + ", " + currentLongitude);
            new Prefs(this).putStringValue(Tags.TAG_LAT, String.valueOf(currentLatitude));
            new Prefs(this).putStringValue(Tags.TAG_LONG, String.valueOf(currentLongitude));
        }
    }

    private void set_image() {
        if (new Prefs(this).getUserdata().avtar.isEmpty()) {
            AppDelegate.LogT("set_image = null");
        } else {
            Picasso.with(this)
                    .load(new Prefs(this).getUserdata().avtar)
                    .placeholder(R.drawable.img) // optional
                    .error(R.drawable.img).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    AppDelegate.LogT("MainActivity onBitmapLoaded");
                    display_pic.setImageBitmap(bitmap);
                    try {
                        background_img.setImageBitmap(AppDelegate.blurRenderScript(MainActivity.this, bitmap));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    AppDelegate.LogT("MainActivity onBitmapFailed");
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    AppDelegate.LogT("MainActivity onPrepareLoad");
                }
            });
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                AppDelegate.LogT("onConnectionFailed Initialited" + connectionResult);
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);

            } catch (IntentSender.SendIntentException e) {
                AppDelegate.LogE(e);
            }
        } else {
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppDelegate.hideKeyBoard(this);
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
        AppDelegate.LogGP("onLocationChanged latLng = " + currentLatitude + ", " + currentLongitude);
        new Prefs(this).putStringValue(Tags.TAG_LAT, String.valueOf(currentLatitude));
        new Prefs(this).putStringValue(Tags.TAG_LONG, String.valueOf(currentLongitude));
        try {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (com.google.android.gms.location.LocationListener) MainActivity.this);
                mGoogleApiClient.disconnect();
                AppDelegate.LogGP("Fused Location api disconnect called");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @SuppressWarnings("deprecation")
    public void setInitailSideBar(int value) {
        bar1.setVisibility(View.INVISIBLE);
        bar2.setVisibility(View.INVISIBLE);
        bar3.setVisibility(View.INVISIBLE);
        bar4.setVisibility(View.INVISIBLE);
        bar5.setVisibility(View.INVISIBLE);
        bar6.setVisibility(View.INVISIBLE);
        switch (value) {
            case 0:
                bar1.setVisibility(View.VISIBLE);
                break;
            case 1:
                bar2.setVisibility(View.VISIBLE);
                break;
            case 2:
                bar3.setVisibility(View.VISIBLE);
                break;
            case 3:
                bar4.setVisibility(View.VISIBLE);
                break;
            case 4:
                bar5.setVisibility(View.VISIBLE);
                break;
            case 5:
                bar6.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_inbox:
                toggleSlider();
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof StatsFragment))
                    menuClickListener.onItemClick(null, v, PANEL_INBOX, PANEL_INBOX);
                AppDelegate.LogT("clicked tab");
                break;

            case R.id.rl_calendar:
                toggleSlider();
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof CalendarFragment))
                    menuClickListener.onItemClick(null, v, PANEL_CALENDAR, PANEL_CALENDAR);
                AppDelegate.LogT("clicked tab");
                break;

            case R.id.rl_stats:
                toggleSlider();
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof StatsFragment))
                    menuClickListener.onItemClick(null, v, PANEL_STATS, PANEL_STATS);
                AppDelegate.LogT("clicked tab");
                break;

            case R.id.rl_fitdays:
                toggleSlider();
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof HomeFragment))
                    menuClickListener.onItemClick(null, v, PANEL_FITDAYS, PANEL_FITDAYS);
                AppDelegate.LogT("clicked tab");
                break;
            case R.id.rl_setting:
                toggleSlider();
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof SettingsFragment))
                    menuClickListener.onItemClick(null, v, PANEL_SETTINGS, PANEL_SETTINGS);
                AppDelegate.LogT("clicked tab");
                break;

            case R.id.rl_logout:
                toggleSlider();
                new Prefs(this).clearTempPrefs();
                new Prefs(this).clearSharedPreference();
                new Prefs(this).setRemembered("false");
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finish();
                break;
            case R.id.user_img:
                toggleSlider();
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof ViewProfileFragment))
                    menuClickListener.onItemClick(null, v, PANEL_VIEWPROFILE, PANEL_VIEWPROFILE);
                AppDelegate.LogT("clicked tab display_pic");
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof RadarFragment) {
            AppDelegate.LogT("RadarFragment called");
            if (RadarFragment.onReciveServerResponse != null)
                RadarFragment.onReciveServerResponse.setOnReciveResult("test", Tags.back);
        } else if (getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof HomeFragment) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (apiName.equals(ServerRequestConstants.NOTIFICATIONS)) {
            parseNOTIFICATIONS(result);
        }
    }

    private void parseNOTIFICATIONS(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONArray response = jsonObject.getJSONArray(Tags.response);
                counter = 0;
                ArrayList<Notification_Fragments_Model> team_list = new ArrayList<>();
                for (int i = 0; i < response.length(); i++) {
                    Notification_Fragments_Model userDataModel = new Notification_Fragments_Model();
                    JSONObject object = response.getJSONObject(i);
                    userDataModel.team_id = object.getInt(Tags.team_id);
                    userDataModel.notify_id = object.getInt(Tags.id);
                    userDataModel.user_id = object.getInt("user_id");
                    userDataModel.notificaton_type = object.getString(Tags.notificaton_type);
                    userDataModel.action_taken = object.getInt(Tags.action_taken);
                    userDataModel.message = object.getString(Tags.message);
                    userDataModel.status = object.getInt(Tags.status);
                    userDataModel.teamMembers = object.getInt(Tags.teamMembers);
                    userDataModel.teamname = object.getString(Tags.teamname);
                    userDataModel.team_avtar = object.getString(Tags.team_avtar);
                    userDataModel.username_first = object.getString(Tags.username_first);
                    userDataModel.username_second = object.getString(Tags.username_second);
                    userDataModel.username_third = object.getString(Tags.username_third);
                    userDataModel.FirstAvtar = object.getString(Tags.FirstAvtar);
                    userDataModel.SecondAvtar = object.getString(Tags.SecondAvtar);
                    userDataModel.ThirdAvtar = object.getString(Tags.ThirdAvtar);
                    userDataModel.coach_id = object.has(Tags.coach_id) ? object.getInt(Tags.coach_id) : 0;
                    team_list.add(userDataModel);
                    if (userDataModel.action_taken == 0) {
                        counter++;
                    }
                }
                mHandler.sendEmptyMessage(4);
//                showTeamlist(team_list);
                // AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, GlobalMapFragments.this);
            } else {
//                if (jsonObject.getInt(Tags.status) == 0) {
//                    AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, FitTeamFragments.this);
//                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null/*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
//                    AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, FitTeamFragments.this);
//                }
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view,
                                final int position, long id) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(position);
                }
            }, 600);
        }
    }

    private void displayView(int position) {
        AppDelegate.hideKeyBoard(MainActivity.this);
        if (PANEL_VIEWPROFILE != position)
            setInitailSideBar(position);
        Fragment fragment = null;
        switch (position) {
            case PANEL_FITDAYS:
                fragment = new HomeFragment();
                break;
            case PANEL_INBOX:
                fragment = new NotificationFragments();
                break;
            case PANEL_STATS:
                fragment = new StatsFragment();
                break;
            case PANEL_CALENDAR:
                fragment = new CalendarFragment();
                break;
            case PANEL_SETTINGS:
//                fragment = new SettingsFragment();
                startActivity(new Intent(MainActivity.this, EditProfileActivity.class));
                break;
            case PANEL_LOGOUT:
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finish();
                break;
            case PANEL_VIEWPROFILE:
                fragment = new ViewProfileFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(Parameters.login_id, new Prefs(this).getUserdata().userId);
                if (fragment != null) {
                    AppDelegate.showFragmentAnimation(getSupportFragmentManager(), fragment, R.id.main_content, bundle, null);
                }
                break;
        }
        if (fragment != null) {
            AppDelegate.showFragmentAnimation(getSupportFragmentManager(), fragment, R.id.main_content);
        } else if (position != 6) {
            AppDelegate.LogE("Error in creating fragment");
        }
    }

    class SliderListener extends SlidingPaneLayout1.SimplePanelSlideListener {
        @Override
        public void onPanelOpened(View panel) {
            if (!isSlideOpen) {
                isSlideOpen = true;
            }
            AppDelegate.hideKeyBoard(MainActivity.this);
        }

        @Override
        public void onPanelClosed(View panel) {
            if (isSlideOpen) {
                isSlideOpen = false;
            }
        }

        @Override
        public void onPanelSlide(View view, float slideOffset) {
            ratio = (int) -(0 - (slideOffset) * 255);
            whileSlide(ratio);
        }
    }

    public void toggleSlider() {
        if (mSlidingPaneLayout != null)
            if (!mSlidingPaneLayout.isOpen()) {
                mSlidingPaneLayout.openPane();
            } else {
                mSlidingPaneLayout.closePane();
            }
    }

    public void whileSlide(int view) {
        int newalfa = 255 - view;
        float subvalue = newalfa / 2.55f;
        float f = (100f - subvalue) * 0.01f;
        Animation alphaAnimation = new AlphaAnimation(init, f);
        alphaAnimation.setDuration(0);
        alphaAnimation.setFillAfter(true);
        init = f;
        side_panel.startAnimation(alphaAnimation);
    }

    public class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {

            if (image != null) {
                display_pic.setImageBitmap(image);
                try {
                    background_img.setImageBitmap(AppDelegate.blurRenderScript(MainActivity.this, image));
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }

            } else {

            }
        }
    }
}
