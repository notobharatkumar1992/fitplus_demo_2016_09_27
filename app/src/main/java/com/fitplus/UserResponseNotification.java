package com.fitplus;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.FitDayAdapter;
import fragments.FitTeamFragments;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.ChatModel;
import model.PostAysnc_Model;
import model.UserResponseModel;
import utils.Prefs;

public class UserResponseNotification extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnDialogClickListener {
    ImageView img_loading, img_loading1, img_loading2, img_loading3;
    Activity mActivity;
    private RecyclerView trending_list;
    private Prefs prefs;
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    private FitDayAdapter trending_Adapter;
    private ImageView back, FirstAvtar, SecondAvtar, ThirdAvtar;
    TextView username_first, username_second, username_third;
    Intent intent;
    private int id;
    private TextView team_name, team_count, do_not_disturb;
    private Bundle bundle;
    private UserResponseModel userResponse;
    int param = 0;
    private ImageView avtar_img;
    public static final String USER = "user";
    public static final String COACH = "coach";
    public static final String FINDCOACH_TEAM = "findcoach_Team";
    public static final String FINDCOACH_TEAM_ACCEPT = "findcoach_team_accept";
    public static final String GETCOACH_TEAM = "getcoach_Team";
    public static final String COACH_ADDED = "coach_added";
    public static final String NEWSCHEDULE = "NewSchedule";
    public static final String LOCKHOURS_TEAM = "lockhours_team";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_notification);
        mActivity = this;
        prefs = new Prefs(this);
        bundle = getIntent().getExtras();
        userResponse = bundle.getParcelable("coupan_detail");
        //AppDelegate.LogT("user id" + id + "checked");
        findIDs();
        TextView title = (TextView) findViewById(R.id.title);
        if (userResponse.request_type.equalsIgnoreCase(USER)) {
            title.setText("User request for team.");
            team_count.setVisibility(View.VISIBLE);
            do_not_disturb.setVisibility(View.VISIBLE);
        } else if (userResponse.request_type.equalsIgnoreCase(COACH)) {
            title.setText("Coach request for Team");
            team_count.setVisibility(View.VISIBLE);
            do_not_disturb.setVisibility(View.VISIBLE);
        } else if (userResponse.request_type.equalsIgnoreCase(FINDCOACH_TEAM)) {
            title.setText("Permission to find coach");
            team_count.setVisibility(View.VISIBLE);
            do_not_disturb.setVisibility(View.GONE);
        } else if (userResponse.request_type.equalsIgnoreCase(GETCOACH_TEAM)) {
            title.setText("Permission to get coach");
            team_count.setVisibility(View.VISIBLE);
            do_not_disturb.setVisibility(View.GONE);
        } else if (userResponse.request_type.equalsIgnoreCase(NEWSCHEDULE)) {
            title.setText("Permission to schedule next workout");
            team_count.setVisibility(View.VISIBLE);
            do_not_disturb.setVisibility(View.GONE);
        }else if (userResponse.request_type.equalsIgnoreCase(LOCKHOURS_TEAM)) {
            title.setText("Permission to schedule next workout");
            team_count.setVisibility(View.VISIBLE);
            do_not_disturb.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mActivity != null) {
            mActivity = null;
        }
    }

    private void findIDs() {
        findViewById(R.id.accept).setOnClickListener(this);
        findViewById(R.id.reject).setOnClickListener(this);
        do_not_disturb = (TextView) findViewById(R.id.do_not_disturb);
        do_not_disturb.setOnClickListener(this);
        avtar_img = (ImageView) findViewById(R.id.img_content);
        team_name = (TextView) findViewById(R.id.txt_team_name);
        team_count = (TextView) findViewById(R.id.txt_team_members);
        team_name.setText(userResponse.team_name);
        team_count.setText("Members : " + userResponse.team_members);
        FirstAvtar = (ImageView) findViewById(R.id.img_user_one);
        SecondAvtar = (ImageView) findViewById(R.id.img_user_two);
        ThirdAvtar = (ImageView) findViewById(R.id.img_user_three);
        username_first = (TextView) findViewById(R.id.txt_user_one);
        username_second = (TextView) findViewById(R.id.txt_user_two);
        username_third = (TextView) findViewById(R.id.txt_user_three);
        img_loading = (ImageView) findViewById(R.id.img_loading);
        img_loading1 = (ImageView) findViewById(R.id.img_loading1);
        img_loading2 = (ImageView) findViewById(R.id.img_loading2);
        img_loading3 = (ImageView) findViewById(R.id.img_loading3);
        AppDelegate.LogT("in User response notification First avtar==>" + userResponse.FirstAvtar + "   Firstname==" + userResponse.username_first + " second avtar==" + userResponse.SecondAvtar + "  second name==" + userResponse.username_second + "  third avtar==" + userResponse.ThirdAvtar + " third name== " + userResponse.username_third + "   ");
        set_image();
    }

    private void set_image() {
        if (userResponse.team_avtar.isEmpty()) {
            AppDelegate.LogT("set_image = null");
        } else {
            img_loading.setVisibility(View.VISIBLE);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
                    frameAnimation.setCallback(img_loading);
                    frameAnimation.setVisible(true, true);
                    frameAnimation.start();
                    ((Animatable) img_loading.getDrawable()).start();
                }
            });
            Picasso.with(this)
                    .load(userResponse.team_avtar)
                    .placeholder(R.drawable.img) // optional
                    .error(R.drawable.img).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    AppDelegate.LogT("MainActivity onBitmapLoaded");
                    avtar_img.setImageBitmap(bitmap);
                    img_loading.setVisibility(View.GONE);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    AppDelegate.LogT("MainActivity onBitmapFailed");
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    AppDelegate.LogT("MainActivity onPrepareLoad");
                }
            });
        }
        if (!userResponse.FirstAvtar.isEmpty() || AppDelegate.isValidString(userResponse.username_first)) {
            img_loading1.setVisibility(View.VISIBLE);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    AnimationDrawable frameAnimation = (AnimationDrawable) img_loading1.getDrawable();
                    frameAnimation.setCallback(img_loading1);
                    frameAnimation.setVisible(true, true);
                    frameAnimation.start();
                    ((Animatable) img_loading1.getDrawable()).start();
                }
            });
            AppDelegate.LogT("userResponse.FirstAvtar");
            FirstAvtar.setVisibility(View.VISIBLE);
            username_first.setText(userResponse.username_first + "");
            Picasso.with(this)
                    .load(userResponse.FirstAvtar)
                    .placeholder(R.drawable.img) // optional
                    .error(R.drawable.img).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    AppDelegate.LogT("MainActivity onBitmapLoaded");
                    FirstAvtar.setImageBitmap(bitmap);
                    img_loading1.setVisibility(View.GONE);

                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    AppDelegate.LogT("MainActivity onBitmapFailed");
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    AppDelegate.LogT("MainActivity onPrepareLoad");
                }
            });
        } else {
            FirstAvtar.setVisibility(View.INVISIBLE);
        }
        if (!userResponse.SecondAvtar.isEmpty() || AppDelegate.isValidString(userResponse.username_second)) {
            img_loading2.setVisibility(View.VISIBLE);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    AnimationDrawable frameAnimation = (AnimationDrawable) img_loading2.getDrawable();
                    frameAnimation.setCallback(img_loading2);
                    frameAnimation.setVisible(true, true);
                    frameAnimation.start();
                    ((Animatable) img_loading2.getDrawable()).start();
                }
            });
            AppDelegate.LogT("userResponse.SecondAvtar");
            SecondAvtar.setVisibility(View.VISIBLE);
            username_second.setText(userResponse.username_second + "");
            Picasso.with(this)
                    .load(userResponse.SecondAvtar)
                    .placeholder(R.drawable.img) // optional
                    .error(R.drawable.img).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    AppDelegate.LogT("MainActivity onBitmapLoaded");
                    SecondAvtar.setImageBitmap(bitmap);
                    img_loading2.setVisibility(View.GONE);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    AppDelegate.LogT("MainActivity onBitmapFailed");
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    AppDelegate.LogT("MainActivity onPrepareLoad");
                }
            });
        } else {
            SecondAvtar.setVisibility(View.INVISIBLE);
        }
        if (!userResponse.ThirdAvtar.isEmpty() || AppDelegate.isValidString(userResponse.username_third)) {
            img_loading3.setVisibility(View.VISIBLE);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    AnimationDrawable frameAnimation = (AnimationDrawable) img_loading3.getDrawable();
                    frameAnimation.setCallback(img_loading3);
                    frameAnimation.setVisible(true, true);
                    frameAnimation.start();
                    ((Animatable) img_loading3.getDrawable()).start();
                }
            });
            AppDelegate.LogT("userResponse.ThirdAvtar");
            ThirdAvtar.setVisibility(View.VISIBLE);
            username_third.setText(userResponse.username_third + "");
            Picasso.with(this)
                    .load(userResponse.ThirdAvtar)
                    .placeholder(R.drawable.img) // optional
                    .error(R.drawable.img).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    AppDelegate.LogT("MainActivity onBitmapLoaded");
                    ThirdAvtar.setImageBitmap(bitmap);
                    img_loading3.setVisibility(View.GONE);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    AppDelegate.LogT("MainActivity onBitmapFailed");
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    AppDelegate.LogT("MainActivity onPrepareLoad");
                }
            });
        } else {
            ThirdAvtar.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.accept:
                if (prefs.getUserdata() != null && AppDelegate.isValidString(String.valueOf(prefs.getUserdata().userId))) {
                    param = Parameters.accept;
                    execute(Parameters.accept);
                } else {
                    AppDelegate.ShowDialog(this, "Please Login .", "Alert");
                }
                break;
            case R.id.reject:
                if (prefs.getUserdata() != null && AppDelegate.isValidString(String.valueOf(prefs.getUserdata().userId))) {
                    param = Parameters.reject;
                    execute(Parameters.reject);
                } else {
                    AppDelegate.ShowDialog(this, "Please Login.", "Alert");
                }
                break;
            case R.id.do_not_disturb:
                if (prefs.getUserdata() != null && AppDelegate.isValidString(String.valueOf(prefs.getUserdata().userId))) {
                    param = Parameters.do_not_disturb;
                    execute(Parameters.do_not_disturb);
                } else {
                    AppDelegate.ShowDialog(this, "Please Login .", "Alert");
                }
                break;
        }
    }

    private void execute(int type) {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj = null;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(this).getUserdata().userId, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.team_id, userResponse.team_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.devicetype, 2, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.notify_id, Integer.parseInt(userResponse.notify_id), ServerRequestConstants.Key_PostintValue);
                if (userResponse.request_type.equalsIgnoreCase("user")) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.type, type, ServerRequestConstants.Key_PostintValue);
                    mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.USER_RESPONSE,
                            mPostArrayList, null);
                } else if (userResponse.request_type.equalsIgnoreCase("COACH")) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.action, type, ServerRequestConstants.Key_PostintValue);
                    mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.COACH_ACTION,
                            mPostArrayList, null);
                } else if (userResponse.request_type.equalsIgnoreCase(FINDCOACH_TEAM)) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.type, FINDCOACH_TEAM);
                    //AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.coach_id, 0);

                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.action, type, ServerRequestConstants.Key_PostintValue);
                    mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.USER_ACTION,
                            mPostArrayList, null);
                } else if (userResponse.request_type.equalsIgnoreCase(GETCOACH_TEAM)) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.type, GETCOACH_TEAM);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.coach_id, userResponse.coach_id);
                    //AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.coachname, userResponse.coachname);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.action, type, ServerRequestConstants.Key_PostintValue);
                    mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.USER_ACTION,
                            mPostArrayList, null);
                } else if (userResponse.request_type.equalsIgnoreCase(NEWSCHEDULE)) {
                    //AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.coachname, userResponse.coachname);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.action, type, ServerRequestConstants.Key_PostintValue);
                    mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.SCHEDULE_ACTION,
                            mPostArrayList, null);
                }else if (userResponse.request_type.equalsIgnoreCase(LOCKHOURS_TEAM)) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.type, LOCKHOURS_TEAM);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.action, type, ServerRequestConstants.Key_PostintValue);
                    mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.USER_ACTION,
                            mPostArrayList, null);
                }
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.USER_RESPONSE)) {
            if(FitTeamFragments.mHandler!=null){
                FitTeamFragments.mHandler.sendEmptyMessage(1);
            }
            parseTrendingList(result);
        } else if (apiName.equals(ServerRequestConstants.COACH_ACTION)) {
            parseTrendingList(result);
        } else if (apiName.equals(ServerRequestConstants.USER_ACTION)) {
            parseTrendingList(result);
        } else if (apiName.equals(ServerRequestConstants.SCHEDULE_ACTION)) {
            parseTrendingList(result);
        }
    }

    private void parseTrendingList(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                AppDelegate.LogT("jsonObject.getInt(Tags.status) == 1" + jsonObject.getString(Tags.message));
//                AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.ok, UserResponseNotification.this);
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
                try {
                    ChatModel chatModel = new ChatModel(Integer.parseInt(userResponse.team_id));
                    if (CreateTeamActivity.onReciveSocketMessage != null) {
                        CreateTeamActivity.onReciveSocketMessage.setOnReciveSocketMessage("refresh", chatModel);
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                if (param == 1) {
//                startActivity(new Intent(UserResponseNotification.this, RadarFragment.class));
                    finish();
                } else if (param == 2 || param == 3) {
                    finish();
                }
            } else {
                if (jsonObject.getInt(Tags.status) == 0) {
//                    AppDelegate.showToast(this, jsonObject.getString(Tags.message));
                    AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.cancel, UserResponseNotification.this);
//                    finish();
                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
//                    AppDelegate.showToast(this, jsonObject.getString(Tags.message));
                    AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.cancel, UserResponseNotification.this);
//                    finish();
                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(this, getResources().getString(R.string.response_error));
            AppDelegate.LogE(e);
        }

    }


    @Override
    public void setOnDialogClickListener(String name) {
        if (name.equalsIgnoreCase(Tags.ok)) {
            if (param == 1) {
//                startActivity(new Intent(UserResponseNotification.this, RadarFragment.class));
                finish();
            } else if (param == 2 || param == 3) {
                finish();
            }
        } else if (name.equalsIgnoreCase(Tags.cancel)) {
            finish();
        }
    }
}
