package com.fitplus;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.PostAysnc_Model;
import model.UserDataModel;
import utils.Prefs;
import utils.ProgressD;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnDialogClickListener {
    private Intent intent;
    private String Email;
    EditText email_id;
    TextView submit;
    private boolean result;
    public static ForgotPasswordActivity forgotpassword;
    private String identifier;
    private int status;
    int varificationcode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password);
        forgotpassword = this;
        initializeObjects();
    }


    private void initializeObjects() {
        email_id = (EditText) findViewById(R.id.email_id);
        submit = (TextView) findViewById(R.id.submit);
        submit.setOnClickListener(this);
        identifier = AppDelegate.getValue(this, Tags.IDENTIFIER);
        findViewById(R.id.back).setOnClickListener(this);
    }

    private boolean validate_emailid(String email) {
        boolean result = true;
        try {
            if (email == null) {
                result = false;
                AppDelegate.ShowDialog(ForgotPasswordActivity.this, "Please enter Email Id !!!", "Oops!!!");
            }
        } catch (Exception e) {
            intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
            startActivity(intent);
        }
        if (!email.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
            result = false;
            AppDelegate.ShowDialog(ForgotPasswordActivity.this, "Invalid Email Address", "Oops!!!");
        }
        return result;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppDelegate.hideKeyBoard(this);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        if (forgotpassword != null) {
            forgotpassword = null;
        }
        ProgressD.hideProgress_dialog();
        System.gc();
        super.onDestroy();
    }

    private void ForgotPassword() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.email, Email);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.FORGOT_PASSWORD,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                if (forgotpassword != null)
                    forgotpassword.finish();
                break;
            case R.id.submit:
                if (submit.getText().toString().equalsIgnoreCase("SUBMIT")) {
                    Email = email_id.getText().toString();
                    result = validate_emailid(Email);
                    if (result == true) {
                        ForgotPassword();
                    }
                } else {
                    check_varification_code();
                }
        }
    }

    private void check_varification_code() {
        int varify = Integer.parseInt(email_id.getText().toString());
        if (varify == varificationcode) {
//            AppDelegate.ShowDialogID(ForgotPasswordActivity.this, "Verification successful." + "", "Alert!!!", "verify", this);
            AppDelegate.showToast(ForgotPasswordActivity.this, "Verification successful.");
            Intent intent = new Intent(ForgotPasswordActivity.this, ResetPasswordActivity.class);
            startActivity(intent);
            finish();
        } else {
//            AppDelegate.ShowDialogID(ForgotPasswordActivity.this, "Please enter correct verification code." + "", "Alert!!!", "notverify", this);
            AppDelegate.showToast(ForgotPasswordActivity.this, "Please enter correct verification code.");
            email_id.setText("");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, "Service Time Out!!!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.FORGOT_PASSWORD)) {
            AppDelegate.save(this, result, Parameters.saveForgot);
            parseForgot();
        }
    }

    private void parseForgot() {
        String response = AppDelegate.getValue(this, Parameters.saveForgot);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
            status = jsonObject.getInt("status");
            String message = jsonObject.getString("message");
//            AppDelegate.ShowDialogID(ForgotPasswordActivity.this, message + "", "Alert!!!", "ForgotPasswordActivity", this);

            AppDelegate.showToast(ForgotPasswordActivity.this, jsonObject.getString("message"));
            if (status == 1) {
                email_id.setHint("Enter Varification Code");
                email_id.setText("");
                submit.setText("Send");
            } else {
                email_id.setText("");
            }

            if (status == 1) {
                JSONObject jsonArray = jsonObject.getJSONObject(Tags.response);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.userId = jsonArray.getInt("user_id");
                varificationcode = jsonArray.getInt("verification_code");
                new Prefs(this).setUserData(userDataModel);
            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setOnDialogClickListener(String name) {
        if (name.equals("ForgotPasswordActivity")) {
            if (status == 1) {
                email_id.setHint("Enter Varification Code");
                email_id.setText("");
                submit.setText("Send");
            } else {
                email_id.setText("");
            }
        } else if (name.equalsIgnoreCase("verify")) {
            Intent intent = new Intent(ForgotPasswordActivity.this, ResetPasswordActivity.class);
            startActivity(intent);
            finish();
        } else if (name.equalsIgnoreCase("notverify")) {
            email_id.setText("");
        }
    }
}
