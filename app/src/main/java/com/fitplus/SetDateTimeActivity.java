package com.fitplus;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import UI.CustomTimePickerDialog;
import butterknife.ButterKnife;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.PostAysnc_Model;
import model.TeamListModel;
import model.UserDataModel;
import utils.Prefs;
import utils.decorators.CurrentDateDecorator;
import utils.decorators.EventDecorator;
import utils.decorators.MySelectorDecorator;
import utils.decorators.OneDayDecorator;

/**
 * Created by admin on 26-07-2016.
 */
public class SetDateTimeActivity extends AppCompatActivity implements OnClickListener, AdapterView.OnItemClickListener, OnReciveServerResponse, OnDialogClickListener {
    RelativeLayout toolbar;
    TextView toolbar_title;
    ImageView tool_menu;
    RecyclerView team_list;
    private Bundle bundle;
    private CustomTimePickerDialog timePicker;
    private int selectfromhour, selectfromminute;
    private String timeset;
    private TextView time;
    // private CompactCalendarView date;
    private String setdate;
    private int team_id_type;
    private TextView month, txt_hour, txt_monthly;
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    private RelativeLayout rel_tab_bar;
    private Dialog builder;
    MaterialCalendarView date;
    private SimpleDateFormat month_date;
    private  OneDayDecorator oneDayDecorator = new OneDayDecorator(this);
    @Nullable
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_datetime);
        ButterKnife.bind(this);
        bundle = getIntent().getExtras();
        team_id_type = bundle.getInt(Tags.Find_Coach);
        initView();
        show_dialog();
    }


    private void initView() {
        toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        rel_tab_bar = (RelativeLayout) findViewById(R.id.rel_tab_bar);
        toolbar_title = (TextView) findViewById(R.id.title);
        tool_menu = (ImageView) findViewById(R.id.back);
        tool_menu.setOnClickListener(this);
        time = (TextView) findViewById(R.id.time);
        txt_hour = (TextView) findViewById(R.id.hour);
        txt_monthly = (TextView) findViewById(R.id.monthly);
        month = (TextView) findViewById(R.id.month);
        txt_hour.setOnClickListener(this);
        txt_monthly.setOnClickListener(this);
        Calendar calender = Calendar.getInstance();
        Date currentLocalTime = calender.getTime();
        DateFormat dates = new SimpleDateFormat("HH:mm");
        String localTime = dates.format(currentLocalTime);
        time.setText(localTime+"");
//        date = (CompactCalendarView) findViewById(R.id.date);
        date=(MaterialCalendarView)findViewById(R.id.date);
        date.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                oneDayDecorator.setDate(date.getDate());
                Date calender = date.getDate();
                setdate = new SimpleDateFormat("yyyy-MM-dd").format(calender).toString();
                Log.e("Selected date", setdate + "");
                widget.invalidateDecorators();
                set_time();
            }
        });
        date.setShowOtherDates(MaterialCalendarView.SHOW_ALL);

        date.setTopbarVisible(true);
        time=(TextView)findViewById(R.id.time);
        Calendar cal = Calendar.getInstance();
        month_date = new SimpleDateFormat("MMMM yyyy");
        String month_name = month_date.format(cal.getTime());
        month.setText("" + month_name);
        date.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
                Calendar calendar=Calendar.getInstance();
                calendar.set(Calendar.MONTH,date.getMonth() );
                calendar.set(Calendar.YEAR,date.getYear() );
                month_date = new SimpleDateFormat("MMMM yyyy");
                String month_name = month_date.format(calendar.getTime());
                month.setText(month_name+"");
            }
        });
        date.state().edit().setMinimumDate(Calendar.getInstance().getTime())
                //.setMaximumDate(instance2.getTime())
                .commit();

        date.addDecorators(
                new MySelectorDecorator(this),
                //new HighlightWeekendsDecorator(),
                oneDayDecorator
        );
        Calendar instance = Calendar.getInstance();
        CalendarDay day = CalendarDay.from(instance.getTime());
        ArrayList<CalendarDay> calenderdates = new ArrayList<>();
        calenderdates.add(day);
        new DateSimulator(calenderdates).execute();
//        date.setListener(new CompactCalendarView.CompactCalendarViewListener() {
//            @Override
//            public void onDayClick(Date dateClicked) {
//                setdate = new SimpleDateFormat("yyyy-MM-dd").format(dateClicked).toString();
//                set_time();
//                AppDelegate.LogT("setdate=>>>>>" + setdate);
//            }
//
//            @Override
//            public void onMonthScroll(Date firstDayOfNewMonth) {
//                month.setText(dateFormatForMonth.format(firstDayOfNewMonth));
//            }
//        });
        time.setOnClickListener(this);
        findViewById(R.id.done).setOnClickListener(this);
        rel_tab_bar.setVisibility(View.VISIBLE);
        setselection(0);
    }
    private class DateSimulator extends AsyncTask<Void, Void, List<CalendarDay>> {
        ArrayList<CalendarDay> calenderdates;

        public DateSimulator(ArrayList<CalendarDay> calenderdates) {
            this.calenderdates = calenderdates;
        }

        @Override
        protected List<CalendarDay> doInBackground(@NonNull Void... voids) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return calenderdates;
        }

        @Override
        protected void onPostExecute(@NonNull List<CalendarDay> calendarDays) {
            super.onPostExecute(calendarDays);

            if (isFinishing()) {
                return;
            }
            Log.v("test==", calendarDays + "");
            date.addDecorator(new CurrentDateDecorator(SetDateTimeActivity.this, calendarDays));
        }
    }
    private void show_dialog() {
        if (builder != null && builder.isShowing()) {
            builder.dismiss();
        }
        builder = new Dialog(this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setContentView(R.layout.findcoach_alert);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView ok;
        builder.setCancelable(false);
        ok = (TextView) builder.findViewById(R.id.yes);
        builder.show();
        // builder.show();
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
            }
        });
    }
    public void setselection(int value) {
        txt_hour.setSelected(false);
        txt_monthly.setSelected(false);
        txt_hour.setTextColor(getResources().getColor(R.color.white));
        txt_monthly.setTextColor(getResources().getColor(R.color.white));
        switch (value) {
            case 0:
                txt_hour.setSelected(true);
                txt_hour.setTextColor(getResources().getColor(R.color.txt_color_gray_3));
                break;
            case 1:
                txt_monthly.setSelected(true);
                txt_monthly.setTextColor(getResources().getColor(R.color.txt_color_gray_3));
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.monthly:
                setselection(1);
                break;
            case R.id.hour:
                setselection(0);
                break;
            case R.id.back:
                finish();
                break;
            case R.id.time:
                AppDelegate.LogT("time clicked");
                set_time();
                break;
            case R.id.done:
                if (AppDelegate.isValidString(timeset) && AppDelegate.isValidString(setdate)) {
                    AppDelegate.showDialog_twoButtons(this, getResources().getString(R.string.find_a_coach), Tags.Find_Coach, this);
                } else if (!AppDelegate.isValidString(setdate)) {
                    AppDelegate.ShowDialog(this, "Please select date.", "Time out!!!");
                } else {
                    if (!AppDelegate.isValidString(setdate)) {
                        AppDelegate.ShowDialog(this, "Please select time.", "Time out!!!");
                    }
                }
                break;
        }
    }

    private void execute_find_coach() {
        int status = 1;
        if (txt_hour.isSelected()) {
            status = 1;
        } else if (txt_monthly.isSelected()) {
            status = 2;
        }
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(this).getUserdata().userId);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.latitude, new Prefs(this).getStringValue(Tags.TAG_LAT, ""));
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.longitude, new Prefs(this).getStringValue(Tags.TAG_LONG, ""));
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.team_id, team_id_type, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.training_date, setdate + " " + timeset);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.deviceType, 2, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.training_period, status, ServerRequestConstants.Key_PostintValue);
                mPostAsyncObj = new PostAsync(this, SetDateTimeActivity.this, ServerRequestConstants.FIND_COACH,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(this, this.getResources().getString(R.string.try_again), "Alert!!!");
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {

        }
    }

    private void set_time() {
        timePicker = new CustomTimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                Calendar calender = Calendar.getInstance();
                calender.set(Calendar.HOUR, selectedHour);
                calender.set(Calendar.MINUTE, selectedMinute);
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                String test = sdf.format(calender.getTime());
                selectfromhour = selectedHour;
                selectfromminute = selectedMinute;
                time.setText(test+"");
                timeset =test;

            }
        }, selectfromhour, selectfromminute, false);//Yes 24 hour time
        timePicker.show();

        AppDelegate.LogT("date=>..." + timeset);
    }


    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, this.getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.FIND_COACH)) {
            parseCreateTeamList(result);
        } else if (apiName.equals(ServerRequestConstants.CREATE_TEAM)) {
            parseCreateTeamList(result);
        }
    }

    private void parseCreateTeamList(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1 && jsonObject.getInt(Tags.dataFlow) == 1) {
                JSONObject host_user = jsonObject.getJSONObject(Tags.HOST_USER);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.first_name = host_user.getString(Tags.first_name);
                userDataModel.last_name = host_user.getString(Tags.last_name);
                userDataModel.email = host_user.getString(Tags.email);
                userDataModel.password = host_user.getString(Tags.password);
                userDataModel.created = host_user.getString(Tags.created);
                userDataModel.str_Gender = host_user.getString(Tags.gender);
                userDataModel.userId = host_user.getInt(Tags.user_id);
                userDataModel.dob = host_user.getString(Tags.birthdate);
                userDataModel.nickname = host_user.getString(Tags.nick_name);
                userDataModel.avtar = host_user.getString(Tags.avtar);
                userDataModel.views = host_user.getString(Tags.views);
                userDataModel.follower_count = host_user.getInt(Tags.follower_count);
                userDataModel.profile_visibility = host_user.getInt(Tags.profile_visibility);
                userDataModel.presentation_vedio = host_user.getString(Tags.presentation_vedio);
                userDataModel.bank_account = host_user.getString(Tags.bank_account);
                userDataModel.certificates = host_user.getString(Tags.certificates);
                userDataModel.country_id = host_user.getString(Tags.country_id);
                userDataModel.role_id = host_user.getInt(Tags.role_id);
                userDataModel.state_id = host_user.getString(Tags.state_id);
                userDataModel.city_id = host_user.getString(Tags.city_id);
                userDataModel.post_code = host_user.getString(Tags.post_code);
                userDataModel.sex_group = host_user.getString(Tags.sex_group);
                userDataModel.fat_status = host_user.getString(Tags.fat_status);
                userDataModel.type = host_user.getString(Tags.type);
                userDataModel.token = host_user.getString(Tags.token);
                userDataModel.is_login = host_user.getInt(Tags.is_login);
                userDataModel.is_social = host_user.getInt(Tags.is_social);
                userDataModel.is_verified = host_user.getInt(Tags.is_verified);
                userDataModel.latitude = host_user.getDouble(Tags.latitude);
                userDataModel.longitude = host_user.getDouble(Tags.longitude);
                userDataModel.block_on = host_user.getString(Tags.block_on);
                userDataModel.block_for = host_user.getString(Tags.block_for);
                TeamListModel teamListModel = new TeamListModel();
                JSONObject host_team = jsonObject.getJSONObject(Tags.HOST_TEAM);
                teamListModel.team_id = host_team.getInt(Tags.id);
                teamListModel.team_name = host_team.getString(Tags.team_name);
                teamListModel.objective = host_team.getString(Tags.objective);
                teamListModel.first_member_id = host_team.getInt(Tags.first_member_id);
                teamListModel.second_member_id = host_team.getInt(Tags.second_member_id);
                teamListModel.third_member_id = host_team.getInt(Tags.third_member_id);
                teamListModel.coach_id = host_team.getInt(Tags.coach_id);
                teamListModel.latitude = host_team.getDouble(Tags.latitude);
                teamListModel.longitude = host_team.getDouble(Tags.longitude);
                teamListModel.created = host_team.getString(Tags.created);
                teamListModel.team_avtar = host_team.getString(Tags.team_avtar);
            }

            AppDelegate.showToast(this, jsonObject.getString(Tags.message));
            finish();
        } catch (Exception e) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.response_error), "");
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnDialogClickListener(String name) {
        if (name.equalsIgnoreCase(Tags.Find_Coach)) {
            execute_find_coach();
        }
    }


}
