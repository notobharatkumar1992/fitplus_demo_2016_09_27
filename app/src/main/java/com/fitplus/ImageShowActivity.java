package com.fitplus;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.crashlytics.android.Crashlytics;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnReciveServerResponse;
import io.fabric.sdk.android.Fabric;
import model.FitDayModel;
import model.PostAysnc_Model;
import model.ScheduleModel;
import utils.Prefs;

public class ImageShowActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse {
    private Intent mainIntent;
    public static Activity mActivity;

    ImageView imge;
    private Bundle bundle;
    private FitDayModel fitday;
    TextView comment;
    ImageView txt_report_abuse;
    int abuse = 0;
    private Dialog builder;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
            /*.showImageForEmptyUri(R.drawable.dummy_user_icon)
            .showImageOnFail(fallback)
            .showImageOnLoading(fallback).*/build();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.image);
        mActivity = null;
        bundle = getIntent().getExtras();
        fitday = bundle.getParcelable("FitDay");
        abuse = getIntent().getIntExtra(Tags.abuse, 0);
        initView();
    }

    private void initView() {
        imge = (ImageView) findViewById(R.id.image);
        comment = (TextView) findViewById(R.id.comment);
        comment.setText(fitday.comment + "");
        txt_report_abuse = (ImageView) findViewById(R.id.txt_report_abuse);
        txt_report_abuse.setOnClickListener(this);
        if (abuse == 1) {
            txt_report_abuse.setVisibility(View.VISIBLE);
            if (fitday.viewstatus == 1) {
                execute_viewStatus();
            }
        } else {
            txt_report_abuse.setVisibility(View.GONE);
        }
        if (fitday != null) {
            try {
                imageLoader.displayImage(fitday.file_name, imge, options);
                /*AppDelegate.getInstance(this).getImageLoader().get(fitday.file_name, new ImageLoader.ImageListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AppDelegate.LogE(error);
                    }

                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                        if (response.getBitmap() != null) {
                            imge.setImageBitmap(response.getBitmap());
                        }
                    }
                });*/
            } catch (Exception e) {
                AppDelegate.LogE(e);
                finish();
            }
        }
    }

    private void execute_viewStatus() {
        try {
            if (AppDelegate.haveNetworkConnection(ImageShowActivity.this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(ImageShowActivity.this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(ImageShowActivity.this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(this).getUserdata().userId);
                AppDelegate.getInstance(ImageShowActivity.this).setPostParamsSecond(mPostArrayList, Parameters.fitday_id, fitday.id);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(ImageShowActivity.this, ImageShowActivity.this, ServerRequestConstants.FITDAYCOUNT,
                        mPostArrayList, null);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(ImageShowActivity.this, getResources().getString(R.string.try_again), "Alert!!!");
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mActivity != null) {
            mActivity = null;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_report_abuse:
                show_dialog();
                break;
        }
    }

    private void show_dialog() {
        if (builder != null && builder.isShowing()) {
            builder.dismiss();
        }
        builder = new Dialog(this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setContentView(R.layout.report_abuse);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView ok, cancel;
        final EditText et_comment;
        builder.setCancelable(false);
        cancel = (TextView) builder.findViewById(R.id.no);
        ok = (TextView) builder.findViewById(R.id.yes);
        et_comment = (EditText) builder.findViewById(R.id.et_comment);
        builder.show();
        // builder.show();
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppDelegate.isValidString(et_comment.getText().toString())) {
                    execute_Report_abuse();
                    builder.dismiss();
                } else {
                    AppDelegate.showToast(ImageShowActivity.this, getResources().getString(R.string.abuse_validation));
                }
            }
        });
    }

    private void execute_Report_abuse() {
        try {
            if (AppDelegate.haveNetworkConnection(ImageShowActivity.this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(ImageShowActivity.this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(ImageShowActivity.this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(this).getUserdata().userId);
                AppDelegate.getInstance(ImageShowActivity.this).setPostParamsSecond(mPostArrayList, Parameters.fitday_id, fitday.id);
                AppDelegate.getInstance(ImageShowActivity.this).setPostParamsSecond(mPostArrayList, Parameters.user_comments, fitday.id);

                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(ImageShowActivity.this, ImageShowActivity.this, ServerRequestConstants.ABUSE_REPORT,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(ImageShowActivity.this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(ImageShowActivity.this, getResources().getString(R.string.try_again), "Alert!!!");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, this.getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.ABUSE_REPORT)) {
            parse_ABUSE_REPORT(result);
        } else if (apiName.equals(ServerRequestConstants.FITDAYCOUNT)) {
            if (FitDayActivity.mHandler != null) {
                FitDayActivity.mHandler.sendEmptyMessage(3);
            }
        }
    }


    private void parse_ABUSE_REPORT(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            AppDelegate.showToast(this, jsonObject.getString(Tags.message));
        } catch (Exception e) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.response_error), "");
            AppDelegate.LogE(e);
        }
    }
}
