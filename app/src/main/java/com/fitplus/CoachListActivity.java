package com.fitplus;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.TrendingAdapter;
import fragments.FitTeamFragments;
import interfaces.OnDialogClickListener;
import interfaces.OnListItemClickListener;
import interfaces.OnReciveServerResponse;
import model.ChatModel;
import model.PostAysnc_Model;
import model.RadarModel;
import model.UserDataModel;
import utils.Prefs;
import utils.SpacesItemDecoration;

public class CoachListActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnDialogClickListener, OnListItemClickListener {

    public static Activity mActivity;
    private RecyclerView trending_list;
    private Prefs prefs;
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    private TrendingAdapter trending_Adapter;
    private ImageView back;
    ArrayList<RadarModel> radarModel;
    private Bundle bundle;
    private ArrayList<RadarModel> become_coach;
    private ImageView userimg;
    int team_id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trending_list);
        mActivity = this;
        findIDs();
        prefs = new Prefs(this);
        team_id=getIntent().getIntExtra(Tags.team_id,0);
        if (AppDelegate.isValidString(AppDelegate.getValue(this, Tags.radarresponse))) {
            parseaddTeammember(AppDelegate.getValue(this, Tags.radarresponse));
        }
    }

    private void parseaddTeammember(String result) {
        try {
            ArrayList<RadarModel> radarmodel = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONArray json = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < json.length(); i++) {
                    JSONObject obj = json.getJSONObject(i);
                    RadarModel radar = new RadarModel();
                    radar.id = obj.getInt(Tags.id);
                    radar.request_id = obj.getInt(Tags.request_id);
                    radar.coach_id = obj.getInt(Tags.coach_id);
                    radar.coach_action = obj.getInt(Tags.coach_action);
                    radar.final_status = obj.getInt(Tags.final_status);
                    radar.followers = obj.getInt(Tags.followers);
                    radar.distance = obj.has(Tags.distance) ? obj.getDouble(Tags.distance) : 0;
                    radar.status = obj.getInt(Tags.status);
                    JSONObject object = obj.getJSONObject("user");
                    UserDataModel userDataModel = new UserDataModel();
                    userDataModel.first_name = object.getString(Tags.first_name);
                    userDataModel.last_name = object.getString(Tags.last_name);
                    userDataModel.email = object.getString(Tags.email);
                    userDataModel.password = object.getString(Tags.password);
                    userDataModel.created = object.getString(Tags.created);
                    userDataModel.str_Gender = object.getString(Tags.gender);
                    userDataModel.userId = object.getInt(Tags.user_id);
                    userDataModel.dob = object.getString(Tags.birthdate);
                    userDataModel.nickname = object.getString(Tags.nick_name);
                    userDataModel.avtar = object.has(Tags.avtar) ? object.getString(Tags.avtar) : "";
                    userDataModel.avtar_thumb = object.has(Tags.avtar_thumb) ? object.getString(Tags.avtar_thumb) : "http://www.sjafhasfajfh";
                    userDataModel.views = object.getString(Tags.views);
                    userDataModel.follower_count = object.getInt(Tags.follower_count);
                    userDataModel.profile_visibility = object.getInt(Tags.profile_visibility);
                    userDataModel.presentation_vedio = object.getString(Tags.presentation_vedio);
                    userDataModel.bank_account = object.getString(Tags.bank_account);
                    userDataModel.certificates = object.getString(Tags.certificates);
                    userDataModel.country_id = object.getString(Tags.country_id);
                    userDataModel.role_id = object.getInt(Tags.role_id);
                    userDataModel.state_id = object.getString(Tags.state_id);
                    userDataModel.city_id = object.getString(Tags.city_id);
                    userDataModel.post_code = object.getString(Tags.post_code);
                    userDataModel.sex_group = object.getString(Tags.sex_group);
                    userDataModel.fat_status = object.getString(Tags.fat_status);
                    userDataModel.type = object.getString(Tags.type);
                    userDataModel.token = object.getString(Tags.token);
                    userDataModel.is_login = object.getInt(Tags.is_login);
                    userDataModel.is_social = object.getInt(Tags.is_social);
                    userDataModel.is_verified = object.getInt(Tags.is_verified);
                    userDataModel.latitude = object.getDouble(Tags.latitude);
                    userDataModel.longitude = object.getDouble(Tags.longitude);
                    radar.coach_profile = userDataModel;
                    radarmodel.add(radar);
                }
                setTrendingList(radarmodel);
            } else {

            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mActivity != null) {
            mActivity = null;
        }
    }

    private void findIDs() {
        trending_list = (RecyclerView) findViewById(R.id.trending);

        back = (ImageView) findViewById(R.id.back);
        userimg = (ImageView) findViewById(R.id.userimg);
        userimg.setVisibility(View.GONE);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.VIEW_FITDAY)) {
            new Prefs(this).clearTempPrefs();
            parseTrendingList(result);
        }else   if (apiName.equals(ServerRequestConstants.GET_COACH)) {
            parseGET_COACH(result);
        }
    }

    private void parseTrendingList(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                ArrayList<UserDataModel> trending = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    UserDataModel userDataModel = new UserDataModel();
                    JSONObject object = jsonArray.getJSONObject(i);
                    userDataModel.userId = object.getInt(Tags.user_id);
                    userDataModel.first_name = object.getString(Tags.first_name);
                    userDataModel.last_name = object.getString(Tags.last_name);
                    userDataModel.email = object.getString(Tags.email);
                    userDataModel.password = object.getString(Tags.password);
                    userDataModel.created = object.getString(Tags.created);
                    userDataModel.str_Gender = object.getString(Tags.gender);
                    userDataModel.userId = object.getInt(Tags.user_id);
                    userDataModel.dob = object.getString(Tags.birthdate);
                    userDataModel.nickname = object.getString(Tags.nick_name);
                    userDataModel.avtar = object.getString(Tags.avtar);
                    userDataModel.avtar_thumb = object.getString(Tags.avtar_thumb);
                    userDataModel.views = object.getString(Tags.views);
                    userDataModel.follower_count = object.getInt(Tags.follower_count);
                    userDataModel.profile_visibility = object.getInt(Tags.profile_visibility);
                    userDataModel.presentation_vedio = object.getString(Tags.presentation_vedio);
                    userDataModel.bank_account = object.getString(Tags.bank_account);
                    userDataModel.certificates = object.getString(Tags.certificates);
                    userDataModel.country_id = object.getString(Tags.country_id);
                    userDataModel.role_id = object.getInt(Tags.role_id);
                    userDataModel.state_id = object.getString(Tags.state_id);
                    userDataModel.city_id = object.getString(Tags.city_id);
                    userDataModel.post_code = object.getString(Tags.post_code);
                    userDataModel.sex_group = object.getString(Tags.sex_group);
                    userDataModel.fat_status = object.getString(Tags.fat_status);
                    userDataModel.type = object.getString(Tags.type);
                    userDataModel.token = object.getString(Tags.token);
                    userDataModel.is_login = object.getInt(Tags.is_login);
                    userDataModel.is_social = object.getInt(Tags.is_social);
                    userDataModel.is_verified = object.getInt(Tags.is_verified);
                    userDataModel.latitude = object.getDouble(Tags.latitude);
                    userDataModel.longitude = object.getDouble(Tags.longitude);
                    trending.add(userDataModel);
                }
                // setTrendingList(trending);
//                AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.ok, CoachListActivity.this);
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
            } else {
                if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
//                    AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.ok, CoachListActivity.this);
                    AppDelegate.showToast(this, jsonObject.getString(Tags.message));
                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(this, getResources().getString(R.string.response_error));
            AppDelegate.LogE(e);
        }

    }

    private ArrayList<RadarModel> trending = new ArrayList<>();

    private void setTrendingList(ArrayList<RadarModel> trending) {
        this.trending = trending;
        trending_list.setPadding(AppDelegate.dpToPix(this, 5), AppDelegate.dpToPix(this, 5), AppDelegate.dpToPix(this, 5), AppDelegate.dpToPix(this, 5));
        trending_list.setHasFixedSize(true);
        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        trending_list.setLayoutManager(gaggeredGridLayoutManager);
        trending_list.addItemDecoration(new SpacesItemDecoration(AppDelegate.dpToPix(this, 5)));
        trending_Adapter = new TrendingAdapter(this, trending, this);
        trending_list.setAdapter(trending_Adapter);

        try {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    trending_Adapter.notifyDataSetChanged();
                    trending_list.invalidate();
                }
            }, 2000);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }
    private void execute_GetCoach(int coach_id) {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.coach_id, coach_id);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(this).getUserdata().userId);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.team_id, team_id);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(this,this , ServerRequestConstants.GET_COACH,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    private void parseGET_COACH(String result) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(result);
            AppDelegate.showToast(this, jsonObject.getString(Tags.message));
            if (jsonObject.getInt(Tags.status) == 1 && jsonObject.getInt(Tags.dataFlow) == 1) {
                if(RadarActivity.mActivity!=null){
                    RadarActivity.mActivity.finish();
                }if(CoachListActivity.mActivity!=null){
                    CoachListActivity.mActivity.finish();
                }
                try {
                    ChatModel chatModel = new ChatModel((team_id));
                    if (CreateTeamActivity.onReciveSocketMessage != null) {
                        CreateTeamActivity.onReciveSocketMessage.setOnReciveSocketMessage("refresh", chatModel);
                    }
                    if(FitTeamFragments.mHandler!=null){
                        FitTeamFragments.mHandler.sendEmptyMessage(1);
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void setOnDialogClickListener(String name) {
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        try {
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        if (name.equalsIgnoreCase("name") && trending != null && trending.size() > position) {
            Intent intent = new Intent(CoachListActivity.this, ViewProfileActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt(Parameters.login_id, trending.get(position).coach_id);
            bundle.putInt(Tags.FROM, ViewProfileActivity.FROM_TOGET);
            intent.putExtra(Tags.team_id,team_id);
            intent.putExtras(bundle);
            startActivity(intent);
        }else if (name.equalsIgnoreCase("get") && trending != null && trending.size() > position) {
           execute_GetCoach(trending.get(position).coach_id);

            }
    }
}
