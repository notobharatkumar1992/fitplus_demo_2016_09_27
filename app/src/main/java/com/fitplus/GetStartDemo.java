package com.fitplus;

import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.util.ArrayList;

import Constants.Tags;
import adapters.ImageAdapter;
import adapters.SplashPagerAdapter;
import interfaces.OnListItemClickListener;
import utils.MyViewPager;

public class GetStartDemo extends AppCompatActivity implements OnListItemClickListener, View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    private LinearLayout pager_indicator;
    MyViewPager viewpager;
    private ArrayList<Object> slider;
    private ImageAdapter imageAdapter;
    private SplashPagerAdapter splashPagerAdapter;
    private ImageView[] dots;
    private int dotsCount, item_position = 0, selected_tab = 0, click_type = 0;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_start_demo);
        findIDs();
        AppDelegate.getHashKey(this);
        showGPSalert();
        getToken();
    }

    private void showGPSalert() {
        try {
            if (mGoogleApiClient == null) {
                mGoogleApiClient = new GoogleApiClient.Builder(this)
                        .addApi(LocationServices.API)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this).build();
                mGoogleApiClient.connect();
            }
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            //**************************

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                status.startResolutionForResult(
                                        GetStartDemo.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }
                }
            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void getToken() {
        GCMClientManager pushClientManager = new GCMClientManager(this, getString(R.string.project_number));
        pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
            @Override
            public void onSuccess(String registrationId, boolean isNewRegistration) {
                Log.d("Registration id", registrationId);

                AppDelegate.save(GetStartDemo.this, registrationId, Tags.REGISTRATION_ID);
                //send this registrationId to your server
            }

            @Override
            public void onFailure(String ex) {
                super.onFailure(ex);
                Log.d("onFailure", ex);
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void findIDs() {
        viewpager = (MyViewPager) findViewById(R.id.image);
        splashPagerAdapter = new SplashPagerAdapter(GetStartDemo.this, 3);
        viewpager.setAdapter(splashPagerAdapter);

        findViewById(R.id.get_start).setOnClickListener(this);
        pager_indicator = (LinearLayout) findViewById(R.id.pager_indicator);
        setImages();
    }

    private void setImages() {
        slider = new ArrayList<>();

        slider.add(R.drawable.getstart);
        slider.add(R.drawable.getstart);
        slider.add(R.drawable.getstart);
        imageAdapter = new ImageAdapter(slider, GetStartDemo.this);
//        viewpager.setAdapter(imageAdapter);
        setUiPageViewController();
    }

    private void setUiPageViewController() {
        pager_indicator.removeAllViews();
        dotsCount = imageAdapter.getCount();
        if (dotsCount > 0) {
            dots = new ImageView[dotsCount];
            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new ImageView(this);
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.tab));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(AppDelegate.getInstance(this).dp2px(15), AppDelegate.getInstance(this).dp2px(15));
                params.setMargins(AppDelegate.getInstance(this).dp2px(5), 0, AppDelegate.getInstance(this).dp2px(5), 0);
                pager_indicator.addView(dots[i], params);
            }
            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.tabactive));
        }
        next();
    }

    private void next() {
        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switchBannerPage(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        switchBannerPage(0);
    }

    private void switchBannerPage(int position) {
        if (dotsCount > 0) {
            for (int i = 0; i < dotsCount; i++) {
                dots[i].setImageResource(R.drawable.tab);
            }
            dots[position].setImageResource(R.drawable.tabactive);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.get_start:
                Intent intent = new Intent(GetStartDemo.this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
