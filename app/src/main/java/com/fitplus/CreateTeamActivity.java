package com.fitplus;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fitplus.service.ChatService;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import UI.CircleImageView;
import adapters.ChatAdapter;
import fragments.FitTeamFragments;
import fragments.RadarFragment;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import interfaces.OnReciveSocketMessage;
import model.ChatModel;
import model.Create_Team_Model;
import model.PostAysnc_Model;
import model.TeamListModel;
import model.UserDataModel;
import utils.Prefs;
import utils.SpacesChatDecoration;

/**
 * Created by admin on 26-07-2016.
 */
public class CreateTeamActivity extends FragmentActivity implements OnClickListener, AdapterView.OnItemClickListener, OnReciveServerResponse, OnDialogClickListener, OnReciveSocketMessage {

    public static final int FROM_CHAT = 0, FROM_LIST = 1;

    private Prefs prefs;
    private UserDataModel dataModel;
    private Bundle bundle;

    RelativeLayout toolbar;
    TextView toolbar_title;
    private EditText et_message;

    CircleImageView team_icon;
    ImageView tool_menu, create_team, img_send, img_loading;
    RecyclerView team_list;
    Create_Team_Model create_team_model;
    TeamListModel teamlist;
    private int key;
    public int show_radar = 0;
    private Handler mHandler;
    public int team_id = 0;
    private ArrayList<ChatModel> arrayChatList = new ArrayList<>();
    private ChatAdapter chatAdapter;
    public static OnReciveSocketMessage onReciveSocketMessage;
    private ChatModel chatModel;
    int coach_id = 0;
    public static boolean isAppInBackground = false;
    private ImageView user, radar;
    private Intent intent;
    private ImageView calender;
    private int show_calender = 3;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.team);
        onReciveSocketMessage = this;
        prefs = new Prefs(this);
        dataModel = prefs.getUserdata();
        bundle = getIntent().getExtras();
        key = bundle.getInt("key");
        if (key == 1) {
            teamlist = bundle.getParcelable(Tags.createName);
            team_id = teamlist.team_id;
            // coach_id = teamlist.coach_id;
        } else if (key == 2) {
            create_team_model = bundle.getParcelable(Tags.TeamName);
            team_id = create_team_model.id;
        } else if (key == 3) {
            chatModel = bundle.getParcelable(Tags.message);
            create_team_model = new Create_Team_Model();
            create_team_model.id = chatModel.team_id;
            create_team_model.team_name = chatModel.team_name;
            team_id = create_team_model.id;
        }
        initView();
        setTeam_Avtar();
        setHandler();
        execute_FetchMessage();
        isAppInBackground = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onReciveSocketMessage = null;
        if (FitTeamFragments.mHandler != null) {
            FitTeamFragments.mHandler.sendEmptyMessage(1);
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(CreateTeamActivity.this);
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(CreateTeamActivity.this);
                } else if (msg.what == 1) {
                    AppDelegate.LogT("coach_id => " + coach_id);
                    AppDelegate.LogT("show_radar => " + show_radar);
                    chatAdapter.notifyDataSetChanged();
                    team_list.invalidate();
                    show_icons();
                } else if (msg.what == 2) {
                    try {
                        team_list.post(new Runnable() {
                            @Override
                            public void run() {
                                if (arrayChatList.size() > 0)
                                    team_list.smoothScrollToPosition(arrayChatList.size() - 1);
                            }
                        });
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            }
        };
    }

    private void show_icons() {
        if (coach_id == 0) {
            user.setVisibility(View.VISIBLE);
        } else if (coach_id > 0) {
            user.setVisibility(View.GONE);
        }
        if (show_radar == 0) {
            radar.setVisibility(View.GONE);
        } else {
            radar.setVisibility(View.VISIBLE);
        }
        if (coach_id == 0 || coach_id == new Prefs(this).getUserdata().userId) {
            calender.setVisibility(View.GONE);
        } else {
            calender.setVisibility(View.VISIBLE);
        }
    }


    private void initView() {
        toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        toolbar_title = (TextView) findViewById(R.id.title);
        tool_menu = (ImageView) findViewById(R.id.back);
        tool_menu.setOnClickListener(this);
        img_loading = (ImageView) findViewById(R.id.img_loading);
        create_team = (ImageView) findViewById(R.id.add_member);
        create_team.setOnClickListener(this);
        calender = (ImageView) findViewById(R.id.calender);
        calender.setOnClickListener(this);
        if (key == 1) {
            toolbar_title.setText("" + teamlist.team_name);
        } else {
            toolbar_title.setText("" + create_team_model.team_name);
        }
        et_message = (EditText) findViewById(R.id.et_message);
        user = (ImageView) findViewById(R.id.user);
        user.setOnClickListener(this);
        radar = (ImageView) findViewById(R.id.radar);
        radar.setOnClickListener(this);
        //  radar.setVisibility(View.GONE);
        findViewById(R.id.img_send).setOnClickListener(this);
        team_icon = (CircleImageView) findViewById(R.id.team_icon);
        team_list = (RecyclerView) findViewById(R.id.team_list);
        team_list.setLayoutManager(new LinearLayoutManager(this));
        team_list.addItemDecoration(new SpacesChatDecoration(AppDelegate.dpToPix(this, 5), true));
        chatAdapter = new ChatAdapter(this, arrayChatList);
        team_list.setAdapter(chatAdapter);
        team_list.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                AppDelegate.LogT("onScrollStateChanged called");
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        AppDelegate.LogT(coach_id + " coach id");
        if (coach_id > 0) {
            user.setVisibility(View.GONE);
            //  radar.setVisibility(View.GONE);
        }
    }

    private void setTeam_Avtar() {
        if (key == 1) {
            teamlist = bundle.getParcelable(Tags.createName);
            if (AppDelegate.isValidString(teamlist.team_avtar)) {
                img_loading.setVisibility(View.VISIBLE);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
                        frameAnimation.setCallback(img_loading);
                        frameAnimation.setVisible(true, true);
                        frameAnimation.start();
                        ((Animatable) img_loading.getDrawable()).start();
                    }
                });
                Picasso.with(this).load(teamlist.team_avtar).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        team_icon.setImageBitmap(bitmap);
                        img_loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                        AppDelegate.LogT("CreateTeam onBitmapFailed called");
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                        AppDelegate.LogT("CreateTeam onBitmapFailed called");
                    }
                });
            }
        } else if (key == 2) {
            create_team_model = bundle.getParcelable(Tags.TeamName);
            if (AppDelegate.isValidString(create_team_model.team_avtar)) {
                img_loading.setVisibility(View.VISIBLE);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
                        frameAnimation.setCallback(img_loading);
                        frameAnimation.setVisible(true, true);
                        frameAnimation.start();
                        ((Animatable) img_loading.getDrawable()).start();
                    }
                });
                Picasso.with(this).load(create_team_model.team_avtar).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        team_icon.setImageBitmap(bitmap);
                        img_loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                    }
                });
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppDelegate.LogT("onPause");
        isAppInBackground = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppDelegate.LogT("onResume");
        isAppInBackground = false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.radar:
                intent = new Intent(CreateTeamActivity.this, RadarActivity.class);
                intent.putExtra(Tags.team_id, team_id);
                // AppDelegate.showFragmentAnimation(getSupportFragmentManager(), new RadarFragment(), R.id.container);
                startActivity(intent);
                break;
            case R.id.add_member:
                execute_Add_member();
                break;

            case R.id.back:
                if (FitTeamFragments.mHandler != null) {
                    FitTeamFragments.mHandler.sendEmptyMessage(1);
                }
                finish();
                break;
            case R.id.calender:
                bundle = new Bundle();
                if (key == 1) {
                    bundle.putInt(Tags.Find_Coach, teamlist.team_id);
                } else {
                    bundle.putInt(Tags.Find_Coach, create_team_model.id);
                }
                intent = new Intent(this, SetLocationActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.user:
                bundle = new Bundle();
                if (key == 1) {
                    bundle.putInt(Tags.Find_Coach, teamlist.team_id);
                } else {
                    bundle.putInt(Tags.Find_Coach, create_team_model.id);
                }
                intent = new Intent(this, SetDateTimeActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.img_send:
                if (et_message.length() == 0) {
                    AppDelegate.showToast(CreateTeamActivity.this, "Please enter message before send.");
                } else if (AppDelegate.haveNetworkConnection(CreateTeamActivity.this)) {
                    ChatModel chatModel = new ChatModel();
                    chatModel.chat_status = ChatModel.FROM_USER;
                    chatModel.txt_msg_send = et_message.getText().toString();
                    if (key == 1) {
                        chatModel.team_id = teamlist.team_id;
                    } else {
                        chatModel.team_id = create_team_model.id;
                    }

                    chatModel.txt_time_send = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(Calendar.getInstance().getTime());
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("sender_id", dataModel.userId);
                        jsonObject.put("team_id", chatModel.team_id);
                        jsonObject.put("message", chatModel.txt_msg_send);
                        jsonObject.put("status", "1");
                    } catch (JSONException e) {
                        AppDelegate.LogE(e);
                    }
                    ChatService.mSocket.emit("send message", jsonObject.toString());
                    AppDelegate.LogC("send message => " + jsonObject.toString());
                    addMessageToList(chatModel);
                    et_message.setText("");
                }
                break;
        }
    }

    private void execute_FetchMessage() {
        try {
            if (AppDelegate.haveNetworkConnection(CreateTeamActivity.this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(CreateTeamActivity.this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(CreateTeamActivity.this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(CreateTeamActivity.this).getUserdata().userId);
                AppDelegate.getInstance(CreateTeamActivity.this).setPostParamsSecond(mPostArrayList, Tags.latitude, new Prefs(CreateTeamActivity.this).getStringValue(Tags.TAG_LAT, ""));
                AppDelegate.getInstance(CreateTeamActivity.this).setPostParamsSecond(mPostArrayList, Tags.longitude, new Prefs(CreateTeamActivity.this).getStringValue(Tags.TAG_LONG, ""));
                if (key == 1) {
                    AppDelegate.getInstance(CreateTeamActivity.this).setPostParamsSecond(mPostArrayList, Parameters.team_id, teamlist.team_id);
                } else {
                    AppDelegate.getInstance(CreateTeamActivity.this).setPostParamsSecond(mPostArrayList, Parameters.team_id, create_team_model.id);
                }
                AppDelegate.getInstance(CreateTeamActivity.this).setPostParamsSecond(mPostArrayList, Parameters.devicetype, 2, ServerRequestConstants.Key_PostintValue);

                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(CreateTeamActivity.this, CreateTeamActivity.this, ServerRequestConstants.FETCH_MESSAGES,
                        mPostArrayList, null);
                // AppDelegate.showProgressDialog(CreateTeamActivity.this);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(CreateTeamActivity.this, "Please try again.", "Alert!!!");
            AppDelegate.LogE(e);
        }
    }


    private void execute_Add_member() {
        try {
            if (AppDelegate.haveNetworkConnection(CreateTeamActivity.this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(CreateTeamActivity.this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(CreateTeamActivity.this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(CreateTeamActivity.this).getUserdata().userId);
                AppDelegate.getInstance(CreateTeamActivity.this).setPostParamsSecond(mPostArrayList, Tags.latitude, new Prefs(CreateTeamActivity.this).getStringValue(Tags.TAG_LAT, ""));
                AppDelegate.getInstance(CreateTeamActivity.this).setPostParamsSecond(mPostArrayList, Tags.longitude, new Prefs(CreateTeamActivity.this).getStringValue(Tags.TAG_LONG, ""));
                if (key == 1) {
                    AppDelegate.getInstance(CreateTeamActivity.this).setPostParamsSecond(mPostArrayList, Parameters.team_id, teamlist.team_id);
                } else {
                    AppDelegate.getInstance(CreateTeamActivity.this).setPostParamsSecond(mPostArrayList, Parameters.team_id, create_team_model.id);
                }
                AppDelegate.getInstance(CreateTeamActivity.this).setPostParamsSecond(mPostArrayList, Parameters.devicetype, 2, ServerRequestConstants.Key_PostintValue);

                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(CreateTeamActivity.this, CreateTeamActivity.this,
                        ServerRequestConstants.ADD_TEAM_MEMBER,
                        mPostArrayList, null);
                // AppDelegate.showProgressDialog(CreateTeamActivity.this);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(CreateTeamActivity.this, "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
        }
    }

    @Override
    public void onBackPressed() {
        if (FitTeamFragments.mHandler != null) {
            FitTeamFragments.mHandler.sendEmptyMessage(1);
        }
        super.onBackPressed();
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        //AppDelegate.hideProgressDialog(CreateTeamActivity.this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(CreateTeamActivity.this, CreateTeamActivity.this.getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.ADD_TEAM_MEMBER)) {
            parseaddTeammember(result);
            execute_FetchMessage();
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.FETCH_MESSAGES)) {
            parseFetchMessage(result);
        }
    }

    private void parseFetchMessage(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                arrayChatList.clear();
                coach_id = jsonObject.getInt(Tags.coach_id);
                //  show_radar = jsonObject.getInt(Tags.coach);
                show_radar = jsonObject.getInt(Tags.radar);
                show_calender = jsonObject.getInt(Tags.calender);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    ChatModel chatModel = new ChatModel();

                    chatModel.id = object.getInt(Tags.sender_id);
                    chatModel.team_id = object.getInt(Tags.team_id);
                    AppDelegate.LogT("check => id => " + dataModel.userId + " == " + object.getInt(Tags.sender_id));
                    if (object.getInt(Tags.notification_status) == 1) {
                        chatModel.chat_status = ChatModel.FROM_ACTION;
                        chatModel.action_message = object.getString(Tags.message);
                        chatModel.action_time = object.getString(Tags.created);
                        chatModel.action_user_id = object.getString(Tags.sender_id);
                        chatModel.action_by = object.getString(Tags.sender_name);

                    } else if (dataModel.userId == object.getInt(Tags.sender_id)) {
                        chatModel.chat_status = ChatModel.FROM_USER;
                        chatModel.txt_msg_send = object.getString(Tags.message);
                        chatModel.txt_time_send = object.getString(Tags.created);

                    } else {
                        chatModel.chat_status = ChatModel.FROM_OTHER;
                        chatModel.txt_msg_rcv = object.getString(Tags.message);
                        chatModel.txt_time_rcv = object.getString(Tags.created);
                        chatModel.txt_pic_rcv = object.getString(Tags.sender_avtar);
                        chatModel.txt_name_rcv = object.getString(Tags.sender_name);

                    }
                    arrayChatList.add(chatModel);
                }
            } else {
            }
            mHandler.sendEmptyMessage(1);
            mHandler.sendEmptyMessage(2);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseaddTeammember(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                AppDelegate.showToast(CreateTeamActivity.this, jsonObject.getString(Tags.message));
            } else {
                AppDelegate.ShowDialogID(CreateTeamActivity.this, jsonObject.getString(Tags.message), "Alert", Tags.ok, CreateTeamActivity.this);
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(CreateTeamActivity.this, getResources().getString(R.string.response_error), "");
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnDialogClickListener(String name) {
    }

    private void addMessageToList(ChatModel message) {
        arrayChatList.add(message);
        mHandler.sendEmptyMessage(1);
        team_list.smoothScrollToPosition(arrayChatList.size() - 1);
    }

    @Override
    public void setOnReciveSocketMessage(String apiName, ChatModel chatModel) {
        if (apiName.equalsIgnoreCase("chat")) {
            if (key == 1) {
                teamlist = getIntent().getExtras().getParcelable(Tags.createName);
                if (teamlist.team_id == chatModel.team_id) {
                    addMessageToList(chatModel);
                    if (isAppInBackground) {
                        PushNotificationService.showUserChatNotification(CreateTeamActivity.this, chatModel);
                    }
                } else {
                    PushNotificationService.showUserChatNotification(CreateTeamActivity.this, chatModel);
                }
            } else {
                if (create_team_model.id == chatModel.team_id) {
                    addMessageToList(chatModel);
                    if (isAppInBackground) {
                        PushNotificationService.showUserChatNotification(CreateTeamActivity.this, chatModel);
                    }
                } else {
                    PushNotificationService.showUserChatNotification(CreateTeamActivity.this, chatModel);
                }
            }
        } else if (apiName.equalsIgnoreCase("refresh")) {
            if (key == 1) {
                teamlist = getIntent().getExtras().getParcelable(Tags.createName);
                if (teamlist.team_id == chatModel.team_id) {
                    execute_FetchMessage();
                }
            } else {
                if (create_team_model.id == chatModel.team_id) {
                    execute_FetchMessage();
                }
            }
        }

    }
}
