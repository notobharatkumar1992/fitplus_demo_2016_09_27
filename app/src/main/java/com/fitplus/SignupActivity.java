package com.fitplus;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import io.fabric.sdk.android.Fabric;
import model.PostAysnc_Model;
import model.UserDataModel;
import utils.Prefs;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, OnReciveServerResponse, OnDialogClickListener {


    private TextView register, birthday;
    EditText firstname, lastname, city, email, password, confirmpass, nickname;
    ToggleButton gender;
    String first_name, last_name, cityy, email_id, pass, confirm_pass, birth, Gender, nick_name;
    private int mYear, mMonth, mDay, mHour, mMinute;
    public static final String DATE_FORMAT = "dd-MM-yyyy";
    public static final String TIME_FORMAT_12_HOUR = "hh:mm aa";
    public static final String TIME_FORMAT_24_HOUR = "HH:mm";
    private Prefs prefs;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude = 0, currentLongitude = 0;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.signup);
        prefs = new Prefs(this);
        initGPS();
        initView();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Disconnect from API onPause()
        try {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (com.google.android.gms.location.LocationListener) SignupActivity.this);
                mGoogleApiClient.disconnect();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        AppDelegate.LogT("onConnected Initialited");
        if (location == null) {
            try {
                AppDelegate.LogT("onConnected Initialited== null");
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (com.google.android.gms.location.LocationListener) SignupActivity.this);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            AppDelegate.LogT("latLng = " + currentLatitude + ", " + currentLongitude);
            new Prefs(this).putStringValue(Tags.TAG_LAT, String.valueOf(currentLatitude));
            new Prefs(this).putStringValue(Tags.TAG_LONG, String.valueOf(currentLongitude));
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                AppDelegate.LogT("onConnectionFailed Initialited" + connectionResult);
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);

            } catch (IntentSender.SendIntentException e) {
                AppDelegate.LogE(e);
            }
        } else {
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppDelegate.hideKeyBoard(this);
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
        AppDelegate.LogGP("onLocationChanged latLng = " + currentLatitude + ", " + currentLongitude);
        new Prefs(this).putStringValue(Tags.TAG_LAT, String.valueOf(currentLatitude));
        new Prefs(this).putStringValue(Tags.TAG_LONG, String.valueOf(currentLongitude));
        try {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (com.google.android.gms.location.LocationListener) SignupActivity.this);
                mGoogleApiClient.disconnect();
                AppDelegate.LogGP("Fused Location api disconnect called");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }


    private void initGPS() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        AppDelegate.LogT("mGoogleApiClient Initialited");
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(500)        // 10 seconds, in milliseconds
                .setFastestInterval(500); // 10 second, in milliseconds
        AppDelegate.LogT("mLocationRequest Initialited");
    }

    private void initView() {
        birthday = (TextView) findViewById(R.id.birthday);
        register = (TextView) findViewById(R.id.register);
        gender = (ToggleButton) findViewById(R.id.gender);
        firstname = (EditText) findViewById(R.id.first_name);
        lastname = (EditText) findViewById(R.id.last_name);
        city = (EditText) findViewById(R.id.city);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        nickname = (EditText) findViewById(R.id.nick_name);
        confirmpass = (EditText) findViewById(R.id.confirm_password);
        register.setOnClickListener(this);
        gender.setOnClickListener(this);
        birthday.setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
    }



    private void showDateDialog() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        birthday.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    }
                }, mYear, mMonth, mDay);
        c.add(Calendar.YEAR, -10);
        dpd.getDatePicker().setMaxDate(c.getTimeInMillis()/*c.getTimeInMillis()*//*System.currentTimeMillis() - 10000*/);
        dpd.show();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.birthday:
                showDateDialog();
                break;
            case R.id.register:
                new Prefs(this).clearTempPrefs();
                gettext();
                validate();
                break;
            case R.id.back:
                finish();
                break;
        }
    }

    private void executeSignup() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.email, email_id.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.password, pass.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.gender, Gender.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.birthdate, birth.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.first_name, first_name.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.last_name, last_name.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.nick_name, nick_name.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_id, String.valueOf(AppDelegate.getValue(this, Tags.REGISTRATION_ID)).trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_type, String.valueOf(2).trim());
                // AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_token, AppDelegate.getValue(SignupActivity.this, Tags.REGISTRATION_ID), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.latitude, new Prefs(this).getStringValue(Tags.TAG_LAT, ""));
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.longitude, new Prefs(this).getStringValue(Tags.TAG_LONG, ""));
                AppDelegate.LogT("sign up longitude " + new Prefs(this).getStringValue(Tags.TAG_LONG, ""));
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.REGISTER, mPostArrayList, null);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    private void validate() {
        if (!AppDelegate.isValidString(first_name)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.forname), "Alert!!!");
        } else if (!AppDelegate.isValidString(last_name)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.forlastname), "Alert!!!");
        } else if (!AppDelegate.isValidString(nick_name)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.fornick), "Alert!!!");
        }/* else if (!AppDelegate.isValidString(cityy)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.forcity), "Alert!!!");
        }*/ else if (!AppDelegate.CheckEmail(email_id)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.foremail), "Alert!!!");
        } else if (!AppDelegate.isValidString(Gender)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.forgender), "Alert!!!");
        } else if (!AppDelegate.isValidString(birth)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.forbirth), "Alert!!!");
        } else if (!AppDelegate.isValidString(pass)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.forpass), "Alert!!!");
        } else if (pass.length() < 6) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.forlength), "Alert!!!");
        } else if (!AppDelegate.isValidString(confirm_pass)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.forconpass), "Alert!!!");
        } else if (!pass.equals(confirm_pass)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.formatch), "Alert!!!");
        } else if (!AppDelegate.password_validation(this, pass)) {

        } else {
            if (AppDelegate.haveNetworkConnection(this, false)) {
                executeSignup();
            } else {
                AppDelegate.ShowDialog(this, getResources().getString(R.string.not_connected), "Alert!!!");
            }
        }
    }

    private void gettext() {
        first_name = firstname.getText().toString();
        last_name = lastname.getText().toString();
        email_id = email.getText().toString();
        cityy = city.getText().toString();
        if (gender.isChecked()) {
            Gender = gender.getTextOn().toString();
        } else {
            Gender = gender.getTextOff().toString();
        }
        birth = birthday.getText().toString();
        pass = password.getText().toString();
        confirm_pass = confirmpass.getText().toString();
        nick_name = nickname.getText().toString();
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, "Service Time Out!!!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.REGISTER)) {
            parseRegister(result);
        }
    }

    private void parseRegister(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            UserDataModel userDataModel = new UserDataModel();
            userDataModel.message = jsonObject.getString(Tags.message);
            userDataModel.datatflow = jsonObject.getInt(Tags.dataFlow);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                userDataModel = new UserDataModel();
                //userDataModel.status = object.getInt(Tags.status);
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.password = object.getString(Tags.password);
                userDataModel.created = object.getString(Tags.created);
                userDataModel.str_Gender = object.getString(Tags.gender);
                userDataModel.userId = object.getInt(Tags.user_id);
                userDataModel.dob = object.getString(Tags.birthdate);
                userDataModel.nickname = object.getString(Tags.nick_name);
                prefs.setUserData(userDataModel);
//                AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.ok, SignupActivity.this);
                AppDelegate.showToast(SignupActivity.this, jsonObject.getString(Tags.message));
                startActivity(new Intent(SignupActivity.this, BeginnerQuestionActivity.class));
                finish();
            } else {
                if (jsonObject.getInt(Tags.status) == 0) {
                    AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", "", SignupActivity.this);
                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
                    AppDelegate.showAlert(this, jsonObject.getString(Tags.message));
                }
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showAlert(this, "Response is not proper. Please try again later.");
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void setOnDialogClickListener(String name) {
        if (name.equalsIgnoreCase(Tags.ok)) {
            startActivity(new Intent(SignupActivity.this, BeginnerQuestionActivity.class));
            finish();
        }
    }
}
