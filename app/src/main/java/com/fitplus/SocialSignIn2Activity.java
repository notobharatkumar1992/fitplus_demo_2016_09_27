package com.fitplus;

import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.Fb_detail_GetSet;
import model.PostAysnc_Model;
import model.UserDataModel;
import utils.Prefs;
import utils.ProgressD;

public class SocialSignIn2Activity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnDialogClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private Intent intent;
    private String Email;
    EditText email_id;
    TextView submit;
    private boolean result;
    public static SocialSignIn2Activity forgotpassword;
    private String identifier;
    private int status;
    int varificationcode = 0;
    private Bundle bundle;
    Fb_detail_GetSet getset;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude = 0, currentLongitude = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password);
        forgotpassword = this;
        initGPS();
        bundle = getIntent().getExtras();
        getset = bundle.getParcelable(Tags.data);
        initializeObjects();
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (com.google.android.gms.location.LocationListener) SocialSignIn2Activity.this);
                mGoogleApiClient.disconnect();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        AppDelegate.LogT("onConnected Initialited");
        if (location == null) {
            try {
                AppDelegate.LogT("onConnected Initialited== null");
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (com.google.android.gms.location.LocationListener) SocialSignIn2Activity.this);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            AppDelegate.LogT("latLng = " + currentLatitude + ", " + currentLongitude);
            new Prefs(this).putStringValue(Tags.TAG_LAT, String.valueOf(currentLatitude));
            new Prefs(this).putStringValue(Tags.TAG_LONG, String.valueOf(currentLongitude));
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                AppDelegate.LogT("onConnectionFailed Initialited" + connectionResult);
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);

            } catch (IntentSender.SendIntentException e) {
                AppDelegate.LogE(e);
            }
        } else {
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppDelegate.hideKeyBoard(this);
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(android.location.Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
        AppDelegate.LogGP("onLocationChanged latLng = " + currentLatitude + ", " + currentLongitude);
        new Prefs(this).putStringValue(Tags.TAG_LAT, String.valueOf(currentLatitude));
        new Prefs(this).putStringValue(Tags.TAG_LONG, String.valueOf(currentLongitude));
        try {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (com.google.android.gms.location.LocationListener) SocialSignIn2Activity.this);
                mGoogleApiClient.disconnect();
                AppDelegate.LogGP("Fused Location api disconnect called");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    private void initializeObjects() {
        email_id = (EditText) findViewById(R.id.email_id);
        submit = (TextView) findViewById(R.id.submit);
        submit.setOnClickListener(this);
        identifier = AppDelegate.getValue(this, Tags.IDENTIFIER);
        findViewById(R.id.back).setOnClickListener(this);
        TextView title = (TextView) findViewById(R.id.title);
        title.setText("");
    }

    private void initGPS() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        AppDelegate.LogT("mGoogleApiClient Initialited");
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(500)        // 10 seconds, in milliseconds
                .setFastestInterval(500); // 10 second, in milliseconds
        AppDelegate.LogT("mLocationRequest Initialited");
    }

    private boolean validate_emailid(String email) {
        boolean result = true;
        try {
            if (email == null) {
                result = false;
                AppDelegate.ShowDialog(SocialSignIn2Activity.this, "Please enter Email Id !!!", "Oops!!!");
            }
        } catch (Exception e) {
            intent = new Intent(SocialSignIn2Activity.this, LoginActivity.class);
            startActivity(intent);
        }
        if (!AppDelegate.CheckEmail(email)) {
            result = false;
            AppDelegate.ShowDialog(SocialSignIn2Activity.this, "Invalid Email Address", "Oops!!!");
        }
        return result;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        if (forgotpassword != null) {
            forgotpassword = null;
        }
        ProgressD.hideProgress_dialog();
        System.gc();
        super.onDestroy();
    }

    private void ForgotPassword() {
        if (getset == null) {
        } else if (AppDelegate.haveNetworkConnection(this, false)) {
            try {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_id, String.valueOf(AppDelegate.getValue(this, Tags.REGISTRATION_ID)).trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_type, String.valueOf(2));
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.gid, getset.getGid() + "");
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.fid, getset.getSocial_id() + "");
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.first_name, getset.getFirstname() + "");
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.last_name, getset.getLast_name() + "");
                if (AppDelegate.isValidString(getset.gender)) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.gender, getset.gender.trim());
                } else {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.gender, "male");
                }

                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.email, email_id.getText().toString() + "");
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.latitude, new Prefs(this).getStringValue(Tags.TAG_LAT, ""));
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.longitude, new Prefs(this).getStringValue(Tags.TAG_LONG, ""));
                PostAsync mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.SOCIAL_SIGNIN2,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                if (forgotpassword != null)
                    forgotpassword.finish();
                break;
            case R.id.submit:

                Email = email_id.getText().toString();
                result = validate_emailid(Email);
                if (result == true) {
                    ForgotPassword();
                }
        }
    }


    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, "Service Time Out!!!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.SOCIAL_SIGNIN2)) {
            parseForgot(result);
        }
    }

    private void parseForgot(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.gid = object.getString(Tags.gid);
                userDataModel.fid = object.getString(Tags.fid);
                userDataModel.type = object.getString(Tags.type);
                userDataModel.sex_group = object.getString(Tags.sex_group);
                userDataModel.fat_status = object.getString(Tags.fat_status);
                userDataModel.latitude = object.getDouble(Tags.latitude);
                userDataModel.longitude = object.getDouble(Tags.longitude);
                userDataModel.userId = object.getInt(Tags.user_id);
                userDataModel.created = object.getString(Tags.created);

                Prefs prefs = new Prefs(this);
                prefs.setUserData(userDataModel);
                prefs.putUserId(String.valueOf(userDataModel.userId));
                if (!AppDelegate.isValidString(userDataModel.fat_status)) {
                    Intent intent = new Intent(SocialSignIn2Activity.this, BeginnerQuestionActivity.class);
                    startActivity(intent);
                }
                finish();
            } else {
                if (jsonObject.getInt(Tags.status) == 0) {
                    AppDelegate.ShowDialog(this, jsonObject.getString(Tags.message), "alert");
                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null) {
                    AppDelegate.showAlert(this, jsonObject.getString(Tags.message));
                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(this, getResources().getString(R.string.response_error));
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnDialogClickListener(String name) {

    }
}
