package com.fitplus;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import fragments.SettingsFragment;
import fragments.ViewProfileFragment;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import io.fabric.sdk.android.Fabric;
import model.PostAysnc_Model;
import model.UserDataModel;
import utils.Prefs;

/**
 * Created by admin on 26-07-2016.
 */
public class BeginnerQuestionActivity extends AppCompatActivity implements OnClickListener, OnReciveServerResponse, OnDialogClickListener {
    private View rootview;
    TextView option1, option2, option3, option4, option5, option6;
    LinearLayout ll1, ll2, ll3;
    private String first, second, third;
    Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.question_container);
        mActivity = null;
        initView();
    }

    private void initView() {
        option1 = (TextView) findViewById(R.id.option1);
        option2 = (TextView) findViewById(R.id.option2);
        option3 = (TextView) findViewById(R.id.option3);
        option4 = (TextView) findViewById(R.id.option4);
        option5 = (TextView) findViewById(R.id.option5);
        option6 = (TextView) findViewById(R.id.option6);
        ll1 = (LinearLayout) findViewById(R.id.ll_first);
        ll2 = (LinearLayout) findViewById(R.id.ll_second);
        ll3 = (LinearLayout) findViewById(R.id.ll_third);
        option1.setOnClickListener(this);
        option2.setOnClickListener(this);
        option3.setOnClickListener(this);
        option4.setOnClickListener(this);
        option5.setOnClickListener(this);
        option6.setOnClickListener(this);
        ll1.setOnClickListener(this);
        ll2.setOnClickListener(this);
        ll3.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        if (ll3.getVisibility() == View.VISIBLE) {
            changeVisibility(1);
        } else if (ll2.getVisibility() == View.VISIBLE) {
            changeVisibility(0);
        } else if (ll1.getVisibility() == View.VISIBLE) {
            finish();
        }
    }

    private void changeVisibility(int value) {
        ll1.setVisibility(View.GONE);
        ll2.setVisibility(View.GONE);
        ll3.setVisibility(View.GONE);
        switch (value) {
            case 0:
                ll1.setVisibility(View.VISIBLE);
                break;
            case 1:
                ll2.setVisibility(View.VISIBLE);
                break;
            case 2:
                ll3.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.option1:
                option1.setSelected(true);
                option2.setSelected(false);
                try {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            changeVisibility(1);
                        }
                    }, 300);
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }

                first = getResources().getString(R.string.beginner);
                break;
            case R.id.option2:
                try {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            changeVisibility(1);
                        }
                    }, 300);
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                option1.setSelected(false);
                option2.setSelected(true);
                first = getResources().getString(R.string.regular);
                break;
            case R.id.option3:
                option4.setSelected(false);
                option3.setSelected(true);
                try {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            changeVisibility(2);
                        }
                    }, 300);
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                second = getResources().getString(R.string.gain_muscle);
                break;
            case R.id.option4:
                option3.setSelected(false);
                option4.setSelected(true);
                try {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            changeVisibility(2);
                        }
                    }, 300);
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                second = getResources().getString(R.string.loose_fat);
                break;
            case R.id.option5:
                option6.setSelected(false);
                option5.setSelected(true);
                third = getResources().getString(R.string.unisex);
                try {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            execute_UserCategory();
                        }
                    }, 300);
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }

                break;
            case R.id.option6:
                option5.setSelected(false);
                option6.setSelected(true);
                third = getResources().getString(R.string.mixed);
                try {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            execute_UserCategory();
                        }
                    }, 300);
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }

                break;
        }
    }

    private void execute_UserCategory() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(this).getUserdata().userId);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.category_one, first.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.category_two, second.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.category_three, third.trim());
                //  AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.device_token, AppDelegate.getValue(SignupActivity.getActivity(), Tags.REGISTRATION_ID), ServerRequestConstants.Key_PostintValue);
                mPostAsyncObj = new PostAsync(this, BeginnerQuestionActivity.this, ServerRequestConstants.USER_CATEGORY, mPostArrayList, null);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, this.getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.USER_CATEGORY)) {
            parseCategory(result);
        }
    }

    private void parseCategory(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.fat_status = object.getString(Tags.fat_status);
                userDataModel.password = object.getString(Tags.password);
                userDataModel.created = object.getString(Tags.created);
                userDataModel.str_Gender = object.getString(Tags.gender);
                userDataModel.userId = object.getInt(Tags.user_id);
                userDataModel.dob = object.getString(Tags.birthdate);
                userDataModel.nickname = object.getString(Tags.nick_name);
                userDataModel.avtar = object.getString(Tags.avtar);
                userDataModel.views = object.getString(Tags.views);
                userDataModel.follower_count = object.getInt(Tags.follower_count);
                userDataModel.profile_visibility = object.getInt(Tags.profile_visibility);
                userDataModel.presentation_vedio = object.getString(Tags.presentation_vedio);
                userDataModel.bank_account = object.getString(Tags.bank_account);
                userDataModel.certificates = object.getString(Tags.certificates);
                userDataModel.country_id = object.getString(Tags.country_id);
                userDataModel.role_id = object.getInt(Tags.role_id);
                userDataModel.state_id = object.getString(Tags.state_id);
                userDataModel.city_id = object.getString(Tags.city_id);
                userDataModel.post_code = object.getString(Tags.post_code);
                userDataModel.sex_group = object.getString(Tags.sex_group);
                userDataModel.type = object.getString(Tags.type);
                userDataModel.token = object.getString(Tags.token);
                userDataModel.is_login = object.getInt(Tags.is_login);
                userDataModel.is_social = object.getInt(Tags.is_social);
                userDataModel.is_verified = object.getInt(Tags.is_verified);
                userDataModel.latitude = object.getDouble(Tags.latitude);
                userDataModel.longitude = object.getDouble(Tags.longitude);
                new Prefs(this).setUserData(userDataModel);
                //AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.ok, BeginnerQuestionActivity.this);
                AppDelegate.showToast(BeginnerQuestionActivity.this, jsonObject.getString(Tags.message));
                if (AppDelegate.setting_or_not == true) {
                    if (SettingsFragment.mHandler != null) {
                        AppDelegate.LogT("updateGlobal handler calld");
                        SettingsFragment.mHandler.sendEmptyMessage(1);
                    }
                    finish();
                } else {
                    startActivity(new Intent(BeginnerQuestionActivity.this, MainActivity.class));
                    finish();
                }

            } else {
                if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
                    AppDelegate.showAlert(this, jsonObject.getString(Tags.message));
                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(this, getResources().getString(R.string.response_error));
            AppDelegate.LogE(e);
        }
    }


    @Override
    public void setOnDialogClickListener(String name) {
//        if (name.equalsIgnoreCase(Tags.ok)) {
//            startActivity(new Intent(BeginnerQuestionActivity.this, MainActivity.class));
//            finish();
//        }
    }
}
