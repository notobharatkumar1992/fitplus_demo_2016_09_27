package com.fitplus;

/**
 * Created by Heena on 21-Sep-16.
 */

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import UI.TouchImageView;
import model.FitDayModel;

public class ViewPagerAdapter extends PagerAdapter {
    private View rootview;
    ImageView image1, image2, image3, image4, image5, image6;
    private Bundle bundle;
    TextView text;
    Context context;
    // ImageView star, video;
    TouchImageView image;
    VideoView videoView;
    LinearLayout card, hide_visibility;
    /*FitDayModel fitDayModelArrayList;*/
    private MediaController mMediaController;
    Boolean show = false;
    int position;
    ArrayList<FitDayModel> fitDayModel;
    ImageLoader imageLoader = ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
            /*.showImageForEmptyUri(R.drawable.dummy_user_icon)
            .showImageOnFail(fallback)
            .showImageOnLoading(fallback).*/build();
    private LayoutInflater inflater;

    public ViewPagerAdapter(Context context, ArrayList<FitDayModel> fitDayModel) {
        this.context = context;
        this.fitDayModel = fitDayModel;
    }

    @Override
    public int getCount() {
        return fitDayModel.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }


    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.fit_day_item_zoomable, container,
                false);
        image = (TouchImageView) rootview.findViewById(R.id.img_content);
        videoView = (VideoView) rootview.findViewById(R.id.videoView);
        if (fitDayModel.get(position).file_type == 1) {
            image.setVisibility(View.VISIBLE);
            // video.setVisibility(View.GONE);
            videoView.setVisibility(View.GONE);
            imageLoader.displayImage(fitDayModel.get(position).file_name, image, options);

        } else if (fitDayModel.get(position).file_type == 2) {
            image.setVisibility(View.VISIBLE);
            videoView.setVisibility(View.GONE);
            imageLoader.displayImage(fitDayModel.get(position).file_thumb, image, options);
            playvdo(fitDayModel.get(position).file_name);
        }
        ((ViewPager) container).addView(itemView);
        return itemView;
    }

    private void playvdo(String url) {
        videoView.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (show == false) {
                }
            }
        }, 60000);
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.d("video", "setOnErrorListener ");
                AppDelegate.ShowDialog(context, "Can't play this vedio", "Alert");
                return true;
            }
        });
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                show = true;
                mp.getDuration();
                AppDelegate.LogT("position of video==" + position);
                if (ViewImageVideoActivity.onListItemClickListener != null) {
                    ViewImageVideoActivity.onListItemClickListener.setOnListItemClickListener("playing", position);
                }
                if (ViewImageVideoActivity.getDurationClickListener != null) {
                    ViewImageVideoActivity.getDurationClickListener.setonGetDurationClickListener("video", position, mp.getDuration());
                }
                image.setVisibility(View.GONE);
                videoView.setVisibility(View.VISIBLE);
                AppDelegate.LogT(" image.setVisibility(View.GONE);" + (image.getVisibility() == View.VISIBLE));
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        image.setVisibility(View.VISIBLE);
                        if (ViewImageVideoActivity.onListItemClickListener != null) {
                            ViewImageVideoActivity.onListItemClickListener.setOnListItemClickListener("stopped", position);
                        }
                    }
                });
            }
        });
        videoView.setVideoURI(Uri.parse(url));
        videoView.requestFocus();
        videoView.start();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((LinearLayout) object);

    }

    public void pausevideo() {
        videoView.stopPlayback();

    }

}