package com.fitplus;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import Constants.Tags;
import io.fabric.sdk.android.Fabric;
import utils.Prefs;

public class SplashActivity extends AppCompatActivity {
    private Intent mainIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.splash);
        final Prefs prefs = new Prefs(this);
        //   if (prefs.getUserdata() != null && AppDelegate.isValidString(prefs.getUserId())){
        getToken();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (prefs.getUserdata() != null && AppDelegate.isValidString(String.valueOf(prefs.getUserdata().userId)) && prefs.isRemembered().equalsIgnoreCase("true")) {
                    if (!AppDelegate.isValidString(String.valueOf(prefs.getUserdata().fat_status))) {
                        startActivity(new Intent(SplashActivity.this, BeginnerQuestionActivity.class));
                    } else {
                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    }
                    finish();
                } else {
                    startActivity(new Intent(SplashActivity.this, GetStartDemo.class));
                    finish();
                }
            }
        }, 2000);

        AppDelegate.LogT("GCM => " + String.valueOf(AppDelegate.getValue(this, Tags.REGISTRATION_ID)).trim());
    }

    private void getToken() {
        GCMClientManager pushClientManager = new GCMClientManager(this, getString(R.string.project_number));
        pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
            @Override
            public void onSuccess(String registrationId, boolean isNewRegistration) {
                Log.d("Registration id", registrationId);

                AppDelegate.save(SplashActivity.this, registrationId, Tags.REGISTRATION_ID);
                //send this registrationId to your server
            }

            @Override
            public void onFailure(String ex) {
                super.onFailure(ex);
                Log.d("onFailure", ex);
            }
        });
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
