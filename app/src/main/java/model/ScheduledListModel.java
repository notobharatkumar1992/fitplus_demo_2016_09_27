package model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by admin on 30-08-2016.
 */
public class ScheduledListModel implements Parcelable {
    public int status, dataFlow, id, team_id, coach_id, training_period, is_confirm,log_hours;
    public String coach, training_date, teamName, teamAvtar;
    public ArrayList<String> teamMembers;
    public double latitude, longitude;
    public ArrayList<String> teamMembersAvtar;

    public ScheduledListModel() {
    }

    protected ScheduledListModel(Parcel in) {
        status = in.readInt();
        dataFlow = in.readInt();
        id = in.readInt();
        team_id = in.readInt();
        coach_id = in.readInt();
        training_period = in.readInt();
        is_confirm = in.readInt();
        coach = in.readString();
        training_date = in.readString();
        teamName = in.readString();
        teamAvtar = in.readString();
        teamMembers = in.createStringArrayList();
        latitude = in.readDouble();
        longitude = in.readDouble();
        teamMembersAvtar = in.createStringArrayList();
        log_hours=in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataFlow);
        dest.writeInt(id);
        dest.writeInt(team_id);
        dest.writeInt(coach_id);
        dest.writeInt(training_period);
        dest.writeInt(is_confirm);
        dest.writeString(coach);
        dest.writeString(training_date);
        dest.writeString(teamName);
        dest.writeString(teamAvtar);
        dest.writeStringList(teamMembers);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeStringList(teamMembersAvtar);
        dest.writeInt(log_hours);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ScheduledListModel> CREATOR = new Creator<ScheduledListModel>() {
        @Override
        public ScheduledListModel createFromParcel(Parcel in) {
            return new ScheduledListModel(in);
        }

        @Override
        public ScheduledListModel[] newArray(int size) {
            return new ScheduledListModel[size];
        }
    };
}
