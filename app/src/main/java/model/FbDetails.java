package model;

import com.fitplus.AppDelegate;

import org.json.JSONException;
import org.json.JSONObject;

import Constants.Tags;

/**
 * Created by admin on 28-07-2016.
 */
public class FbDetails {
    public FbDetails() {
    }

    public Fb_detail_GetSet getFacebookDetail(String string) {
        Fb_detail_GetSet fb_detail_getSet=new Fb_detail_GetSet();
        try {
            AppDelegate.LogT("message+"+string+"");
            JSONObject object=new JSONObject(string);

            fb_detail_getSet.setSocial_id(object.getString(Tags.id));
            fb_detail_getSet.setFirstname(object.getString(Tags.first_name));
            fb_detail_getSet.setLast_name(object.getString(Tags.last_name));
            fb_detail_getSet.setBitrhday(object.getString(Tags.birthday));
            fb_detail_getSet.setGender(object.getString(Tags.gender));
            fb_detail_getSet.setName(object.getString(Tags.name));
            fb_detail_getSet.setGid("");
            fb_detail_getSet.setSocial_id("");
            AppDelegate.LogT(fb_detail_getSet+"");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  fb_detail_getSet;
    }
}
