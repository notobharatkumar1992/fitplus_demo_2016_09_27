package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 29-08-2016.
 */
public class Notification_Fragments_Model implements Parcelable {
    public int status, dataFlow,notify_id,team_id,user_id,action_taken,teamMembers,coach_id;
    public String teamname,team_avtar,message,notificaton_type;
    public String user_img_one,user_name_one;
    public String FirstAvtar,ThirdAvtar,SecondAvtar,username_second,username_first,username_third;

    public Notification_Fragments_Model() {
    }

    @Override
    public String toString() {
        return "Notification_Fragments_Model{" +
                "status=" + status +
                ", dataFlow=" + dataFlow +
                ", notify_id=" + notify_id +
                ", team_id=" + team_id +
                ", user_id=" + user_id +
                ", action_taken=" + action_taken +
                ", teamMembers=" + teamMembers +
                ", coach_id=" + coach_id +
                ", teamname='" + teamname + '\'' +
                ", team_avtar='" + team_avtar + '\'' +
                ", message='" + message + '\'' +
                ", notificaton_type='" + notificaton_type + '\'' +
                ", user_img_one='" + user_img_one + '\'' +
                ", user_name_one='" + user_name_one + '\'' +
                ", FirstAvtar='" + FirstAvtar + '\'' +
                ", ThirdAvtar='" + ThirdAvtar + '\'' +
                ", SecondAvtar='" + SecondAvtar + '\'' +
                ", username_second='" + username_second + '\'' +
                ", username_first='" + username_first + '\'' +
                ", username_third='" + username_third + '\'' +
                '}';
    }

    protected Notification_Fragments_Model(Parcel in) {
        status = in.readInt();
        dataFlow = in.readInt();
        notify_id = in.readInt();
        team_id = in.readInt();
        user_id = in.readInt();
        action_taken = in.readInt();
        teamMembers = in.readInt();
        coach_id = in.readInt();
        teamname = in.readString();
        team_avtar = in.readString();
        message = in.readString();
        notificaton_type = in.readString();
        user_img_one = in.readString();
        user_name_one = in.readString();
        FirstAvtar = in.readString();
        ThirdAvtar = in.readString();
        SecondAvtar = in.readString();
        username_second = in.readString();
        username_first = in.readString();
        username_third = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataFlow);
        dest.writeInt(notify_id);
        dest.writeInt(team_id);
        dest.writeInt(user_id);
        dest.writeInt(action_taken);
        dest.writeInt(teamMembers);
        dest.writeInt(coach_id);
        dest.writeString(teamname);
        dest.writeString(team_avtar);
        dest.writeString(message);
        dest.writeString(notificaton_type);
        dest.writeString(user_img_one);
        dest.writeString(user_name_one);
        dest.writeString(FirstAvtar);
        dest.writeString(ThirdAvtar);
        dest.writeString(SecondAvtar);
        dest.writeString(username_second);
        dest.writeString(username_first);
        dest.writeString(username_third);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Notification_Fragments_Model> CREATOR = new Creator<Notification_Fragments_Model>() {
        @Override
        public Notification_Fragments_Model createFromParcel(Parcel in) {
            return new Notification_Fragments_Model(in);
        }

        @Override
        public Notification_Fragments_Model[] newArray(int size) {
            return new Notification_Fragments_Model[size];
        }
    };
}
