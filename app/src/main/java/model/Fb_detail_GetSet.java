package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 28-07-2016.
 */
public class Fb_detail_GetSet implements Parcelable {
   public String social_id,gid;
   public String firstname,last_name, emailid,gender,bitrhday,name;

    public Fb_detail_GetSet() {
    }

    protected Fb_detail_GetSet(Parcel in) {
        social_id = in.readString();
        gid = in.readString();
        firstname = in.readString();
        last_name = in.readString();
        emailid = in.readString();
        gender = in.readString();
        bitrhday = in.readString();
        name = in.readString();
    }

    public static final Creator<Fb_detail_GetSet> CREATOR = new Creator<Fb_detail_GetSet>() {
        @Override
        public Fb_detail_GetSet createFromParcel(Parcel in) {
            return new Fb_detail_GetSet(in);
        }

        @Override
        public Fb_detail_GetSet[] newArray(int size) {
            return new Fb_detail_GetSet[size];
        }
    };

    public String getSocial_id() {
        return social_id;
    }

    public void setSocial_id(String social_id) {
        this.social_id = social_id;
    }

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBitrhday() {
        return bitrhday;
    }

    public void setBitrhday(String bitrhday) {
        this.bitrhday = bitrhday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Fb_detail_GetSet{" +
                "social_id='" + social_id + '\'' +
                ", gid='" + gid + '\'' +
                ", firstname='" + firstname + '\'' +
                ", last_name='" + last_name + '\'' +
                ", emailid='" + emailid + '\'' +
                ", gender='" + gender + '\'' +
                ", bitrhday='" + bitrhday + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(social_id);
        dest.writeString(gid);
        dest.writeString(firstname);
        dest.writeString(last_name);
        dest.writeString(emailid);
        dest.writeString(gender);
        dest.writeString(bitrhday);
        dest.writeString(name);
    }
}
