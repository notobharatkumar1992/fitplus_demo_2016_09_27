package fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.fitplus.AppDelegate;
import com.fitplus.BeginnerQuestionActivity;
import com.fitplus.LoginActivity;
import com.fitplus.MainActivity;
import com.fitplus.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import Async.LocationAddress;
import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import UI.CircleImageView;
import adapters.LanguageAdapter;
import interfaces.OnReciveServerResponse;
import model.PostAysnc_Model;
import model.UserDataModel;
import utils.Prefs;

import static android.media.ExifInterface.ORIENTATION_NORMAL;
import static android.media.ExifInterface.TAG_ORIENTATION;

/**
 * Created by admin on 26-07-2016.
 */
public class SettingsFragment extends Fragment implements OnClickListener, OnReciveServerResponse {

    private View rootview;

    private CircleImageView cimg_upload_image;
    private ImageView img_loading, facebook, twitter;
    private EditText password, lname, fname, confirm_password, about_me;
    private Bitmap OriginalPhoto;
    private TextView email, location;
    public static File capturedFile;
    public static Uri imageURI = null;
    public static String str_file_path = "";
    public static Uri imageUri;
    public static Uri cropimageUri;
    private Prefs prefs;
    private UserDataModel userDataModel;
    public static  Handler mHandler;
    private AnimationDrawable frameAnimation;
    private ScrollView scroll;

    private ImageView remember_me, notification;
    public TextView user_category, txt_language;

    private Dialog dialog;
    private ListView list;
    private String lang;
    private int notification_type = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.setting, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        prefs = new Prefs(getActivity());

        prefs = new Prefs(getActivity());
        userDataModel = prefs.getUserdata();
        initView();
        setHandler();
        setValues();

    }

    private void setValues() {
        scroll.fullScroll(View.FOCUS_UP);//if you move at the end of the scroll
        scroll.pageScroll(View.FOCUS_UP);//if you move at the middle of the scroll
        scroll.smoothScrollTo(0, 0);
        img_loading.setVisibility(View.VISIBLE);
        frameAnimation = (AnimationDrawable) img_loading.getDrawable();
        frameAnimation.setCallback(img_loading);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();
        Picasso.with(getActivity()).load(userDataModel.avtar).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                AppDelegate.LogT("onBitmapLoaded" + userDataModel.avtar);
                cimg_upload_image.setImageBitmap(bitmap);
                frameAnimation.stop();
                img_loading.setVisibility(View.GONE);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                AppDelegate.LogT("onBitmapFailed" + userDataModel.avtar);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                AppDelegate.LogT("onPrepareLoad" + userDataModel.avtar);
            }
        });
        email.setText(userDataModel.email + "");
        fname.setText(AppDelegate.isValidString(userDataModel.first_name) ? userDataModel.first_name + "" : "");
        lname.setText(AppDelegate.isValidString(userDataModel.last_name) ? userDataModel.last_name + "" : "");
        about_me.setText(AppDelegate.isValidString(userDataModel.about_me) ? userDataModel.about_me + "" : "");

        setloc(Double.parseDouble(new Prefs(getActivity()).getStringValue(Tags.TAG_LAT, "")), Double.parseDouble(new Prefs(getActivity()).getStringValue(Tags.TAG_LONG, "")));
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                AppDelegate.LogT("dispatchMessage handler calld");
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 1) {
                    AppDelegate.LogT("set User category" + userDataModel.fat_status + (userDataModel.sex_group) + (userDataModel.type) + "");
                    userDataModel = prefs.getUserdata();
                    user_category.setText((AppDelegate.isValidString(userDataModel.fat_status) ? userDataModel.fat_status : "") + ", " + (AppDelegate.isValidString(userDataModel.sex_group) ? userDataModel.sex_group : "") + ", " + (AppDelegate.isValidString(userDataModel.type) ? userDataModel.type : " "));
                } else if (msg.what == 2) {
                    setAddressFromGEOcoder(msg.getData());
                } else if (msg.what == 12) {

                } else if (msg.what == 13) {
                    executeEdit();
                }
            }
        };
    }

    private void Dialogue() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        list = (ListView) dialog.findViewById(R.id.dialog_list);
    }

    private void executeEdit() {
        if (notification.isSelected()) {
            notification_type = 1;
        } else {
            notification_type = 0;
        }
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();

                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(getActivity()).getUserdata().userId, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.first_name, fname.getText().toString() + "");
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.last_name, lname.getText().toString() + "");
//                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.latitude,"");
//                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.longitude,"");
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.notification_status, notification_type, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.language, txt_language.getText().toString());


                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.password, password.getText().toString() + "");
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.latitude, new Prefs(getActivity()).getStringValue(Tags.TAG_LAT, ""));
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.longitude, new Prefs(getActivity()).getStringValue(Tags.TAG_LONG, ""));

                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.about_me, about_me.getText().toString() + "");
//
                // AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.email, email_address.getText().toString() + "");
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                PostAsync mPostAsyncObj;

                mPostAsyncObj = new PostAsync(getActivity(), SettingsFragment.this, ServerRequestConstants.EDIT_PROFILE,
                        mPostArrayList, null);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.uploadedfile0, capturedFile, ServerRequestConstants.Key_PostFileValue);

                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }

    }

    private boolean validateAll() {
        boolean result = true;
        if (AppDelegate.isValidString(password.getText().toString()) && AppDelegate.isValidString(confirm_password.getText().toString())) {
            if (!password.getText().toString().equals(confirm_password.getText().toString())) {
                result = false;
                AppDelegate.showToast(getActivity(), getResources().getString(R.string.formatch));
            } else {

            }
        } else if (AppDelegate.isValidString(password.getText().toString()) && !AppDelegate.isValidString(confirm_password.getText().toString())) {
            result = false;
            AppDelegate.showToast(getActivity(), getResources().getString(R.string.forconpass));

        } else if (!AppDelegate.isValidString(password.getText().toString()) && AppDelegate.isValidString(confirm_password.getText().toString())) {
            result = false;
            AppDelegate.showToast(getActivity(), getResources().getString(R.string.forpass));

        } else if (!AppDelegate.isValidString(fname.getText().toString())) {
            result = false;
            AppDelegate.showToast(getActivity(), getResources().getString(R.string.forname));
        } else if (!AppDelegate.isValidString(lname.getText().toString())) {
            result = false;
            AppDelegate.showToast(getActivity(), getResources().getString(R.string.forlastname));
        }
        return result;
    }

    public void showImageSelectorList() {
        AppDelegate.hideKeyBoard(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ListView modeList = new ListView(getActivity());
        String[] stringArray = new String[]{"Camera", "Gallery", "Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        new OpenCamera().execute();
                        break;
                    case 1:
                        dialog.dismiss();
                        openGallery();
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    public void openGallery() {
        startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), AppDelegate.SELECT_PICTURE);
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), getActivity().getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.GET_PROFILE)) {
            //parsegetprofile(result);
        } else if (apiName.equals(ServerRequestConstants.EDIT_PROFILE)) {
            parseEditProfile(result);
        }
    }
    private void parseEditProfile(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.gid = object.getString(Tags.gid);
                userDataModel.fid = object.getString(Tags.fid);
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.password = object.getString(Tags.password);
                userDataModel.created = object.getString(Tags.created);
                userDataModel.str_Gender = object.getString(Tags.gender);
                userDataModel.userId = object.getInt(Tags.user_id);
                userDataModel.dob = object.getString(Tags.birthdate);
                userDataModel.nickname = object.getString(Tags.nick_name);
                userDataModel.avtar = object.getString(Tags.avtar);
                userDataModel.avtar_thumb = object.getString(Tags.avtar_thumb);
                userDataModel.views = object.getString(Tags.views);
                userDataModel.follower_count = object.getInt(Tags.follower_count);
                userDataModel.profile_visibility = object.getInt(Tags.profile_visibility);
                userDataModel.presentation_vedio = object.getString(Tags.presentation_vedio);
                userDataModel.bank_account = object.getString(Tags.bank_account);
                userDataModel.certificates = object.getString(Tags.certificates);
                userDataModel.country_id = object.getString(Tags.country_id);
                userDataModel.role_id = object.getInt(Tags.role_id);
                userDataModel.state_id = object.getString(Tags.state_id);
                userDataModel.city_id = object.getString(Tags.city_id);
                userDataModel.post_code = object.getString(Tags.post_code);
                userDataModel.sex_group = object.getString(Tags.sex_group);
                userDataModel.fat_status = object.getString(Tags.fat_status);
                userDataModel.type = object.getString(Tags.type);
                userDataModel.token = object.getString(Tags.token);
                userDataModel.is_login = object.getInt(Tags.is_login);
                userDataModel.is_social = object.getInt(Tags.is_social);
                userDataModel.is_verified = object.getInt(Tags.is_verified);
                userDataModel.latitude = object.getDouble(Tags.latitude);
                userDataModel.longitude = object.getDouble(Tags.longitude);
                userDataModel.about_me = object.getString(Tags.about_me);
                userDataModel.notification_status = object.getInt(Tags.notification_status);
                userDataModel.language = object.getString(Tags.language);
                prefs.setUserData(userDataModel);
                updateGlobal();

                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                AppDelegate.LogT("after  updatation" + new Prefs(getActivity()).getUserdata());
                /*finish();*/
            } else {
                if (jsonObject.getInt(Tags.status) == 0) {
                    AppDelegate.ShowDialog(getActivity(), jsonObject.getString(Tags.message), "alert");
                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
                    AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            AppDelegate.LogE(e);
        }
    }

    private void updateGlobal() {

    // Side bar
    MainActivity.getInstance().new LoadImage().execute(new Prefs(getActivity()).getUserdata().avtar);
    // For profile
    if (ViewProfileFragment.mHandler != null) {
        AppDelegate.LogT("updateGlobal handler calld");
        ViewProfileFragment.mHandler.sendEmptyMessage(1);
    }
}
    private void setloc(double latitude, double longitude) {
        LocationAddress.getAddressFromLocation(latitude, longitude, getActivity(), mHandler);
    }


    private void setAddressFromGEOcoder(Bundle data) {
        location.setText(data.getString(Tags.ADDRESS) + "");
    }

    class OpenCamera extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            AppDelegate.showProgressDialog(getActivity());
        }

        @Override
        protected Void doInBackground(Void... params) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            String str_file_path = getNewFile();
            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true) {
                AppDelegate.showToast(getActivity(), "File not created, please try agin later.");
                return null;
            }
            imageURI = Uri.fromFile(capturedFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            AppDelegate.hideProgressDialog(getActivity());
        }
    }

    public String getNewFile() {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + getString(R.string.app_name));
        } else {
            directoryFile = getActivity().getDir(getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try {
                if (capturedFile.createNewFile()) {
                    AppDelegate.LogT("File created = "
                            + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        AppDelegate.LogT("onActivityResult Profile");
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case AppDelegate.SELECT_PICTURE:
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getData();
                    imageUri = uri;
                    AppDelegate.LogT("uri=" + uri);
                    if (uri != null) {
                        // User had pick an image.
                        Cursor cursor = getActivity()
                                .getContentResolver()
                                .query(uri,
                                        new String[]{MediaStore.Images.ImageColumns.DATA},
                                        null, null, null);
                        cursor.moveToFirst();
                        // Link to the image
                        final String imageFilePath = cursor.getString(0);
                        cursor.close();
                        int orientation = 0;
                        if (imageFilePath != null && !imageFilePath.equalsIgnoreCase("")) {
                            try {
                                ExifInterface ei = new ExifInterface(imageFilePath);
                                orientation = ei.getAttributeInt(
                                        TAG_ORIENTATION,
                                        ORIENTATION_NORMAL);
                            } catch (IOException e) {
                                AppDelegate.LogE(e);
                            }
                            capturedFile = new File(imageFilePath);
                            OriginalPhoto = AppDelegate.decodeFile(capturedFile);

                            Log.d("image", "orientation is " + orientation);
                            Matrix matrix = new Matrix();

                            switch (orientation) {
                                case ExifInterface.ORIENTATION_NORMAL:
                                    System.out.println("ORIENTATION_NORMAL" + orientation);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                                    matrix.setScale(-1, 1);
                                    System.out.println("ORIENTATION_FLIP_HORIZONTAL" + orientation);

                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                                    matrix.setRotate(180);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSPOSE:
                                    matrix.setRotate(90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSVERSE:
                                    matrix.setRotate(-90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(-90);
                                    break;
                                default:
                                    break;

                            }
                            Log.d("OriginalPhoto", "after OriginalPhoto is " + OriginalPhoto);
                            if (OriginalPhoto != null) {
                                OriginalPhoto = Bitmap.createBitmap(OriginalPhoto, 0, 0, OriginalPhoto.getWidth(),
                                        OriginalPhoto.getHeight(), matrix, true);
                                Uri rotateduri = getImageUri(getActivity(), OriginalPhoto);
                                if (rotateduri != null) {
                                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
                                        performCrop(rotateduri);
                                    else {
                                        this.OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                                        cimg_upload_image.setImageBitmap(OriginalPhoto);
                                        img_loading.setVisibility(View.GONE);
                                    }
                                } else {
                                    if (imageUri != null)
                                        performCrop(imageUri);
                                    else
                                        Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), "Failed to deliver result.", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                }
                break;
            case AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE:
                AppDelegate.LogT("onActivityResult str_file_path = " + str_file_path + ", imageUri = " + imageUri);
                //Get our saved file into a bitmap object:
                try {
                    if (resultCode == Activity.RESULT_OK) {
                        int orientation = 0;
                        Uri uri = Uri.fromFile(new File(capturedFile.getAbsolutePath()));

                        BitmapFactory.Options o = new BitmapFactory.Options();
                        o.inJustDecodeBounds = true;
                        OriginalPhoto = AppDelegate.decodeSampledBitmapFromFile(capturedFile.getAbsolutePath(), 1000, 700);
                        Log.d("OriginalPhoto", "OriginalPhoto is " + OriginalPhoto);
                        try {
                            ExifInterface ei = new ExifInterface(
                                    uri.getPath());

                            orientation = ei.getAttributeInt(
                                    ExifInterface.TAG_ORIENTATION,
                                    ExifInterface.ORIENTATION_NORMAL);

                            Log.d("image", "orientation is " + orientation);
                            Matrix matrix = new Matrix();

                            switch (orientation) {
                                case ExifInterface.ORIENTATION_NORMAL:
                                    System.out.println("ORIENTATION_NORMAL" + orientation);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                                    matrix.setScale(-1, 1);
                                    System.out.println("ORIENTATION_FLIP_HORIZONTAL" + orientation);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                                    matrix.setRotate(180);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSPOSE:
                                    matrix.setRotate(90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSVERSE:
                                    matrix.setRotate(-90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(-90);
                                    break;
                            }
                            Log.d("OriginalPhoto", "after OriginalPhoto is " + OriginalPhoto);
                            OriginalPhoto = Bitmap.createBitmap(OriginalPhoto, 0, 0, OriginalPhoto.getWidth(),
                                    OriginalPhoto.getHeight(), matrix, true);

                            Uri rotateduri = getImageUri(getActivity(), OriginalPhoto);
                            if (rotateduri != null) {
                                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
                                    performCrop(rotateduri);
                                else {
                                    this.OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                                    cimg_upload_image.setImageBitmap(OriginalPhoto);
                                    img_loading.setVisibility(View.GONE);
                                }

                            } else {
                                AppDelegate.showToast(getActivity(), "Failure delivery result, please try again later.");
                            }

                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                            AppDelegate.showToast(getActivity(), "Failure delivery result, please try again later.");
                        }
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                break;
            case AppDelegate.PIC_CROP: {
                AppDelegate.LogT("at onActivicTyResult cropimageUri = " + cropimageUri);
                try {
                    cropimageUri = data.getData();
                    if (resultCode == Activity.RESULT_OK) {
                        if (OriginalPhoto != null) {
                            OriginalPhoto.recycle();
                            OriginalPhoto = null;
                        }
                        OriginalPhoto = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), cropimageUri);
                        AppDelegate.LogT("at onActivicTyResult selectedBitmap = " + OriginalPhoto);
                        OriginalPhoto = Bitmap.createScaledBitmap(
                                OriginalPhoto, 240, 240, true);
                        AppDelegate.LogT("at onActivicTyResult OriginalPhoto = " + OriginalPhoto);
                        cimg_upload_image.setImageBitmap(AppDelegate.getRoundedCornerBitmap(OriginalPhoto, AppDelegate.convertdp(getActivity(), 100)));
                        img_loading.setVisibility(View.GONE);
                        capturedFile = new File(getNewFile());
                        FileOutputStream fOut = null;
                        try {
                            fOut = new FileOutputStream(capturedFile);
                        } catch (FileNotFoundException e) {
                            AppDelegate.LogE(e);
                        }
                        OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                        try {
                            fOut.flush();
                        } catch (IOException e) {
                            AppDelegate.LogE(e);
                        }
                        try {
                            fOut.close();
                        } catch (IOException e) {
                            AppDelegate.LogE(e);
                        }
                    } else {
                        AppDelegate.LogE("at onActivicTyResult failed to crop");
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "CropTitle", null);
        return Uri.parse(path);
    }

    private void performCrop(Uri picUri) {
        try {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "cropuser");
            values.put(MediaStore.Images.Media.DESCRIPTION, "cropuserPic");
            cropimageUri = getActivity().getContentResolver().insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("scale", true);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 1000);
            cropIntent.putExtra("outputY", 1000);
            // retrieve data on return
            cropIntent.putExtra("return-data", false);
//            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, cropimageUri);
            // start the activity - we handle returning in onActivityResult
            AppDelegate.LogT("at performCrop cropimageUri = " + cropimageUri + ", picUri = " + picUri);
            startActivityForResult(cropIntent, AppDelegate.PIC_CROP);
        } catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    String setLanguage() {
        final String[] language = {getActivity().getResources().getString(R.string.french), getActivity().getResources().getString(R.string.english)};
        Dialogue();
        LanguageAdapter spinnerAdapter = new LanguageAdapter(getActivity(), language);
        list.setAdapter(spinnerAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                lang = language[position];
                txt_language.setText(lang);
                dialog.dismiss();
            }
        });
        dialog.show();
        return lang;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppDelegate.LogT("ViewProfile onDestroyView called");
        mHandler = null;
    }

    private void initView() {
        rootview.findViewById(R.id.back).setOnClickListener(this);
        user_category = (TextView) rootview.findViewById(R.id.user_category);
        user_category.setOnClickListener(this);
        txt_language = (TextView) rootview.findViewById(R.id.language);
        txt_language.setOnClickListener(this);
        remember_me = (ImageView) rootview.findViewById(R.id.remember_me);
        remember_me.setOnClickListener(this);
        notification = (ImageView) rootview.findViewById(R.id.notification);
        notification.setOnClickListener(this);
        user_category.setText((AppDelegate.isValidString(userDataModel.fat_status) ? userDataModel.fat_status : "") + " ," + (AppDelegate.isValidString(userDataModel.sex_group) ? userDataModel.sex_group : "") + " ," + (AppDelegate.isValidString(userDataModel.type) ? userDataModel.type : " "));
        if (AppDelegate.isValidString(userDataModel.language)) {
            txt_language.setText(userDataModel.language + "");
        } else {
            txt_language.setText(getActivity().getResources().getString(R.string.english));
        }
        if (userDataModel.notification_status == 1) {
            //AppDelegate.LogT("notification on -====" + (prefs.getnotification(Tags.notification_value)));
            AppDelegate.LogT("userDataModel.notification_status == 1====" + userDataModel.notification_status);
            notification.setSelected(true);
        } else {
            AppDelegate.LogT("userDataModel.notification_status == 0===" + userDataModel.notification_status);
            //AppDelegate.LogT("notification off-====" + (prefs.getnotification(Tags.notification_value)));
            notification.setSelected(false);
        }
        String emailid = AppDelegate.getValue(getActivity(), Tags.Email);
        if (AppDelegate.isValidString(emailid)) {
            AppDelegate.Log("emailid", emailid + "" + "hellooo");
            remember_me.setSelected(true);
        } else {
            AppDelegate.Log("emailid", emailid + "" + "byee");
            remember_me.setSelected(false);
        }
        rootview.findViewById(R.id.done).setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cimg_upload_image:
                showImageSelectorList();
                break;
            case R.id.done:
                if (validateAll() == true) {
                    executeEdit();
                }

                break;
            case R.id.language:
                setLanguage();
                break;
            case R.id.back:
                getFragmentManager().popBackStack();
                break;
            case R.id.user_category:
                AppDelegate.setting_or_not = true;
                startActivity(new Intent(getActivity(), BeginnerQuestionActivity.class));
                break;
            case R.id.remember_me:
                if (remember_me.isSelected()) {
                    remember_me.setSelected(false);
                    AppDelegate.remember_me = false;
                    AppDelegate.save(getActivity(), "", Tags.Email);
                    AppDelegate.save(getActivity(), "", Tags.Password);
                    AppDelegate.LogT("userdata is on=====" + AppDelegate.getValue(getActivity(), Tags.Email));
                    update();
                } else {
                    remember_me.setSelected(true);
                    AppDelegate.remember_me = true;
                    AppDelegate.LogT("userdata is off=====" + AppDelegate.getValue(getActivity(), Tags.Email));
                    update();
                }
                break;
            case R.id.notification:
                if (notification.isSelected()) {
                    prefs.putnotification(Tags.notification_value, false);
                    notification.setSelected(false);
                    AppDelegate.LogT("off=====" + prefs.getnotification(Tags.notification_value));
                } else {
                    prefs.putnotification(Tags.notification_value, true);
                    notification.setSelected(true);
                    AppDelegate.LogT("on=====" + prefs.getnotification(Tags.notification_value));
                }
                break;
        }
    }

    private void execute_setting() {
        if (notification.isSelected()) {
            notification_type = 1;
        } else {
            notification_type = 0;
        }
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(getActivity()).getUserdata().userId);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.type, 2, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.notification_status, notification_type, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.language, txt_language.getText().toString());


                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(getActivity(), SettingsFragment.this, ServerRequestConstants.SETTINGS,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void update() {
        if (LoginActivity.mHandler != null) {
            AppDelegate.LogT("updateGlobal handler calld");
            LoginActivity.mHandler.sendEmptyMessage(1);
        }
    }

    private void parseSetting(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
            if (jsonObject.getInt(Tags.status) == 1 && jsonObject.getInt(Tags.dataFlow) == 1) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.password = object.getString(Tags.password);
                userDataModel.created = object.getString(Tags.created);
                userDataModel.str_Gender = object.getString(Tags.gender);
                userDataModel.userId = object.getInt(Tags.user_id);
                userDataModel.dob = object.getString(Tags.birthdate);
                userDataModel.nickname = object.getString(Tags.nick_name);
                userDataModel.avtar = object.getString(Tags.avtar);
                userDataModel.views = object.getString(Tags.views);
                userDataModel.follower_count = object.getInt(Tags.follower_count);
                userDataModel.profile_visibility = object.getInt(Tags.profile_visibility);
                userDataModel.presentation_vedio = object.getString(Tags.presentation_vedio);
                userDataModel.bank_account = object.getString(Tags.bank_account);
                userDataModel.certificates = object.getString(Tags.certificates);
                userDataModel.country_id = object.getString(Tags.country_id);
                userDataModel.role_id = object.getInt(Tags.role_id);
                userDataModel.state_id = object.getString(Tags.state_id);
                userDataModel.city_id = object.getString(Tags.city_id);
                userDataModel.post_code = object.getString(Tags.post_code);
                userDataModel.sex_group = object.getString(Tags.sex_group);
                userDataModel.fat_status = object.getString(Tags.fat_status);
                userDataModel.type = object.getString(Tags.type);
                userDataModel.token = object.getString(Tags.token);
                userDataModel.is_login = object.getInt(Tags.is_login);
                userDataModel.is_social = object.getInt(Tags.is_social);
                userDataModel.is_verified = object.getInt(Tags.is_verified);
                userDataModel.latitude = object.getDouble(Tags.latitude);
                userDataModel.longitude = object.getDouble(Tags.longitude);
                userDataModel.notification_status = object.getInt(Tags.notification_status);
                userDataModel.language = object.getString(Tags.language);
                prefs.setUserData(userDataModel);
                // prefs.setRemembered(toggle.isSelected() + "");
                prefs.putUserId(String.valueOf(userDataModel.userId));

            }
        } catch (Exception e) {
            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            AppDelegate.LogE(e);
        }
    }
}
