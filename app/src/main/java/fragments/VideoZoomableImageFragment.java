package fragments;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;

import com.fitplus.AppDelegate;
import com.fitplus.R;
import com.fitplus.ViewImageVideoActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import Constants.Tags;
import UI.TouchImageView;
import model.FitDayModel;
import universalvideoview.UniversalVideoView;

/**
 * Created by admin on 17-08-2016.
 */

public class VideoZoomableImageFragment extends Fragment implements UniversalVideoView.VideoViewCallback {

    ImageView image1, image2, image3, image4, image5, image6;
    private Bundle bundle;
    TextView text;
    // ImageView star, video;
    TouchImageView image;
    // public VideoView videoView;
    LinearLayout ll_main, hide_visibility;
    FitDayModel fitDayModelArrayList;
    private MediaController mMediaController;
    Boolean show = false;
    int position;
    ImageLoader imageLoader = ImageLoader.getInstance();
    UniversalVideoView mVideoView;
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
            /*.showImageForEmptyUri(R.drawable.dummy_user_icon)
            .showImageOnFail(fallback)
            .showImageOnLoading(fallback).*/build();
    private int cachedHeight;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        bundle = getArguments();
        fitDayModelArrayList = bundle.getParcelable(Tags.FitDayModel);
        position = bundle.getInt("position", 0);
        return inflater.inflate(R.layout.fit_day_item_zoomable, container, false);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (ViewImageVideoActivity.onListItemClickListener != null) {
                ViewImageVideoActivity.onListItemClickListener.setOnListItemClickListener("visibility", 1);
            }
        } else {
            if (ViewImageVideoActivity.onListItemClickListener != null) {
                ViewImageVideoActivity.onListItemClickListener.setOnListItemClickListener("visibility", 2);
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        image = (TouchImageView) view.findViewById(R.id.img_content);
        // videoView = (VideoView) view.findViewById(R.id.videoView);
        mVideoView = (UniversalVideoView) view.findViewById(R.id.videoView);

        mVideoView.setVideoViewCallback(this);
        view.findViewById(R.id.main_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppDelegate.LogT(" view.setOnClickListener => " + (ViewImageVideoActivity.onListItemClickListener != null));
                if (ViewImageVideoActivity.onListItemClickListener != null) {
                    ViewImageVideoActivity.onListItemClickListener.setOnListItemClickListener("clicked", position);
                }
            }
        });

        AppDelegate.LogT("File type => " + fitDayModelArrayList.file_type);
        if (fitDayModelArrayList.file_type == 1) {
            image.setVisibility(View.VISIBLE);
            mVideoView.setVisibility(View.GONE);
            imageLoader.displayImage(fitDayModelArrayList.file_name, image, options);
        } else if (fitDayModelArrayList.file_type == 2) {
            AppDelegate.LogT("File type => " + fitDayModelArrayList.file_type + ", " + fitDayModelArrayList.file_name);
            image.setVisibility(View.GONE);
            mVideoView.setVisibility(View.VISIBLE);
            playvdo(fitDayModelArrayList.file_name);
        }

    }

    private void playvdo(final String url) {
        mVideoView.setVisibility(View.VISIBLE);

        mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                AppDelegate.LogT("onError => " + mp.getDuration() + ", " + position);
                AppDelegate.showToast(getActivity(), "Can't play this video");
                if (ViewImageVideoActivity.onListItemClickListener != null) {
                    ViewImageVideoActivity.onListItemClickListener.setOnListItemClickListener("clicked", position);
                }
                return true;
            }
        });

        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                show = true;
                // setVideoAreaSize(url);
                AppDelegate.LogT("onPrepared => " + mp.getDuration() + ", ");
                if (ViewImageVideoActivity.onListItemClickListener != null) {
                    ViewImageVideoActivity.onListItemClickListener.setOnListItemClickListener("playing", position);
                }
               /* if (ViewImageVideoActivity.getDurationClickListener != null) {
                    ViewImageVideoActivity.getDurationClickListener.setonGetDurationClickListener("video", position, mp.getDuration());
                }*/
                image.setVisibility(View.GONE);
                mVideoView.setVisibility(View.VISIBLE);
                // AppDelegate.LogT(" image.setVisibility(View.GONE);" + (image.getVisibility() == View.VISIBLE));
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        image.setVisibility(View.VISIBLE);
                        if (ViewImageVideoActivity.onListItemClickListener != null) {
                            ViewImageVideoActivity.onListItemClickListener.setOnListItemClickListener("stopped", position);
                        }
                    }
                });
            }
        });
        mVideoView.setVideoURI(Uri.parse(url));
    }

    public void playVideo() {
        if (mVideoView != null) {
            mVideoView.requestFocus();
            mVideoView.start();
        }
    }

    private void setVideoAreaSize(final String VIDEO_URL) {
        mVideoView.post(new Runnable() {
            @Override
            public void run() {
                int width = mVideoView.getWidth();
                cachedHeight = (int) (width * 405f / 720f);
                ViewGroup.LayoutParams videoLayoutParams = mVideoView.getLayoutParams();
                videoLayoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                videoLayoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
                mVideoView.setLayoutParams(videoLayoutParams);
                // mVideoView.requestFocus();
            }
        });
    }

    @Override
    public void onScaleChange(boolean isFullscreen) {
        AppDelegate.LogT("Media onScaleChange => ");
    }

    @Override
    public void onPause(MediaPlayer mediaPlayer) {
        AppDelegate.LogT("Media onPause => ");
    }

    @Override
    public void onStart(MediaPlayer mediaPlayer) {
        AppDelegate.LogT("Media onStart => ");
    }

    @Override
    public void onBufferingStart(MediaPlayer mediaPlayer) {
        AppDelegate.LogT("Media onBufferingStart => ");
    }

    @Override
    public void onBufferingEnd(MediaPlayer mediaPlayer) {
        AppDelegate.LogT("Media onBufferingEnd => ");
    }
}

