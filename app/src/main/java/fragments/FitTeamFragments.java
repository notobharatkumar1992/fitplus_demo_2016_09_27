package fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fitplus.AppDelegate;
import com.fitplus.CreateTeamActivity;
import com.fitplus.MainActivity;
import com.fitplus.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.TeamListAdapter;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.Create_Team_Model;
import model.PostAysnc_Model;
import model.TeamListModel;
import utils.Prefs;

/**
 * Created by admin on 26-07-2016.
 */
public class FitTeamFragments extends Fragment implements OnClickListener, AdapterView.OnItemClickListener, OnReciveServerResponse, OnDialogClickListener {
    private View rootview;
    RelativeLayout toolbar;
    TextView toolbar_title;
    ImageView tool_menu, create_team;
    RecyclerView team_list;
    private Bundle bundle;
    public static Handler mHandler;
    public boolean shouldUpdate = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.team_list, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 1) {
                   // if (shouldUpdate) {
                        execute_teamList();
                       // shouldUpdate = false;
                   // }
                }
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHandler = null;
    }

    private void initView() {
        toolbar = (RelativeLayout) rootview.findViewById(R.id.toolbar);
        toolbar_title = (TextView) rootview.findViewById(R.id.title);
        tool_menu = (ImageView) rootview.findViewById(R.id.menu);
        tool_menu.setOnClickListener(this);
        create_team = (ImageView) rootview.findViewById(R.id.create_team);
        create_team.setOnClickListener(this);
        team_list = (RecyclerView) rootview.findViewById(R.id.team_list);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        execute_teamList();
    }

    private void execute_teamList() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(getActivity()).getUserdata().userId);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(getActivity(), FitTeamFragments.this, ServerRequestConstants.TEAM_LIST,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.create_team:
                execute_createTeam();
                break;
            case R.id.menu:
                ((MainActivity) getActivity()).toggleSlider();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {

        }
    }

    private void execute_createTeam() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(getActivity()).getUserdata().userId);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.latitude, new Prefs(getActivity()).getStringValue(Tags.TAG_LAT, ""));
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.longitude, new Prefs(getActivity()).getStringValue(Tags.TAG_LONG, ""));
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(getActivity(), FitTeamFragments.this, ServerRequestConstants.CREATE_TEAM,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), getActivity().getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.TEAM_LIST)) {
            parseFitTeamList(result);

        } else if (apiName.equals(ServerRequestConstants.CREATE_TEAM)) {
            parseCreateTeamList(result);
            shouldUpdate = true;
        }
    }

    private void parseCreateTeamList(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                Create_Team_Model userDataModel = new Create_Team_Model();
                userDataModel.first_member_id = object.getString(Tags.first_member_id);
                userDataModel.team_name = object.getString(Tags.team_name);
                userDataModel.id = object.getInt(Tags.id);
                userDataModel.latitude = object.getString(Tags.latitude);
                userDataModel.created = object.getString(Tags.created);
                userDataModel.longitude = object.getString(Tags.longitude);
                userDataModel.team_avtar = object.getString(Tags.team_avtar);
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                bundle = new Bundle();
                bundle.putInt("key", 2);
                bundle.putParcelable(Tags.TeamName, userDataModel);
                Intent intent = new Intent(getActivity(), CreateTeamActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                //AppDelegate.showFragment(getActivity().getSupportFragmentManager(), new CreateTeamActivity(), R.id.container, bundle, null);
            } else {
                if (jsonObject.getInt(Tags.status) == 0) {
                    AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
//                    AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, FitTeamFragments.this);
                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
                    AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
//                    AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, FitTeamFragments.this);
                }
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), getResources().getString(R.string.response_error), "");
            AppDelegate.LogE(e);
        }
    }

    private void parseFitTeamList(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONArray response = jsonObject.getJSONArray(Tags.response);
                ArrayList<TeamListModel> team_list = new ArrayList<>();
                for (int i = 0; i < response.length(); i++) {
                    TeamListModel userDataModel = new TeamListModel();
                    JSONObject object = response.getJSONObject(i);
                    userDataModel.team_id = object.getInt(Tags.id);
                    userDataModel.team_name = object.getString(Tags.team_name);
                    userDataModel.objective = object.getString(Tags.objective);
                    userDataModel.first_member_id = object.getInt(Tags.first_member_id);
                    userDataModel.second_member_id = object.getInt(Tags.second_member_id);
                    userDataModel.third_member_id = object.getInt(Tags.third_member_id);
                    userDataModel.coach_id = object.getInt(Tags.coach_id);
                    userDataModel.lastMessage = object.getString(Tags.lastMessage);
                    userDataModel.latitude = object.getDouble(Tags.latitude);
                    userDataModel.longitude = object.getDouble(Tags.longitude);
                    userDataModel.created = object.getString(Tags.created);
                    userDataModel.team_avtar = object.getString(Tags.team_avtar);
                    userDataModel.datetime = object.getString(Tags.datetime);
                    /*if (object.has(Tags.avtar_thumb)) {
                        userDataModel.avtar_thumb = object.getString(Tags.avtar_thumb);
                    }*/
                    team_list.add(userDataModel);
                }
                showTeamlist(team_list);
                // AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, GlobalMapFragments.this);
            } else {
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
//                if (jsonObject.getInt(Tags.status) == 0) {
//                    AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, FitTeamFragments.this);
//                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null/*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
//                    AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, FitTeamFragments.this);
//                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(getActivity(), getResources().getString(R.string.response_error));
            AppDelegate.LogE(e);
        }
    }

    private void showTeamlist(ArrayList<TeamListModel> team_listArray) {
        TeamListAdapter mAdapter = new TeamListAdapter(getActivity(), team_listArray);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        team_list.setLayoutManager(mLayoutManager);
        team_list.setItemAnimator(new DefaultItemAnimator());
        team_list.setAdapter(mAdapter);
    }

    @Override
    public void setOnDialogClickListener(String name) {

    }
}
