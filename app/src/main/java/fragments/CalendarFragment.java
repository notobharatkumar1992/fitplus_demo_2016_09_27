package fragments;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitplus.AppDelegate;
import com.fitplus.R;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.Recycler_Calendar_dates_Adapter;
import interfaces.OnReciveServerResponse;
import model.PostAysnc_Model;
import model.ScheduledListModel;
import utils.Prefs;
import utils.decorators.CurrentDateDecorator;
import utils.decorators.EventDecorator;
import utils.decorators.MySelectorDecorator;
import utils.decorators.OneDayDecorator;

/**
 * Created by admin on 26-07-2016.
 */
public class CalendarFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener, OnReciveServerResponse {
    private Calendar currentCalender = Calendar.getInstance(Locale.getDefault());
    private RecyclerView recycler_dates;
    private MaterialCalendarView date;
    private TextView month;
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    public static Handler mHandler;
    public static String setdate;
    private SimpleDateFormat month_date;
    private OneDayDecorator oneDayDecorator = new OneDayDecorator(getActivity());
    private ImageView img_user_image;
    private Bitmap bitmap;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.calendar_frag, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        executeScheduleddates();
        setHandler();
        mHandler.sendEmptyMessage(1);
    }
    public class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {

            if (image != null) {
                try {
                    img_user_image.setImageBitmap(AppDelegate.blurRenderScript(getActivity(), image));
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }

            } else {

            }
        }
    }
  /*  private void testAddEvent() {

        addEvents(calendar.getTime());
        date.invalidate();

        logEventsByMonth(date);
    }*/

    private SimpleDateFormat dateFormatForDisplaying = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a", Locale.getDefault());

    private void logEventsByMonth(CompactCalendarView compactCalendarView) {
        currentCalender.setTime(new Date());
        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        currentCalender.set(Calendar.MONTH, Calendar.AUGUST);
        List<String> dates = new ArrayList<>();
        for (Event e : compactCalendarView.getEventsForMonth(new Date())) {
            dates.add(dateFormatForDisplaying.format(e.getTimeInMillis()));
        }
        Log.d("test", "Events for Aug with simple date formatter: " + dates);
        Log.d("test", "Events for Aug month using default local and timezone: " + compactCalendarView.getEventsForMonth(currentCalender.getTime()));
    }


    private void executeScheduleddates() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(getActivity()).getUserdata().userId);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(getActivity(), CalendarFragment.this, ServerRequestConstants.SCHEDULED_DATES,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }

    }

    private void initView(View rootview) {
        recycler_dates = (RecyclerView) rootview.findViewById(R.id.recycler_dates);
        date = (MaterialCalendarView) rootview.findViewById(R.id.date);
        month = (TextView) rootview.findViewById(R.id.month);
        img_user_image=(ImageView)rootview.findViewById(R.id.img_user_image);
        date=(MaterialCalendarView)rootview.findViewById(R.id.date);
        date.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                Date calender = date.getDate();
                 setdate = new SimpleDateFormat("yyyy-MM-dd").format(calender).toString();
                Log.e("Selected date", setdate + "");
                widget.invalidateDecorators();
                executescheduleList(setdate);
            }
        });
        date.setShowOtherDates(MaterialCalendarView.SHOW_ALL);
        Calendar instance = Calendar.getInstance();
        CalendarDay day = CalendarDay.from(instance.getTime());
        ArrayList<CalendarDay> calenderdates = new ArrayList<>();
        calenderdates.add(day);
        new DateSimulator(calenderdates).execute();
        date.setTopbarVisible(true);
        Calendar cal = Calendar.getInstance();
        month_date = new SimpleDateFormat("MMMM yyyy");
        String month_name = month_date.format(cal.getTime());
        month.setText("" + month_name);
        date.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
                Calendar calendar=Calendar.getInstance();
                calendar.set(Calendar.MONTH,date.getMonth() );
                calendar.set(Calendar.YEAR,date.getYear() );
                month_date = new SimpleDateFormat("MMMM yyyy");
                String month_name = month_date.format(calendar.getTime());
                month.setText(month_name+"");
            }
        });
         date.addDecorators(
                new MySelectorDecorator(getActivity()),
                //new HighlightWeekendsDecorator(),
                oneDayDecorator
        );
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.months:

                break;
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), getActivity().getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.SCHEDULED_LIST)) {
            recycler_dates.setAdapter(null);
            parseScheduledList(result);
        } else if (apiName.equals(ServerRequestConstants.SCHEDULED_DATES)) {
            parseScheduledDate(result);
        }
    }
    private class DateSimulator extends AsyncTask<Void, Void, List<CalendarDay>> {
        ArrayList<CalendarDay> calenderdates;

        public DateSimulator(ArrayList<CalendarDay> calenderdates) {
            this.calenderdates = calenderdates;
        }

        @Override
        protected List<CalendarDay> doInBackground(@NonNull Void... voids) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return calenderdates;
        }

        @Override
        protected void onPostExecute(@NonNull List<CalendarDay> calendarDays) {
            super.onPostExecute(calendarDays);

            if (getActivity().isFinishing()) {
                return;
            }
            Log.v("test==", calendarDays + "");
            date.addDecorator(new CurrentDateDecorator(getActivity(), calendarDays));
        }
    }
    private void parseScheduledDate(String result) {
        try {
            JSONObject object = new JSONObject(result);
            ArrayList<String> dates = new ArrayList<>();
            if (object.getInt(Tags.status) == 1 && object.getInt(Tags.dataFlow) == 1) {
                JSONArray array = object.getJSONArray(Tags.response);
                for (int i = 0; i < array.length(); i++) {
                    AppDelegate.LogT("one ");
                    JSONObject obj = array.getJSONObject(i);
                    dates.add(obj.getString(Tags.training_date).replaceAll("/+0000", ""));
                }
                setdatesOnCalender(dates);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


  /*  private void addEvents(Date dateObject) {
//        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
//        currentCalender.setTime(dateObject);
//
//        setToMidnight(currentCalender);
//
        AppDelegate.LogT("addEvents => " + dateObject);
        long timeInMillis = dateObject.getTime();
        AppDelegate.LogT("addEvents => 1 => " + dateObject.getTime());

        List<Event> events = getEvents(timeInMillis);


        date.addEvents(events);
        AppDelegate.LogT("getEvents => " + date.getEvents(timeInMillis).toString());
    }

    private List<Event> getEvents(long timeInMillis) {
        AppDelegate.LogT("getEvents => " + timeInMillis);
        return Arrays.asList(new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)));
    }

    private List<Event> getEvents(long timeInMillis, int day) {
        if (day < 2) {
            return Arrays.asList(new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)));
        } else if (day > 2 && day <= 4) {
            return Arrays.asList(
                    new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)),
                    new Event(Color.argb(255, 100, 68, 65), timeInMillis, "Event 2 at " + new Date(timeInMillis)));
        } else {
            return Arrays.asList(
                    new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)),
                    new Event(Color.argb(255, 100, 68, 65), timeInMillis, "Event 2 at " + new Date(timeInMillis)),
                    new Event(Color.argb(255, 70, 68, 65), timeInMillis, "Event 3 at " + new Date(timeInMillis)));
        }
    }

    private void setToMidnight(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }
*/
    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 0:
                        if(AppDelegate.isValidString(setdate)) {
                            executescheduleList(setdate);
                        }
                        break;
                    case 1:
                        if(!new Prefs(getActivity()).getUserdata().avtar.isEmpty())
                        new LoadImage().execute(new Prefs(getActivity()).getUserdata().avtar);
                        break;
                }
            }
        };
    }

    private void setdatesOnCalender(ArrayList<String> dates) {
        ArrayList<CalendarDay> calenderdates = new ArrayList<>();
        for (int i = 0; i < dates.size(); i++) {
            try {
                Date simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(dates.get(i));
                Log.v("timeformate==>", simpleDateFormat + "date");
                String formattedDate = new SimpleDateFormat("dd-MM-yyyy").format(simpleDateFormat);
                Date date = new SimpleDateFormat("dd-MM-yyyy").parse(formattedDate);
                CalendarDay day = CalendarDay.from(date);
                calenderdates.add(day);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        new ApiSimulator(calenderdates).execute();
    }

    private void executescheduleList(String setdate) {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(getActivity()).getUserdata().userId);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.date, setdate + "");
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(getActivity(), CalendarFragment.this, ServerRequestConstants.SCHEDULED_LIST,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void parseScheduledList(String result) {
        try {
            JSONObject object = new JSONObject(result);
            ArrayList<ScheduledListModel> scheduled_List = new ArrayList<>();
            if (object.getInt(Tags.status) == 1 && object.getInt(Tags.dataFlow) == 1) {
                JSONArray array = object.getJSONArray(Tags.response);
                for (int i = 0; i < array.length(); i++) {
                    ScheduledListModel scheduledmodel = new ScheduledListModel();
                    JSONObject obj = array.getJSONObject(i);
                    scheduledmodel.id = obj.getInt(Tags.id);
                    scheduledmodel.team_id = obj.getInt(Tags.team_id);
                    scheduledmodel.coach_id = obj.getInt(Tags.coach_id);
                    scheduledmodel.training_period = obj.getInt(Tags.training_period);
                    scheduledmodel.is_confirm = obj.getInt(Tags.is_confirm);
                    scheduledmodel.status = obj.getInt(Tags.status);
                    scheduledmodel.latitude = obj.getDouble(Tags.latitude);
                    scheduledmodel.longitude = obj.getDouble(Tags.longitude);
                    scheduledmodel.coach = obj.getString(Tags.coach);
                    scheduledmodel.teamName = obj.getString(Tags.teamName);
                    scheduledmodel.teamAvtar = obj.getString(Tags.teamAvtar);
                    scheduledmodel.training_date = obj.getString(Tags.training_date);
                    scheduledmodel.log_hours=obj.getInt(Tags.log_hours);
                    ArrayList<String> teammembers = new ArrayList<>();
                    JSONArray members = obj.getJSONArray(Tags.teamMembers);
                    for (int j = 0; j < members.length(); j++) {
                        String team = members.getString(j);
                        teammembers.add(team);
                    }
                    ArrayList<String> teammembersAvtar = new ArrayList<>();
                    JSONArray membersAvtar = obj.getJSONArray(Tags.teamMembersAvtar);
                    for (int j = 0; j < membersAvtar.length(); j++) {
                        String team = membersAvtar.getString(j);
                        teammembersAvtar.add(team);
                    }
                    scheduledmodel.teamMembers = teammembers;
                    scheduledmodel.teamMembersAvtar = teammembersAvtar;
                    scheduled_List.add(scheduledmodel);
                    /*if (object.has(Tags.avtar_thumb)) {
                        userDataModel.avtar_thumb = object.getString(Tags.avtar_thumb);
                    }*/
                }
                setdatesOnList(scheduled_List);
            } else if (object.getInt(Tags.status) == 0 && object.getInt(Tags.dataFlow) == 0) {

            }
        } catch (JSONException e) {
            // setdatesOnCalender(dates);
            e.printStackTrace();
        }
    }
    private void setdatesOnList(ArrayList<ScheduledListModel> scheduled_List) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recycler_dates.setLayoutManager(layoutManager);
        Recycler_Calendar_dates_Adapter adapter = new Recycler_Calendar_dates_Adapter(getActivity(), scheduled_List);
        recycler_dates.setAdapter(adapter);
    }
    private class ApiSimulator extends AsyncTask<Void, Void, List<CalendarDay>> {
        ArrayList<CalendarDay> calenderdates;

        public ApiSimulator(ArrayList<CalendarDay> calenderdates) {
            this.calenderdates = calenderdates;
        }

        @Override
        protected List<CalendarDay> doInBackground(@NonNull Void... voids) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return calenderdates;
        }

        @Override
        protected void onPostExecute(@NonNull List<CalendarDay> calendarDays) {
            super.onPostExecute(calendarDays);

            if (getActivity().isFinishing()) {
                return;
            }
            Log.v("test==", calendarDays + "");
            date.addDecorator(new EventDecorator(getActivity(), calendarDays));
        }
    }
}
