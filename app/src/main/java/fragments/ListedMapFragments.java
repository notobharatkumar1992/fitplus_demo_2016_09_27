package fragments;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fitplus.AppDelegate;
import com.fitplus.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.SupportMapFragment;

public class ListedMapFragments extends Fragment {

    View rootview;
    private MapView mapview;
    private SupportMapFragment mSupportMapFragment;
    private GoogleMap mMap;
    private Double latitude, longitude;
    private SupportMapFragment fragment;
    private GoogleMap map_business;
    private RelativeLayout toolbar;
    TextView toolbar_title;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //rootview = inflater.inflate(R.layout.global_map, container, false);
        AppDelegate.LogT("GLOBAL MAP");
        rootview = inflater.inflate(R.layout.global_map, container, false);
        latitude = 26.78;
        longitude = 72.56;
        return rootview;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        toolbar = (RelativeLayout) rootview.findViewById(R.id.toolbar);
        toolbar_title = (TextView) rootview.findViewById(R.id.title);
        fragment = SupportMapFragment.newInstance();
        getChildFragmentManager().beginTransaction()
                .replace(R.id.map_Frame, fragment, "MAP1").addToBackStack(null)
                .commit();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showMap();
            }
        }, 1500);
    }
    private void showMap() {
        map_business = fragment.getMap();
        if (map_business == null) {
            return;
        }
        map_business.setMyLocationEnabled(true);
        map_business.getUiSettings().setMapToolbarEnabled(false);
        map_business.getUiSettings().setZoomControlsEnabled(false);
        map_business.getUiSettings().setCompassEnabled(false);
        map_business.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }

}
