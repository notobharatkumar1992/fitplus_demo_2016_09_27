package fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fitplus.AppDelegate;
import com.fitplus.CreateTeamActivity;
import com.fitplus.MainActivity;
import com.fitplus.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.NotificationListAdapter;
import adapters.TeamListAdapter;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.Create_Team_Model;
import model.Notification_Fragments_Model;
import model.PostAysnc_Model;
import model.TeamListModel;
import utils.Prefs;

/**
 * Created by admin on 26-07-2016.
 */
public class NotificationFragments extends Fragment implements OnClickListener, AdapterView.OnItemClickListener, OnReciveServerResponse, OnDialogClickListener {

    private View rootview;
    RelativeLayout toolbar;
    TextView toolbar_title;
    ImageView tool_menu, create_team;
    RecyclerView team_list;
    private Bundle bundle;

    public static Handler mHandler;
    public boolean shouldUpdate = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.team_list, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 1) {

                        execute_teamList();

                }
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHandler = null;
    }

    private void initView() {
        toolbar = (RelativeLayout) rootview.findViewById(R.id.toolbar);
        toolbar_title = (TextView) rootview.findViewById(R.id.title);
        toolbar_title.setText(getActivity().getResources().getString(R.string.NotificationList));
        tool_menu = (ImageView) rootview.findViewById(R.id.menu);
        tool_menu.setOnClickListener(this);
        create_team = (ImageView) rootview.findViewById(R.id.create_team);
        create_team.setOnClickListener(this);
        create_team.setVisibility(View.GONE);
        team_list = (RecyclerView) rootview.findViewById(R.id.team_list);
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        execute_teamList();
    }
    private void execute_teamList() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(getActivity()).getUserdata().userId);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(getActivity(), NotificationFragments.this, ServerRequestConstants.NOTIFICATIONS,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.create_team:

                break;
            case R.id.menu:
                ((MainActivity) getActivity()).toggleSlider();
                break;

        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {

        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), getActivity().getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.NOTIFICATIONS)) {
            parseNOTIFICATIONS(result);
        }
    }


    private void parseNOTIFICATIONS(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONArray response = jsonObject.getJSONArray(Tags.response);
                ArrayList<Notification_Fragments_Model> team_list = new ArrayList<>();
                MainActivity.counter=0;
                for (int i = 0; i < response.length(); i++) {
                    Notification_Fragments_Model userDataModel = new Notification_Fragments_Model();
                    JSONObject object = response.getJSONObject(i);
                    userDataModel.team_id = object.getInt(Tags.team_id);
                    userDataModel.notify_id = object.getInt(Tags.id);
                    userDataModel.user_id = object.getInt("user_id");
                    userDataModel.notificaton_type = object.getString(Tags.notificaton_type);
                    userDataModel.action_taken = object.getInt(Tags.action_taken);
                    userDataModel.message = object.getString(Tags.message);
                    userDataModel.status = object.getInt(Tags.status);
                    userDataModel.teamMembers = object.getInt(Tags.teamMembers);
                    userDataModel.teamname = object.getString(Tags.teamname);
                    userDataModel.team_avtar = object.getString(Tags.team_avtar);
                   // JSONObject json =jsonObject.getJSONObject(Tags.user);
                  /*  userDataModel.user_img_one=object.getString(Tags.avtar_thumb);
                    userDataModel.user_name_one=object.getString(Tags.first_name);*/
                    userDataModel.username_first=object.getString(Tags.username_first);
                    userDataModel.username_second=object.getString(Tags.username_second);
                    userDataModel.username_third=object.getString(Tags.username_third);
                    userDataModel.FirstAvtar=object.getString(Tags.FirstAvtar);
                    userDataModel.SecondAvtar=object.getString(Tags.SecondAvtar);
                    userDataModel.ThirdAvtar=object.getString(Tags.ThirdAvtar);
                    userDataModel.coach_id= object.has(Tags.coach_id) ? object.getInt(Tags.coach_id) : 0;
                    team_list.add(userDataModel);
                    if(userDataModel.action_taken ==0){
                        MainActivity.counter++;
                    }
                }
                showTeamlist(team_list);
                if(MainActivity.mHandler!=null){
                    MainActivity.mHandler.sendEmptyMessage(4);
                }
                // AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, GlobalMapFragments.this);
            } else {
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
//                if (jsonObject.getInt(Tags.status) == 0) {
//                    AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, FitTeamFragments.this);
//                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null/*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
//                    AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, FitTeamFragments.this);
//                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(getActivity(), getResources().getString(R.string.response_error));
            AppDelegate.LogE(e);
        }
    }
    private void showTeamlist(ArrayList<Notification_Fragments_Model> team_listArray) {
        NotificationListAdapter mAdapter = new NotificationListAdapter(getActivity(), team_listArray);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        team_list.setLayoutManager(mLayoutManager);
        team_list.setItemAnimator(new DefaultItemAnimator());
        team_list.setAdapter(mAdapter);
    }
    @Override
    public void setOnDialogClickListener(String name) {

    }
}
