package utils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by bharat on 24/7/16.
 */
public class SpacesItemTransDecoration extends RecyclerView.ItemDecoration {
    private int space, leftRightspace, count = 0, topOnce;
    private boolean value = false;

    public SpacesItemTransDecoration(int space) {
        this.space = space;
    }

    public SpacesItemTransDecoration(int leftRightspace, int topBottomSpace, int topOnce) {
        this.leftRightspace = leftRightspace;
        this.space = topBottomSpace;
        this.topOnce = topOnce;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if (parent.getChildLayoutPosition(view) % 2 == 0) {
            outRect.left = space;
            outRect.right = space;
        } else {
            outRect.left = 0;
            outRect.right = space;
        }

        if (!value) {
            outRect.bottom = space;
            outRect.top = 0;

            // Add top margin only for the first item to avoid double space between items
            if (count == 0) {
                if (parent.getChildLayoutPosition(view) == 0 || parent.getChildLayoutPosition(view) == 1) {
                    outRect.top = topOnce;
                } else {
                    outRect.top = 0;
                }
            }
        }
    }
}