package utils.decorators;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.fitplus.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.Collection;
import java.util.HashSet;

/**
 * Decorate several days with a dot
 */
public class EventDecorator implements DayViewDecorator {
    private final Drawable drawable;
    Context context;
    private int color;
    private HashSet<CalendarDay> dates;

    public EventDecorator(Context context, Collection<CalendarDay> dates) {
        this.context=context;
        this.dates = new HashSet<>(dates);
        drawable = context.getResources().getDrawable(R.drawable.event_my_selector);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        Log.v("shouldDecorate===", day+" true of false ans=="+dates.contains(day));
        return dates.contains(day);
    }


    @Override
    public void decorate(DayViewFacade view) {
        view.setBackgroundDrawable(drawable);
        //view.addSpan(new DotSpan(5, color));
    }
}
