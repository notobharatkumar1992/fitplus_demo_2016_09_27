package utils.decorators;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;

import com.fitplus.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

/**
 * Use a custom selector
 */
public class MySelectorDecorator implements DayViewDecorator {

    private  Drawable drawable;

    public MySelectorDecorator(Activity context) {
        if(context!=null)
        drawable = context.getResources().getDrawable(R.drawable.my_selector);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return true;
    }

    @Override
    public void decorate(DayViewFacade view) {
        if(drawable!=null) {
            Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
            //  view.setSelectionDrawable(drawable);
            view.setSelectionDrawable(transparentDrawable);
            view.setBackgroundDrawable(drawable);
        }
    }
}
