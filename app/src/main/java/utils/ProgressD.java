package utils;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;


/**
 * Created by admin on 17-05-2016.
 */
public class ProgressD {
    public static  ProgressDialog pDialog;
    static Dialog builder;
    public static void showProgress_Dialog(Context context) {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();
    }
    public static void hideProgress_dialog() {
        try {
            pDialog.dismiss();
            pDialog = null;
        } catch (Exception e) {
//            AppDelegate.LogE(e);
        }
    }
    public static void ShoDialog(Context context, String Message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(Message)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

   /* public static void ShowDialog(Context context, final String Message, final String Title) {
        builder = new Dialog(context);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setContentView(R.layout.error_dialog);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView ok, message, title;
        message = (TextView) builder.findViewById(R.id.message);
        message.setText(Message);

        builder.show();
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (builder != null && builder.isShowing()) {
                    builder.dismiss();
                }
            }
        };

        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                handler.removeCallbacks(runnable);
            }
        });

        handler.postDelayed(runnable, 1000);
    }*/

}
